package com.codespot.insurance.service.insurance;

import com.codespot.insurance.model.dto.DateFilter;
import com.codespot.insurance.model.dto.InsuranceDto;
import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.dto.RemoveDocument;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.Dependent;
import com.codespot.insurance.model.entity.insurance.*;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.model.entity.payment.Claim;
import com.codespot.insurance.model.entity.payment.Credit;
import com.codespot.insurance.model.entity.payment.Receipt;
import com.codespot.insurance.model.repository.CreditRepository;
import com.codespot.insurance.model.repository.InvoiceRepository;
import com.codespot.insurance.model.repository.ReceiptRepository;
import com.codespot.insurance.model.repository.customer.DependentRepository;
import com.codespot.insurance.model.repository.insurance.*;
import com.codespot.insurance.model.spec.InsuranceRenewSpecification;
import com.codespot.insurance.report.model.RenewReport;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.service.claim.ClaimService;
import com.codespot.insurance.service.customer.CustomerService;
import com.codespot.insurance.service.file.FileService;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.codespot.insurance.web.controller.InsuranceController;
import com.condos.shared.aws.S3;
import com.condos.shared.data.page.C2Criteria;
import com.condos.shared.exception.BadRequestException;
import com.condos.shared.exception.NotFoundException;
import com.condos.shared.exception.UnsupportedOperationException;
import lombok.NonNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service(value = "defaultInsuranceService")
public class DefaultInsuranceService implements InsuranceService {


    private final
    InsuranceDetailRepository detailRepository;

    private final
    CustomerService customerService;

    private final
    InsuranceRepository insuranceRepository;

    private final
    BranchRepository branchRepository;

    private final
    InsuranceCarrierRepository carrierRepository;

    private final
    VehicleRepository vehicleRepository;

    private final
    CoverageRepository coverageRepository;

    private final
    ModelMapper modelMapper;

    private final
    LocalRepository localRepository;

    private final
    DependentRepository dependentRepository;

    private final
    InsuranceRenewRepository renewRepository;

    private final
    RenewReport renewReport;

    private final
    FileService fileService;

    private final
    UserService userService;

    private final
    ReceiptRepository receiptRepository;

    private final
    InvoiceRepository invoiceRepository;

    private final
    CreditRepository creditRepository;

    private final
    ClaimService claimService;

    private final
    LoggerActivityService activityService;

    @Autowired
    public DefaultInsuranceService(InsuranceDetailRepository detailRepository,
                                   CustomerService customerService,
                                   InsuranceRepository insuranceRepository,
                                   BranchRepository branchRepository,
                                   InsuranceCarrierRepository carrierRepository,
                                   VehicleRepository vehicleRepository,
                                   CoverageRepository coverageRepository,
                                   ModelMapper modelMapper,
                                   LocalRepository localRepository,
                                   DependentRepository dependentRepository,
                                   InsuranceRenewRepository renewRepository,
                                   RenewReport renewReport,
                                   FileService fileService,
                                   UserService userService,
                                   ReceiptRepository receiptRepository,
                                   InvoiceRepository invoiceRepository, CreditRepository creditRepository, ClaimService claimService, LoggerActivityService activityService) {

        this.detailRepository = detailRepository;
        this.customerService = customerService;
        this.insuranceRepository = insuranceRepository;
        this.branchRepository = branchRepository;
        this.carrierRepository = carrierRepository;
        this.vehicleRepository = vehicleRepository;
        this.coverageRepository = coverageRepository;
        this.modelMapper = modelMapper;
        this.localRepository = localRepository;
        this.dependentRepository = dependentRepository;
        this.renewRepository = renewRepository;
        this.renewReport = renewReport;
        this.fileService = fileService;
        this.userService = userService;
        this.receiptRepository = receiptRepository;
        this.invoiceRepository = invoiceRepository;
        this.creditRepository = creditRepository;
        this.claimService = claimService;
        this.activityService = activityService;
    }

    @Override
    public List<InsuranceDetail> getAll(Long customerId) {
        Customer customer = this.customerService.getOne(customerId);
        return this.detailRepository.findAllByCustomerEquals(customer);
    }

    @Override
    public List<Insurance> getAllPrimitive(Long customer) {
        return this.insuranceRepository.findAllByCustomerClientId(customer);
    }

    @Override
    public Insurance getOne(Long insurance) {
        return this.insuranceRepository.findById(insurance)
                .orElseThrow(() -> new NotFoundException(String.format("Póliza [%s] no pudo ser encontrada!", insurance.toString()), insurance.toString(), LogActivity.Scope.INSURANCE.name(), LogActivity.Action.READ.name()));
    }

    @Override
    @Transactional
    public Insurance save(InsuranceDto insuranceDto) {
        Insurance insurance = this.modelMapper.map(insuranceDto, Insurance.class);
        insurance.setModifiedBy(this.userService.loggedUser());
        insurance = save(insurance);
        List<Vehicle> vehicles = insuranceDto.getVehicles();
        List<Local> locals = insuranceDto.getLocals();
        List<Dependent> dependents = insuranceDto.getDependents();
        Insurance finalInsurance = insurance;
        if (vehicles.size() > 0) {
            vehicles.forEach((vehicle) -> vehicle.setInsurance(finalInsurance));
            vehicles = this.vehicleRepository.saveAll(vehicles);

            StringBuilder veh = new StringBuilder();
            vehicles.forEach(vehicle -> veh.append(vehicle.getId()).append(", "));
            String message = "MODIFICANDO INFORMACION DE VEHICULOS, %s.";
            activityService.success(String.format(message, veh.toString()), LogActivity.Scope.VEHICLE, LogActivity.Action.MODIFY, null);

        } else if (locals.size() > 0) {
            locals.forEach((local) -> local.setInsurance(finalInsurance));
            locals = this.localRepository.saveAll(locals);

            StringBuilder veh = new StringBuilder();
            locals.forEach(local -> veh.append(local.getId()).append(", "));
            String message = "MODIFICANDO INFORMACION DE LOCALES, %s.";
            activityService.success(String.format(message, veh.toString()), LogActivity.Scope.LOCAL, LogActivity.Action.MODIFY, null);

        } else if (dependents.size() > 0) {
            dependents.forEach((dependent) -> dependent.setInsurance(finalInsurance));
            dependents = this.dependentRepository.saveAll(dependents);
            StringBuilder veh = new StringBuilder();
            dependents.forEach(dependent -> veh.append(dependent.getName()).append(", "));
            String message = "MODIFICANDO INFORMACION DE DEPENDIENTES, %s.";
            activityService.success(String.format(message, veh.toString()), LogActivity.Scope.DEPENDENT, LogActivity.Action.MODIFY, null);
        }
        return insurance;
    }

    @Override
    @Transactional
    public Insurance save(Insurance insurance) {
        String message = "MODIFICANDO INFORMACION DE POLIZA, %s.";
        LogActivity.Action action = LogActivity.Action.MODIFY;

        if (insurance.isNew()) {
            message = "AGREGANDO NUEVA POLIZA, %s.";
            action = LogActivity.Action.CREATE;
        }

        insurance = this.insuranceRepository.save(insurance);

        activityService.success(String.format(message, insurance.getInsuranceNumber()), LogActivity.Scope.INSURANCE, action, insurance.getId().toString());
        return insurance;
    }

    @Override
    public InsuranceDto getByCustomerAndInsurance(Long customerId, Long insuranceId) {
        Insurance insurance;
        if (customerId != 0) {
            Customer customer = this.customerService.getOne(customerId);
            insurance = this.insuranceRepository.findByCustomerEqualsAndIdEquals(customer, insuranceId);
        } else {
            insurance = getOne(insuranceId);
        }

        InsuranceDto insuranceDto = this.modelMapper.map(insurance, InsuranceDto.class);
        List<Vehicle> vehicles = this.vehicleRepository.findAllByInsuranceEquals(insurance);
        List<Local> locals = this.localRepository.findAllByInsuranceEquals(insurance);
        List<Dependent> dependents = this.dependentRepository.findAllByInsuranceEquals(insurance);
        insuranceDto.setVehicles(vehicles);
        insuranceDto.setLocals(locals);
        insuranceDto.setDependents(dependents);

        return insuranceDto;
    }

    @Override
    public List<Branch> getAvailableBranches() {
        return this.branchRepository.findAll();
    }

    @Override
    public List<Coverage> getAvailableCoverage() {
        return this.coverageRepository.findAll();
    }

    @Override
    public List<InsuranceCarrier> getAvailableCarriers() {
        return this.carrierRepository.findAll();
    }

    @Override
    public InsuranceCarrier saveCarrier(InsuranceCarrier carrier) {
        return carrierRepository.save(carrier);
    }

    @Override
    public Page<InsuranceCarrier> getAllCarriers(PageDto pageDto) {
        Pageable pageable = PageRequest.of(pageDto.getPage(), pageDto.getSize(), new Sort(Sort.Direction.ASC, "name"));
        return this.carrierRepository.findAll(pageable);
    }

    @Override
    public List<Vehicle> getVehicles(Long insuranceId) {
        Insurance insurance = getOne(insuranceId);
        return this.vehicleRepository.findAllByInsuranceEquals(insurance);
    }

    @Override
    public List<Local> getLocals(Long insuranceId) {
        Insurance insurance = getOne(insuranceId);
        return this.localRepository.findAllByInsuranceEquals(insurance);
    }

    @Override
    public Vehicle getOneVehicle(Long id) {
        return this.vehicleRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Vehículo [%s] no pudo ser encontrada!", id.toString()), id.toString(), LogActivity.Scope.VEHICLE.name(), LogActivity.Action.READ.name()));
    }

    @Override
    @Transactional
    public Branch saveBranch(String desc) {
        Branch branch = new Branch();
        branch.setDescription(desc);

        return this.branchRepository.save(branch);
    }

    @Override
    public Page<InsuranceRenew> getRenew(@NonNull PageDto page, @NonNull C2Criteria criteria, @NonNull DateFilter dateFilter) {

        Pageable pageable = PageRequest.of(page.getPage(), page.getSize(), new Sort(page.getSort(), "endDate"));

        if (criteria.getKey().equals("carrier")) {
            InsuranceCarrier carrier = this.carrierRepository.findById(Long.parseLong((String) criteria.getValue())).orElse(null);
            criteria.setValue(carrier);
        }
        InsuranceRenewSpecification sp1 = InsuranceRenewSpecification.of(criteria);
        criteria = C2Criteria.builder().key("month").value(dateFilter.getMonth().index() + 1).operation(":").build();
        InsuranceRenewSpecification sp2 = InsuranceRenewSpecification.of(criteria);
        criteria = C2Criteria.builder().key("year").value(dateFilter.getYear()).operation(":").build();
        InsuranceRenewSpecification sp3 = InsuranceRenewSpecification.of(criteria);

        if (dateFilter.getMonth().isAll())
            return this.renewRepository.findAll(Specification.where(sp1), pageable);
        return this.renewRepository.findAll(Specification.where(sp1).and(sp2).and(sp3), pageable);
    }

    @Override
    public String printRenew(PageDto pageDto, C2Criteria criteria, DateFilter dateFilter) throws IOException {
        Pageable pageable = PageRequest.of(0, 1000, new Sort(Sort.Direction.ASC, "endDate"));

        if (criteria.getKey().equals("carrier")) {
            InsuranceCarrier carrier = this.carrierRepository.findById(Long.parseLong((String) criteria.getValue())).orElse(null);
            criteria.setValue(carrier);
        }
        InsuranceRenewSpecification sp1 = InsuranceRenewSpecification.of(criteria);
        criteria = C2Criteria.builder().key("month").value(dateFilter.getMonth().index() + 1).operation(":").build();
        InsuranceRenewSpecification sp2 = InsuranceRenewSpecification.of(criteria);
        criteria = C2Criteria.builder().key("year").value(dateFilter.getYear()).operation(":").build();
        InsuranceRenewSpecification sp3 = InsuranceRenewSpecification.of(criteria);

        List<InsuranceRenew> renews = this.renewRepository.findAll(Specification.where(sp1).and(sp2).and(sp3), pageable).getContent();
        File file = renewReport.buildPdf(renews, dateFilter.getMonth(), dateFilter.getYear());
        return this.fileService.s3Upload(file, S3.Extension.PDF);
    }

    @Override
    @Transactional
    public void deleteDocument(RemoveDocument documentInfo) {
        boolean validPassword = this.userService.isValidPassword(documentInfo.getPassword());

        if (validPassword) {
            switch (documentInfo.getType()) {
                case RECEIPT:
                    clearReceipt(documentInfo.getDocumentNo());
                    break;
                case CREDIT:
                    clearCredit(documentInfo.getDocumentNo());
                case INVOICE: {
                    clearInvoice(documentInfo.getDocumentNo());
                    break;
                }
                case INSURANCE:
                    clearInsurance(documentInfo.getDocumentNo());
                    break;
                case CLAIM:
                    clearClaim(documentInfo.getDocumentNo());
                    break;
                case CUSTOMER:
                    clearCustomer(documentInfo.getDocumentNo());
                    break;
                default:
                    throw new UnsupportedOperationException("Invalid operation");
            }
        } else
            throw new BadRequestException("Passwords does not match", documentInfo.getDocumentNo().toString(), documentInfo.getType().name(), LogActivity.Action.DELETE.name());

    }

    @Override
    @Transactional
    public void deleteElement(Long element, InsuranceController.ElementType type) {
        switch (type) {
            case LOCAL:
                Local local = Local.dummy(element);
                this.localRepository.delete(local);
                break;
            case VEHICLE:
                Vehicle vehicle = Vehicle.dummy(element);
                this.vehicleRepository.delete(vehicle);
                break;
            case DEPENDENT:
                Dependent dependent = Dependent.dummy(element);
                this.dependentRepository.delete(dependent);
                break;
        }
    }

    private void clearCustomer(Long documentNo) {
        List<Insurance> insurances = this.insuranceRepository.findAllByCustomerClientId(documentNo);
        insurances.forEach(insurance -> clearInsurance(insurance.getId()));
        this.customerService.delete(documentNo);
    }

    @Transactional
    void clearInsurance(Long id) {
        Insurance insurance = getOne(id);
        List<Invoice> invoices = this.invoiceRepository.findAllByInsuranceEquals(insurance);
        List<Dependent> dependents = this.dependentRepository.findAllByInsuranceEquals(insurance);
        this.dependentRepository.deleteAll(dependents);
        clearInvoice(invoices);
        List<Vehicle> vehicles = this.vehicleRepository.findAllByInsuranceEquals(insurance);
        this.vehicleRepository.deleteAll(vehicles);
        List<Local> locals = this.localRepository.findAllByInsuranceEquals(insurance);
        this.localRepository.deleteAll(locals);
        List<Claim> claims = this.claimService.getAllByInsurance(id);
        clearClaim(claims);
        String message = "BORRADO DE INFORMACION DE POLIZA, ".concat(insurance.getInsuranceNumber());
        LogActivity.Action action = LogActivity.Action.DELETE;
        activityService.warning(message, LogActivity.Scope.INSURANCE, action, insurance.getId().toString());

        this.insuranceRepository.delete(insurance);
    }

    private void clearInvoice(Long id) {
        Invoice invoice = this.invoiceRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Factura [%s] no pudo ser encontrada!", id.toString()), id.toString(), LogActivity.Scope.INVOICE.name(), LogActivity.Action.READ.name()));
        List<Receipt> receipts = this.receiptRepository.findAllByInvoiceEquals(invoice);
        clearReceipt(receipts);
        List<Credit> credits = this.creditRepository.findAllByInvoiceEquals(invoice);
        clearCredit(credits);
        String message = "BORRADO DE INFORMACION DE FACTURA, ".concat(invoice.getInvoiceNo());
        activityService.success(message, LogActivity.Scope.INVOICE, LogActivity.Action.DELETE, invoice.getId().toString());

        this.invoiceRepository.delete(invoice);
    }

    private void clearInvoice(List<Invoice> invoices) {
        invoices.forEach(invoice -> {
            List<Receipt> receipts = this.receiptRepository.findAllByInvoiceEquals(invoice);
            clearReceipt(receipts);
            List<Credit> credits = this.creditRepository.findAllByInvoiceEquals(invoice);
            clearCredit(credits);
            String message = "BORRADO DE INFORMACION DE FACTURA, ".concat(invoice.getInvoiceNo());
            activityService.warning(message, LogActivity.Scope.INVOICE, LogActivity.Action.DELETE, invoice.getId().toString());
            this.invoiceRepository.delete(invoice);
        });
    }

    private void clearReceipt(Long id) {
        Receipt receipt = this.receiptRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Recibo [%s] no pudo ser encontrado!", id.toString()), id.toString(), LogActivity.Scope.RECEIPT.name(), LogActivity.Action.DELETE.name()));
        Invoice invoice = receipt.getInvoice();
        invoice.setBalance(invoice.getBalance() + receipt.getAmount());
        this.invoiceRepository.save(invoice);
        String message = "BORRADO DE INFORMACION DE RECIBO, ".concat(receipt.getReceiptNo());
        activityService.warning(message, LogActivity.Scope.RECEIPT, LogActivity.Action.DELETE, receipt.getId().toString());
        this.receiptRepository.delete(receipt);
    }

    private void clearReceipt(List<Receipt> receipts) {
        receipts.forEach(receipt -> {
            Invoice invoice = receipt.getInvoice();
            invoice.setBalance(invoice.getBalance() + receipt.getAmount());
            this.invoiceRepository.save(invoice);
            String message = "BORRADO DE INFORMACION DE RECIBO, ".concat(receipt.getReceiptNo());
            activityService.warning(message, LogActivity.Scope.RECEIPT, LogActivity.Action.DELETE, receipt.getId().toString());
            this.receiptRepository.delete(receipt);
        });
    }

    private void clearCredit(Long id) {
        Credit credit = this.creditRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Crédito [%s] no pudo ser encontrada!", id.toString()), id.toString(), LogActivity.Scope.CREDIT.name(), LogActivity.Action.DELETE.name()));
        Invoice invoice = credit.getInvoice();
        invoice.setBalance(invoice.getBalance() + credit.getAmount());
        this.invoiceRepository.save(invoice);
        String message = "BORRADO DE INFORMACION DE CREDITO, ".concat(credit.getDocNo());
        activityService.warning(message, LogActivity.Scope.CREDIT, LogActivity.Action.DELETE, credit.getId().toString());
        this.creditRepository.delete(credit);
    }

    private void clearCredit(List<Credit> credits) {
        credits.forEach(credit -> {
            Invoice invoice = credit.getInvoice();
            invoice.setBalance(invoice.getBalance() + credit.getAmount());
            this.invoiceRepository.save(invoice);
            String message = "BORRADO DE INFORMACION DE CREDITO, ".concat(credit.getDocNo());
            activityService.warning(message, LogActivity.Scope.CREDIT, LogActivity.Action.DELETE, credit.getId().toString());
            this.creditRepository.delete(credit);
        });
    }

    private void clearClaim(Long documentNo) {
        Claim claim = this.claimService.getOne(documentNo);
        String message = "BORRADO DE INFORMACION DE RECLAMO, ".concat(claim.getDocNo());
        activityService.warning(message, LogActivity.Scope.CLAIM, LogActivity.Action.DELETE, claim.getId().toString());
        this.claimService.remove(claim);
    }

    private void clearClaim(List<Claim> claims) {
        claims.forEach(claim -> clearClaim(claim.getId()));
    }
}
