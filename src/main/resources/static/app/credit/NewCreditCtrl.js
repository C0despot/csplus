angular
    .module(APP)
    .controller('NewCreditCtrl',
        function ($scope,
                  $state,
                  $stateParams,
                  CreditService,
                  CustomerService,
                  InvoiceService,
                  InsuranceService,
                  NotificationService) {

            $scope.edit = true;
            $scope.hidden = true;
            $scope.loading = false;
            $scope.greenLight = false;
            $scope.validating = false;
            $scope.doc = {
                docNo: null,
                customer: null,
                startDate: new Date(),
                endDate: new Date(),
                beginningDate: new Date(),
                dueDate: nextYear(),
                invoice: {
                    insurance: {
                        insuranceNumber: null
                    }
                }
            };
            $scope.insurances = [];
            $scope.$on('$viewContentLoaded', () => {
                let customer = $stateParams.id;

                InvoiceService
                    .allByCustomer(customer)
                    .then((response) => {
                        $scope.invoices = response.data;
                    });

                CustomerService
                    .getExtendedCustomerInfo(customer)
                    .then((response) => {
                        $scope.doc.customer = angular.copy(response.data);
                    });
                InsuranceService
                    .getByCustomer(customer)
                    .then((response) => {
                        $scope.insurances = response.data;
                    });

                InsuranceService
                    .nextIndex('CREDIT')
                    .then((response) => {
                        $scope.doc.docNo = response.data.result;
                        $scope.greenLight = true;
                    });
            });

            $scope.validateAmount = () => {
                if (!$scope.doc.invoice)
                    return false;

                $scope.doc.insurance = angular.copy($scope.doc.invoice.insurance);
                let balance = angular.copy($scope.doc.invoice.balance);
                let amount = angular.copy($scope.doc.amount);
                balance = numberFormatter(balance);
                amount = numberFormatter(amount);
                if (balance < amount)
                    $("#amount").addClass('has-error');
                else
                    $("#amount").removeClass('has-error');
            };

            $scope.doSave = () => {
                let credit = angular.copy($scope.doc);
                credit.startDate = new Date(credit.startDate);
                credit.endDate = new Date(credit.endDate);
                credit.beginDate = new Date(credit.beginDate);
                credit.dueDate = new Date(credit.dueDate);

                credit.amount = numberFormatter(credit.amount);

                $scope.loading = true;
                CreditService
                    .saveOne(credit)
                    .then(() => {
                        NotificationService.successMsj();
                        $state.go("customer.detail.step_five.list", {id: $stateParams.id});
                    }, () => {
                        NotificationService.errorMsj();
                    })
                    .finally(() => {
                        $scope.loading = false;
                    });
            };

            $scope.validateMe = () => {
                $scope.validating = true;
                InsuranceService
                    .validateNumber($scope.doc.docNo, 'CREDIT')
                    .then(() => {
                        $("#docNo").removeClass("has-error");
                    }, () => {
                        $("#docNo").addClass("has-error");
                    })
                    .finally(() => {
                        $scope.validating = false;
                    })
            }
        });