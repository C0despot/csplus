package com.codespot.insurance.model.entity.insurance;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "BRANCH")
public class Branch {

    @Id
    @Column(name = "BRANCH_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "BRANCH_DESCRIPTION")
    private String description;
}
