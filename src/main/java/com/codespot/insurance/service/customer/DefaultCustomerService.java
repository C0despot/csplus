package com.codespot.insurance.service.customer;

/*
  @author - Jose A. Rodriguez.
*/

import com.codespot.insurance.model.dto.CustomerDto;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.CustomerDoc;
import com.codespot.insurance.model.repository.customer.CustomerDetailRepository;
import com.codespot.insurance.model.repository.customer.CustomerDocRepository;
import com.codespot.insurance.model.repository.customer.CustomerRepository;
import com.codespot.insurance.security.model.type.Types;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.condos.shared.exception.BadRequestException;
import com.condos.shared.exception.NotFoundException;
import com.condos.shared.util.FieldUtil;
import lombok.extern.java.Log;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log
@Service(value = "defaultCustomerService")
public class DefaultCustomerService implements CustomerService {

    private final
    ModelMapper modelMapper;

    private final
    CustomerRepository customerRepository;

    private final
    CustomerDetailRepository customerDetailRepository;

    private final
    CustomerDocRepository docRepository;

    private final
    LoggerActivityService activityService;

    private final
    UserService userService;

    @Autowired
    public DefaultCustomerService(ModelMapper modelMapper, CustomerRepository customerRepository, CustomerDetailRepository customerDetailRepository, CustomerDocRepository docRepository, LoggerActivityService activityService, UserService userService) {
        this.modelMapper = modelMapper;
        this.customerRepository = customerRepository;
        this.customerDetailRepository = customerDetailRepository;
        this.docRepository = docRepository;
        this.activityService = activityService;
        this.userService = userService;
    }

    @Override
    public CustomerDto customerInfo(Long id) {
        Customer customer = getOne(id);
        CustomerDto customerDto = this.modelMapper.map(customer, CustomerDto.class);
        Double balance = this.customerDetailRepository.getCustomerBalance(customer.getClientId(), null);
        customerDto.setBalance(balance);
        return customerDto;
    }

    @Override
    @Transactional
    public Customer save(Customer customer) {
        if (customer.getType().equals(Types.Customer.PERSONA))
            customer.setName(customer.getFirstName() + " " + customer.getLastName());
        else customer.setName(customer.getCompany());
        if (customer.getCellPhone() != null && !customer.getCellPhone().equalsIgnoreCase(""))
            customer.setPhone(customer.getCellPhone());
        else if (customer.getHomePhone() != null && !customer.getHomePhone().equalsIgnoreCase(""))
            customer.setPhone(customer.getHomePhone());
        else {
            customer.setPhone(customer.getWorkPhone());
        }

        String message = "MODIFICANDO INFORMACION DE CLIENTE, %s.";
        LogActivity.Action action = LogActivity.Action.MODIFY;

        LogActivity.Scope scope = LogActivity.Scope.CUSTOMER;
        if (customer.isNew()) {
            if (customer.isEnterprise() && Objects.nonNull(customer.getRnc())) {
                List<Customer> customers = this.customerRepository.findByRncEquals(customer.getRnc());
                if (!customers.isEmpty()) {
                    throw new BadRequestException("RNC ya está registrado!", customer.getRnc(), scope.name(), LogActivity.Action.CREATE.name());
                }
            } else if (Objects.nonNull(customer.getIdCard())) {
                List<Customer> customers = this.customerRepository.findByIdCardEquals(customer.getIdCard());
                if (!customers.isEmpty()) {
                    throw new BadRequestException("CÉDULA o PASAPORTE ya está registrada!", customer.getIdCard(), scope.name(), LogActivity.Action.CREATE.name());
                }
            }
            message = "AGREGANDO NUEVO CLIENTE, %s.";
            action = LogActivity.Action.CREATE;
            customer.setCreatedBy(this.userService.loggedUser());
            customer.setCreationDate(new Date());
        }
        customer.setModifiedBy(this.userService.loggedUser());
        customer.setModificationDate(new Date());

        FieldUtil.validateFields(customer, scope.name(), action.name());
        customer = this.customerRepository.save(customer);

        activityService.success(String.format(message, customer.getName()), scope, action, customer.getClientId().toString());
        return customer;
    }

    @Override
    public Customer getOne(Long id) {
        return this.customerRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("El cliente [%s] no pudo ser encontrado!", id.toString()), id.toString(), LogActivity.Scope.CUSTOMER.name(), LogActivity.Action.READ.name()));
    }

    @Override
    public void delete(long id) {
        Customer customer = getOne(id);
        this.customerRepository.delete(customer);
        String message = "BORRADO DE INFORMACION DE CLIENTE, ".concat(customer.getName());
        LogActivity.Action action = LogActivity.Action.DELETE;
        activityService.warning(message, LogActivity.Scope.CUSTOMER, action, customer.getClientId().toString());
    }

    @Override
    public List<Customer> filter(String criteria) {
        List<Customer> customers = this.customerRepository.findByFilterCriteria("%" + criteria + "%");
        return customers.stream().distinct().collect(Collectors.toList()).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    @Override
    public List<CustomerDoc> filterDocs(String criteria) {
        Pageable pageable = PageRequest.of(0, 50, new Sort(Sort.Direction.ASC, "document"));
        List<CustomerDoc> customers = this.docRepository.findAllByDocumentContaining(criteria, pageable);
        return customers.stream().distinct().collect(Collectors.toList()).stream().filter(Objects::nonNull).collect(Collectors.toList());
    }
}
