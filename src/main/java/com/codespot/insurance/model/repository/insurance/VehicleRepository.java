package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.insurance.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    List<Vehicle> findAllByInsuranceEquals(Insurance insurance);
}
