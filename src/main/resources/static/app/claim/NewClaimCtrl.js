angular
    .module(APP)
    .controller('NewClaimCtrl',
        function ($scope, $state, $stateParams, InvoiceService, InsuranceService, NotificationService, CustomerService) {

            $scope.edit = true;
            $scope.hidden = true;
            $scope.loading = false;
            $scope.greenLight = false;
            $scope.claim = {
                docNo: null,
                customer: null,
                insurance: null,
                creationDate: new Date(),
                endDate: new Date(),
                beginningDate: new Date(),
                dueDate: nextYear(),
                prime: 0,
                type: 'EMISSION'
            };
            $scope.updateCarrier = (insurance) => {
                if (insurance) {
                    $scope.claim.log = angular.copy(insurance.carrier);
                }
                else
                    $scope.claim.log = null;
            };
            $scope.insurances = [];
            $scope.$on('$viewContentLoaded', () => {
                let customer = $stateParams.id;
                InsuranceService
                    .getByCustomer(customer)
                    .then((response) => {
                        $scope.insurances = response.data;
                    });

                InsuranceService
                    .nextIndex('CLAIMS')
                    .then((response) => {
                        $scope.claim.docNo = response.data.result;
                    });
                CustomerService
                    .getExtendedCustomerInfo(customer)
                    .then((response) => {
                        $scope.claim.customer = angular.copy(response.data);
                        $scope.greenLight = true;
                    });
            });

            $scope.doSave = () => {
                let claim = angular.copy($scope.claim);
                claim.creationDate = new Date(claim.creationDate);
                claim.endDate = new Date(claim.endDate);
                claim.beginningDate = new Date(claim.beginningDate);
                claim.dueDate = new Date(claim.dueDate);
                claim.prime = numberFormatter(claim.prime);
                claim.balance = numberFormatter(claim.balance);

                $scope.loading = true;
                InsuranceService
                    .Claim()
                    .save(claim)
                    .then(() => {
                        NotificationService.successMsj();
                        $state.go("customer.detail.step_six.list", {id: $stateParams.id});
                    }, (errorResp) => {
                        NotificationService.errorMsj(errorResp.data.message);
                    })
                    .finally(() => {
                        $scope.loading = false;
                    });
            };

            $scope.validateMe = function () {
                InsuranceService
                    .validateNumber($scope.claim.docNo, 'CLAIMS')
                    .then(function () {
                        $("#docNo").removeClass("has-error");
                    }, function () {
                        $("#docNo").addClass("has-error");
                    })
            }
        });