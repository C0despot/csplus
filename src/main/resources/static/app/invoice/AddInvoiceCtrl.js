angular
    .module(APP)
    .controller('AddInvoiceCtrl',
        function ($scope, $state, $stateParams, InvoiceService, InsuranceService, NotificationService, CustomerService) {

            $scope.edit = true;
            $scope.hidden = true;
            $scope.loading = false;
            $scope.greenLight = false;
            $scope.invoice = {
                invoiceNo: null,
                customer: null,
                insurance: null,
                startDate: new Date(),
                endDate: new Date(),
                beginningDate: new Date(),
                dueDate: nextYear(),
                prime: 0
            };
            $scope.updatePrime = () => {
                if ($scope.invoice.insurance) {
                    $scope.invoice.prime = angular.copy($scope.invoice.insurance.prime);
                }
                $scope.invoice.beginningDate = new Date($scope.invoice.insurance.startDate);
                $scope.invoice.dueDate = new Date($scope.invoice.insurance.endDate);
            };
            $scope.insurances = [];
            $scope.$on('$viewContentLoaded', () => {
                let customer = $stateParams.id;
                let insurance = $stateParams.insurance;

                InsuranceService
                    .getOne(customer, insurance)
                    .then((response) => {
                        let insurance = response.data;
                        $scope.insurances.push(insurance);
                        $scope.invoice.insurance = insurance;
                        $scope.invoice.prime = insurance.prime;
                    });

                InsuranceService
                    .nextIndex('INVOICE')
                    .then((response) => {
                        $scope.invoice.invoiceNo = response.data.result;
                    });
                CustomerService
                    .getExtendedCustomerInfo(customer)
                    .then((response) => {
                        $scope.invoice.customer = angular.copy(response.data);
                    });
            });

            $scope.doSave = () => {
                let invoice = angular.copy($scope.invoice);
                invoice.startDate = new Date(invoice.startDate);
                invoice.endDate = new Date(invoice.endDate);
                invoice.beginningDate = new Date(invoice.beginningDate);
                invoice.dueDate = new Date(invoice.dueDate);
                invoice.prime = numberFormatter(invoice.prime);
                invoice.balance = numberFormatter(invoice.balance);

                $scope.loading = true;
                InvoiceService
                    .save(invoice)
                    .then(() => {
                        NotificationService.successMsj();
                        $state.go("customer.detail.step_three.list", {id: $stateParams.id});
                    }, (errorResp) => {
                        NotificationService.errorMsj(errorResp.data.message);
                    })
                    .finally(() => {
                        $scope.loading = false;
                    });
            };

            $scope.validateMe = function () {
                InsuranceService
                    .validateNumber($scope.invoice.invoiceNo, 'INVOICE')
                    .then(function () {
                        $("#invoiceNo").removeClass("has-error");
                    }, function () {
                        $("#invoiceNo").addClass("has-error");
                    })
            }
        });
