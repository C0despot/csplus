angular
    .module(APP)
    .controller('UserCtrl', function ($scope, NotificationService, $state, UserService, $uibModal) {

        $scope.users = [];

        $scope.pager = {
            page: 0,
            size: 10,
            sort: DESC,
            orderBy: 'id',
            totalPages: 0
        };

        $scope.filter = {
            operation: ':',
            value: '',
            key: 'name'
        };

        $scope.$on('$viewContentLoaded', () => {
            UserService
                .validateAuthorities()
                .then((ignored) => {
                    },
                    () => {
                        $state.go("error.unauthorized");
                    });
            UserService
                .loadUsers($scope.pager, $scope.filter)
                .then((response) => {
                    $scope.users = angular.copy(response.data.content);
                    $scope.pager.totalPages = response.data.totalPages;
                }, () => {
                    NotificationService.errorMsj();
                })
        });


        $scope.addNewModal = () => {
            let modalInstance = $uibModal.open({
                templateUrl: 'app/user/modal/user.html',
                controller: 'CreateUserModalCtrl',
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                windowClass: "animated fadeIn",
            });

            modalInstance.result.then((resp) => {
                if (resp)
                    $state.reload();
            })
        };

        $scope.editUserModal = (user) => {
            let modalInstance = $uibModal.open({
                templateUrl: 'app/user/modal/user.html',
                controller: 'EditUserModalCtrl',
                backdrop: 'static',
                keyboard: false,
                size: 'lg',
                windowClass: "animated fadeIn",
                resolve: {
                    User: function () {
                        return user;
                    }
                }
            });

            modalInstance.result.then((resp) => {
                if (resp)
                    $state.reload();
            })
        };

        $scope.$watch('pager.page', () => {
            $scope.pageHelper = $scope.pager.page + 1;
            $scope.loadResult();
        });

        $scope.setPage = () => {
            $scope.pager.page = $scope.pageHelper - 1;
        };

        $scope.applyFilter = function (event) {
            let field = event.target.id;
            let value = event.target.value;
            $scope.doFilter(field, value);
        };
        $scope.getPct = function (numerator, denominator) {
            if (denominator === 0 || !denominator)
                return '-';
            else if (numerator === 0 || !numerator)
                return '0%';
            else return (numerator * 100 / denominator).toFixed(2) + "%";
        };

        $scope.doFilter = function (field, value) {
            $scope.filter.key = field;
            $scope.filter.value = value;
            $scope.pager.orderBy = field;
            $scope.pager.page = 0;
            $scope.loadResult();
        };

        $scope.loadResult = () => {
            let pager = $scope.pager;
            let filter = $scope.filter;

            UserService.loadUsers(pager, filter)
                .then((response) => {
                    $scope.users = angular.copy(response.data.content);
                    $scope.pager.totalPages = response.data.totalPages;
                }, () => {
                    NotificationService.errorMsj();
                })
        };

    });