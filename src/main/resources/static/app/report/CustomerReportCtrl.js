angular
    .module(APP)
    .controller('CustomerReportCtrl', function ($scope, NotificationService, ReportService, CustomerService, $window) {

        $scope.target = true;
        $scope.data = null;
        $scope.url = `/TEMPORAL.PDF?access_token=${getAccessToken()}`;
        $scope.window = $window;
        $scope.printing = false;
        $scope.customers = [];
        $scope.customer = {
            selected: null
        };

        $scope.toggle = function (scope) {
            scope.toggle();
        };


        $scope.refreshCustomers = function (value) {
            return CustomerService
                .filter(value)
                .then((response) => {
                    $scope.customers = response.data;
                });
        };

        $scope.loadDocuments = (customer) => {
            $scope.data = null;
            ReportService
                .customerCurrentReport(customer.clientId)
                .then((response) => {
                    if (response.data.length > 0)
                        $scope.data = angular.copy(response.data);
                    else NotificationService.errorMsj('El cliente no cuenta con pólizas activas actualmente.');
                })
        };

        $scope.printAllForCurrent = () => {
            $scope.printing = true;
            ReportService
                .printReport('CURRENT', $scope.customer.selected.clientId)
                .then((resp) => {
                    $scope.window.open("/report/pdf/" + resp.data.url + '?access_token=' + getAccessToken());
                })
                .finally(() => $scope.printing = false
                )
        };

        $scope.printOne = (id) => {
            $scope.printing = true;
            ReportService
                .printReport('CURRENT', $scope.customer.selected.clientId, id)
                .then((resp) => {
                    $scope.window.open("/report/pdf/" + resp.data.url + '?access_token=' + getAccessToken());
                })
                .finally(() => $scope.printing = false
                )
        }
    });