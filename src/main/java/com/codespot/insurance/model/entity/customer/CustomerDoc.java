package com.codespot.insurance.model.entity.customer;

import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.listener.ReadOnlyEntity;
import com.codespot.insurance.security.model.type.Types;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "CUSTOMER_DOCUMENTS_VW")
@EntityListeners(value = ReadOnlyEntity.class)
public class CustomerDoc {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "DOCUMENT")
    private String document;

    @ManyToOne
    @JoinColumn(name = "INSURANCE")
    private Insurance insurance;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER")
    private Customer customer;

    @Column(name = "DOC_TYPE")
    @Enumerated(value = EnumType.STRING)
    private Types.CustomerDocType type;

    public String getSpnType() {
        return type.spn();
    }

    public String customerAsStr() {
        if (Objects.nonNull(customer))
            return customer.toString();
        else if (Objects.nonNull(insurance))
            return insurance.getCustomer().toString();
        return "0";
    }

    public String insuranceAsStr() {
        if (Objects.nonNull(insurance))
            return insurance.toString();
        return "0";
    }

    public String planeDoc() {
        return this.id.replaceAll("[^\\d.]", "");
    }
}
