angular
    .module(APP)
    .controller('LogCtrl', function ($scope, NotificationService, $state, UserService, $uibModal) {

        $scope.logs = [];

        $scope.pager = {
            page: 0,
            size: 10,
            sort: DESC,
            orderBy: 'date',
            totalPages: 0
        };

        $scope.filter = {
            operation: ':',
            value: '',
            key: 'message'
        };

        $scope.$on('$viewContentLoaded', () => {
            let user = JSON.parse(localStorage.getItem("user"));
            if (user.role !== 'ADMIN')
                $state.go("error.unauthorized");
        });


        $scope.$watch('pager.page', () => {
            $scope.pageHelper = $scope.pager.page + 1;
            $scope.loadResult();
        });

        $scope.setPage = () => {
            $scope.pager.page = $scope.pageHelper - 1;
        };

        $scope.applyFilter = function (event) {
            let field = event.target.id;
            let value = event.target.value;
            $scope.doFilter(field, value);
        };

        $scope.getPct = function (numerator, denominator) {
            if (denominator === 0 || !denominator)
                return '-';
            else if (numerator === 0 || !numerator)
                return '0%';
            else return (numerator * 100 / denominator).toFixed(2) + "%";
        };

        $scope.doFilter = function (field, value) {
            $scope.filter.key = field;
            $scope.filter.value = value;
            $scope.pager.orderBy = field;
            $scope.pager.page = 0;
            $scope.loadResult();
        };

        $scope.loadResult = () => {
            let pager = $scope.pager;
            let filter = $scope.filter;

            UserService
                .logs(pager, filter)
                .then((response) => {
                    $scope.logs = angular.copy(response.data.content);
                    $scope.pager.totalPages = response.data.totalPages;
                }, () => {
                    NotificationService.errorMsj();
                })
        };

    });