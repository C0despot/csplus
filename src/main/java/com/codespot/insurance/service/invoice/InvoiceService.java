package com.codespot.insurance.service.invoice;

import com.codespot.insurance.model.dto.RenewDto;
import com.codespot.insurance.model.entity.invoice.Invoice;

import java.util.List;

public interface InvoiceService {

    List<Invoice> getAllCustomerInvoice(Long customer, Long insurance);

    Invoice getOne(Long invoice);

    Invoice getOneWithCustomerId(Long customer, Long invoice);

    Invoice save(Invoice invoice);

    Invoice renew(RenewDto renew);

    void updateBalances();
}
