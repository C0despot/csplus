package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.dto.CustomerDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.CustomerDoc;
import com.codespot.insurance.model.entity.insurance.InsuranceDetail;
import com.codespot.insurance.service.customer.CustomerService;
import com.codespot.insurance.service.insurance.InsuranceService;
import com.codespot.insurance.web.resource.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping(value = "customer")
public class CustomerController {

    private final
    CustomerService customerService;

    private final
    InsuranceService insuranceService;

    @Autowired
    public CustomerController(CustomerService customerService, InsuranceService insuranceService) {
        this.customerService = customerService;
        this.insuranceService = insuranceService;
    }

    @GetMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.GET_EXTENDED_CUSTOMER_INFO)
    public ResponseEntity<CustomerDto> getExtendedCustomerInfo(@RequestParam(name = "customer") Long id) {
        CustomerDto customer = this.customerService.customerInfo(id);
        return ResponseEntity.ok(customer);
    }

    @PostMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.SAVE_CUSTOMER_BASIC_INFO)
    public ResponseEntity saveBasicInfo(@RequestBody Customer customer) throws NoSuchFieldException, IllegalAccessException {
        customer = this.customerService.save(customer);
        return ResponseEntity.ok(customer);
    }

    @GetMapping(value = "plans")
    @Logger(transaction = Log.Transaction.GET_ALL_CUSTOMER_INSURANCES)
    public ResponseEntity<List<InsuranceDetail>> getPlans(@RequestParam(name = "customer") Long customer) {
        List<InsuranceDetail> insuranceDetails = this.insuranceService.getAll(customer);
        return ResponseEntity.ok(insuranceDetails);
    }

    @GetMapping(value = "invoices")
    @Logger(transaction = Log.Transaction.GET_ALL_CUSTOMER_INVOICES)
    public ResponseEntity<List<InsuranceDetail>> getInvoices(@RequestParam(name = "customer") Long customer) {
        List<InsuranceDetail> insuranceDetails = this.insuranceService.getAll(customer);
        return ResponseEntity.ok(insuranceDetails);
    }

    @GetMapping(value = "filter")
    @Logger(transaction = Log.Transaction.GET_ALL_CUSTOMER_BY_FILTER)
    public ResponseEntity filter(@RequestParam(name = "q") String criteria) {
        List<Customer> customers = this.customerService.filter(criteria);
        return ResponseEntity.ok(customers);
    }

    @GetMapping(value = "docs")
    @Logger(transaction = Log.Transaction.GET_ALL_CUSTOMER_DOCS_BY_FILTER)
    public ResponseEntity<List<CustomerDoc>> filterDocs(@RequestParam(name = "q") String criteria, Principal principal) {
        List<CustomerDoc> customers = this.customerService.filterDocs(criteria);
        return ResponseEntity.ok(customers);
    }

    @PostMapping(value = "redirect")
    @Logger(transaction = Log.Transaction.BUILD_REDIRECT_URI)
    public ResponseEntity<String> buildRedirectUrl(@RequestBody CustomerDoc doc) {
        String redirectUri;
        switch (doc.getType()) {
            case CUSTOMER:
                redirectUri = String.format("/#/customer/%s/general-info/edition", doc.planeDoc());
                break;
            case INSURANCE:
                redirectUri = String.format("/#/customer/%s/insurance/%s/edit", doc.customerAsStr(), doc.planeDoc());
                break;
            case INVOICE:
                redirectUri = String.format("/#/customer/%s/invoices/%s/edit", doc.customerAsStr(), doc.planeDoc());
                break;
            case DEPENDENT:
                redirectUri = String.format("/#/customer/%s/insurance/%s/edit", doc.customerAsStr(), doc.insuranceAsStr());
                break;
            case CREDIT:
                redirectUri = String.format("/#/customer/%s/credit/%s/edition", doc.customerAsStr(), doc.planeDoc());
                break;
            case RECEIPT:
                redirectUri = String.format("/#/customer/%s/receipt/%s/edition", doc.customerAsStr(), doc.planeDoc());
                break;
            case CLAIM:
                redirectUri = String.format("/#/customer/%s/claim/%s/edition", doc.customerAsStr(), doc.planeDoc());
                break;
            default:
                redirectUri = "";
        }

        return ResponseEntity.ok(String.format("{\"url\": \"%s\"}", redirectUri));

    }
}
