package com.codespot.insurance.service.claim;

import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.payment.Claim;
import com.codespot.insurance.model.repository.ClaimRepository;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.condos.shared.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service(value = "defaultClaimService")
public class DefaultClaimService implements ClaimService {

    private final
    ClaimRepository claimRepository;

    private final
    UserService userService;

    private final
    LoggerActivityService activityService;

    @Autowired
    public DefaultClaimService(ClaimRepository claimRepository, UserService userService, LoggerActivityService activityService) {
        this.claimRepository = claimRepository;
        this.userService = userService;
        this.activityService = activityService;
    }

    @Override
    public List<Claim> getAllByCustomer(Long customerId) {
        return this.claimRepository.findAllByCustomerClientId(customerId);
    }

    @Override
    public List<Claim> getAllByInsurance(Long insurance) {
        return this.claimRepository.findAllByInsuranceId(insurance);
    }

    @Override
    public Claim getOne(Long claimId) {
        return this.claimRepository.findById(claimId)
                .orElseThrow(() -> new NotFoundException(String.format("Reclamo [%s] no pudo ser encontrada!", claimId.toString()), claimId.toString(), LogActivity.Scope.CLAIM.name(), LogActivity.Action.READ.name()));
    }

    @Override
    public Claim save(Claim claim) {
        User loggedUser = this.userService.loggedUser();
        if (Objects.isNull(claim.getId())) {
            claim.setCreationDate(new Date());
            claim.setCreatedBy(loggedUser);
            if (Objects.nonNull(claim.getInsurance()))
                claim.setCarrier(claim.getInsurance().getCarrier());
        }
        claim.setModificationDate(new Date());
        claim.setModifiedBy(loggedUser);

        String message = "MODIFICANDO INFORMACION DE RECLAMO, %s.";
        LogActivity.Action action = LogActivity.Action.MODIFY;

        if (claim.isNew()) {
            message = "AGREGANDO NUEVO RECLAMO, %s.";
            action = LogActivity.Action.CREATE;
        }

        claim = this.claimRepository.save(claim);
        activityService.success(String.format(message, claim.getDocNo()), LogActivity.Scope.CLAIM, action, claim.getId().toString());
        return claim;
    }

    @Override
    public void remove(Claim claim) {
        this.claimRepository.delete(claim);
    }

    @Override
    public void remove(List<Claim> claims) {
        this.claimRepository.deleteAll(claims);
    }
}
