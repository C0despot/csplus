package com.codespot.insurance.security.service.user;


import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.spec.UserSpecification;
import com.codespot.insurance.security.model.PasswordChange;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.repository.UserRepository;
import com.codespot.insurance.service.file.FileService;
import com.codespot.insurance.service.mail.MailService;
import com.condos.shared.data.page.C2Criteria;
import com.condos.shared.exception.BadRequestException;
import com.condos.shared.exception.NotFoundException;
import com.condos.shared.exception.UserAlreadyExistsException;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Service(value = "defaultUserService2")
public class DefaultUserService implements UserService {

    private final
    UserRepository userRepository;

    private final
    MailService mailService;

    private final
    BCryptPasswordEncoder bCryptPasswordEncoder;

    private final
    FileService fileService;

    @Autowired
    public DefaultUserService(UserRepository userRepository, MailService mailService, BCryptPasswordEncoder bCryptPasswordEncoder, FileService fileService) {
        this.userRepository = userRepository;
        this.mailService = mailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.fileService = fileService;
    }

    @Override
    public Page<User> findAll(PageDto page, C2Criteria criteria) {
        UserSpecification specification = UserSpecification.of(criteria);
        Pageable pageable = PageRequest.of(page.getPage(), page.getSize(), new Sort(page.getSort(), page.getOrderBy()));
        return this.userRepository.findAll(specification, pageable);
    }

    @Override
    public User findById(Long id) {
        return this.userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Usuario [%s] no pudo ser encontrada!", id.toString()), id.toString(), LogActivity.Scope.USER.name(), LogActivity.Action.READ.name()));
    }

    @Override
    public User loggedUser() {
        String id = SecurityContextHolder.getContext().getAuthentication().getName();
        return findById(Long.parseLong(id));
    }

    @Override
    public boolean isValidPassword(String password) {
        User user = loggedUser();
        return bCryptPasswordEncoder.matches(password, user.getPassword());
    }

    @Override
    public User findByUuid(String uuid) {
        return this.userRepository.findByUuidEquals(uuid)
                .orElseThrow(() -> new NotFoundException(String.format("Usuario [%s] no pudo ser encontrada!", uuid), uuid, LogActivity.Scope.USER.name(), LogActivity.Action.READ.name()));
    }

    @Override
    public User findByName(String name) {
        User user = this.userRepository.findUser(name);
        user.setPassword(null);
        return user;
    }

    @Override
    public User deleteUserById(Long id) {
        return null;
    }

    @Override
    public Boolean isUserExist(User user) {
        User userRegistered = this.userRepository.findUser(user.getUsername(), user.getEmail());
        if (userRegistered == null)
            return false;
        else if (user.getUsername().equalsIgnoreCase(userRegistered.getUsername()))
            throw new UserAlreadyExistsException(String.format("Username [%s] ya esta en uso", userRegistered.getUsername()), user.getUsername(), LogActivity.Scope.USER.name(), LogActivity.Action.CREATE.name());
        else if (user.getEmail().equalsIgnoreCase(userRegistered.getEmail()))
            throw new UserAlreadyExistsException(String.format("Email [%s] ya esta en uso", userRegistered.getEmail()), user.getEmail(), LogActivity.Scope.USER.name(), LogActivity.Action.CREATE.name());
        return true;
    }

    @Override
    @Transactional
    public User saveUser(User user) throws TemplateException, IOException, MessagingException {
        user.setValidated(false);
        if (user.isNew()) {
            user.setCreatedDate(new Date());
            user.setUuid(UUID.randomUUID().toString());
            user.setRole(user.getRole());
            user.setValidated(false);
            user.setLocked(false);
            String password = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 15);
            user.setPassword(bCryptPasswordEncoder.encode(password));
            user = this.userRepository.save(user);
            mailService.sendWelcomeEmail(user, password);
        } else {
            User fromDb = findById(user.getId());
            user.setPassword(fromDb.getPassword());
        }
        user.setModifiedDate(new Date());
        user = this.userRepository.save(user);

        return user;
    }

    @Override
    @Transactional
    public User updateUser(User user, Long id) {
        User loggedUser = findById(id);
        loggedUser.setUsername(user.getUsername());
        loggedUser.setEmail(user.getEmail());
        loggedUser.setBirthDate(user.getBirthDate());
        loggedUser.setGender(user.getGender());
        loggedUser.setPhone(user.getPhone());
        loggedUser.setName(user.getName());
        loggedUser = this.userRepository.save(loggedUser);
        return loggedUser;
    }

    @Override
    public User updateProfilePicture(MultipartFile file, Long userId) {
        User user = loggedUser();
        String s3Upload = fileService.s3Upload(file);
        user.setImage(fileService.fullS3Url() + "/" + s3Upload);
        return this.userRepository.save(user);
    }

    @Override
    public User changePassword(PasswordChange passwordChange) {
        if (!passwordChange.getNewPsw().equals(passwordChange.getConfirm()))
            throw new BadRequestException("Contraseñas no  coinciden, para el usuario: " + passwordChange.getUser().getName(), passwordChange.getUser().getName(), LogActivity.Scope.USER.name(), LogActivity.Action.MODIFY.name());
        User user = this.findById(passwordChange.getUser().getId());
        if (!bCryptPasswordEncoder.matches(passwordChange.getCurrent(), user.getPassword()))
            throw new BadRequestException("Contraseña actual no coincide, para el usuario: " + passwordChange.getUser().getName(), passwordChange.getUser().getName(), LogActivity.Scope.USER.name(), LogActivity.Action.MODIFY.name());
        user.setPassword(bCryptPasswordEncoder.encode(passwordChange.getNewPsw().toLowerCase()));
        return this.userRepository.save(user);
    }

    @Override
    public User setPassword(PasswordChange passwordChange) {
        if (!passwordChange.getNewPsw().equals(passwordChange.getConfirm()))
            throw new BadRequestException("Contraseñas no coinciden, para el usuario: " + passwordChange.getUser().getName(), passwordChange.getUser().getName(), LogActivity.Scope.USER.name(), LogActivity.Action.MODIFY.name());
        User user = this.findById(passwordChange.getUser().getId());
        user.setPassword(bCryptPasswordEncoder.encode(passwordChange.getNewPsw().toLowerCase()));
        user.setValidated(true);
        return this.userRepository.save(user);
    }

    @Override
    public void resetPassword(String email) throws TemplateException, IOException, MessagingException {
        String password = (UUID.randomUUID().toString().replaceAll("-", "").substring(0, 15)).toLowerCase();
        User user = findByName(email);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setValidated(false);
        user = userRepository.save(user);
        mailService.sendResetPasswordEmail(user, password);
    }

    @Override
    public void restorePassword(String email) {
        User user = this.userRepository.findUser(email);
        if (user == null)
            throw new NotFoundException(String.format("Usuario [%s] no pudo ser encontrada!", email), email, LogActivity.Scope.USER.name(), LogActivity.Action.MODIFY.name());
    }

    @Override
    public User loggedUserOrNull() {
        try {
            return loggedUser();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String password(Long id) {
        return this.userRepository.password(id);
    }
}