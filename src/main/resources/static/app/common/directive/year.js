angular
    .module(APP)
    .directive('ngYear', ['$filter', function (filter) {
        return {
            scope: false,
            require: '?ngModel',
            link: function (scope, elem, attrs) {
                scope.$watch(attrs.ngModel, function (newVal) {
                    if (scope.greenLight) {
                        let value = new Date(newVal);
                        value.setFullYear(value.getFullYear() + 1);
                        let attArr = attrs.ngYear.split(".");
                        if (attArr.length === 2) {
                            scope[attArr[0]][attArr[1]] = angular.copy(value);
                        }
                    }
                })
            }
        };
    }]);