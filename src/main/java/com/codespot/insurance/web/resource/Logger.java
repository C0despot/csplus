package com.codespot.insurance.web.resource;

import com.codespot.insurance.model.entity.Log;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Logger {
    Log.Transaction transaction() default Log.Transaction.ANOTHER;

    boolean isHttp() default true;

}
