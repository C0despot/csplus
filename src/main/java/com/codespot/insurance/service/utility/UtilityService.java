package com.codespot.insurance.service.utility;

import com.codespot.insurance.web.resource.Transaction;

public interface UtilityService {

    boolean validateConstrains(String value, Transaction transaction);

    long nextSequence(Transaction transaction);
}
