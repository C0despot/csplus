angular
    .module(APP)
    .controller('ProfileCtrl', function ($scope, NotificationService, $state, UserService) {
        $scope.user = JSON.parse(localStorage.getItem("user"));
        $scope.incomes = {};
        $scope.loading = false;
        $scope.password = {
            current: '',
            confirm: '',
            newPsw: '',
            user: $scope.user
        };

        $scope.$on('$viewContentLoaded', () => {
            let id = $scope.user.id;
            $scope.incomes = UserService
                .getIncomes(id)
                .then((resp) => {
                    $scope.incomes = resp.data;
                });
        });

        $scope.doChangePassword = () => {
            let password = angular.copy($scope.password);

            if (password.newPsw !== password.confirm) {
                NotificationService.errorMsj('Ambas contraseñas deben ser iguales');
                return;
            }

            $scope.loading = true;
            UserService.doChangePassword(password)
                .then(() => {
                        NotificationService.successMsj();
                        $state.reload();
                    },
                    () => NotificationService.errorMsj())
                .finally(() => $scope.loading = false)
        };

        $scope.loadImage = () => {
            $("#fileInput").click();
        };

        $("#fileInput").change(function (e) {
            e.preventDefault();
            let photo = document.getElementById("fileInput").files[0];  // file from input
            UserService.changeUserImage(photo)
                .then((response) => {
                    localStorage.setItem("user", JSON.stringify(response.data));
                    $state.reload();
                }, () => {

                })
        });

    });