package com.codespot.insurance.model.entity.payment;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.type.Types;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "RECEIPT")
public class Receipt {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "RECEIPT_NUMBER")
    private String receiptNo;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "POLICY")
    private Insurance insurance;

    @ManyToOne
    @JoinColumn(name = "INVOICE_NUMBER")
    private Invoice invoice;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "PAYMENT_WAY")
    private String payment;

    @Column(name = "PAYMENT_TYPE")
    private String paymentType;

    @Column(name = "CURRENCY")
    @Enumerated(value = EnumType.STRING)
    private Types.Currency currency;

    @Column(name = "CREDIT_CARD")
    private String lastFour;

    @Column(name = "CONCEPT")
    private String concept;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @ManyToOne
    @JoinColumn(name = "USER_EDITOR")
    private User user;

    @Transient
    private boolean selected = false;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Override
    public String toString() {
        return this.id.toString();
    }

    @PrePersist
    void onPrePersist() {
        setCreatedBy(getModifiedBy());
        setCreationDate(new Date());
        setModificationDate(new Date());
    }

    @PreUpdate
    void onPreUpdate() {
        setModificationDate(new Date());
    }

    public boolean isNew() {
        return id == null;
    }
}
