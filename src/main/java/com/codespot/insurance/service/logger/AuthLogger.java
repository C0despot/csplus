package com.codespot.insurance.service.logger;

import com.codespot.insurance.model.entity.LogActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class AuthLogger implements AuthenticationEventPublisher {

    private final
    LoggerActivityService activityService;

    @Autowired
    public AuthLogger(LoggerActivityService activityService) {
        this.activityService = activityService;
    }

    @Override
    public void publishAuthenticationSuccess(Authentication authentication) {
        if (authentication instanceof UsernamePasswordAuthenticationToken)
            activityService.success("inicio de sesión, ".concat(authentication.getName()), LogActivity.Scope.ACCESS, LogActivity.Action.ACCESS, authentication.getName());
    }

    @Override
    public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
        activityService.warning("Login fallido, ".concat(authentication.getName()), LogActivity.Scope.ACCESS, LogActivity.Action.ACCESS, authentication.getName());
    }
}
