angular
    .module(APP)
    .controller('CustomerNewCtrl', function ($scope, $state, CustomerService, DataService, NotificationService) {


        $scope.isAddMode = true;
        $scope.title = 'General Information';
        $scope.customer = {
            type: 'Persona Física',
            status: 'Activo'
        };
        $scope.edit = true;
        $scope.loading = false;

        $scope.save = function () {
            let customer = DataService.trimFields(['idCard', 'rnc', 'cellPhone', 'homePhone', 'workPhone'], angular.copy($scope.customer));
            $scope.loading = true;
            CustomerService
                .saveBasicInfo(customer)
                .then(function (response) {
                    NotificationService.successMsj();
                    $state.go('customer.detail.step_one.edit', {id: response.data.clientId})
                }, function (errorResp) {
                    NotificationService.errorMsj(errorResp.data.message);
                })
                .finally(function () {
                    $scope.loading = false;
                });
        }

    });