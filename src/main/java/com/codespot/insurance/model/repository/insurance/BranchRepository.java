package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.insurance.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchRepository extends JpaRepository<Branch, Long> {
}
