package com.codespot.insurance.model.spec;

import com.codespot.insurance.security.model.entity.User;
import com.condos.shared.data.page.C2Criteria;
import com.condos.shared.util.DataUtil;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UserSpecification implements Specification<User> {

    private C2Criteria criteria;

    public static UserSpecification of(C2Criteria criteria) {
        return new UserSpecification(criteria);
    }

    private UserSpecification(C2Criteria criteria){
        this.criteria = criteria;
    }
    @Override
    public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        return DataUtil.getPredicate(root, criteriaBuilder, criteria);
    }
}
