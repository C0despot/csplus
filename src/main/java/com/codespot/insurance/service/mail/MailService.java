package com.codespot.insurance.service.mail;

import com.codespot.insurance.security.model.entity.User;
import freemarker.template.TemplateException;

import javax.mail.MessagingException;
import java.io.IOException;

public interface MailService {

    void sendWelcomeEmail(User user, String rawPassword) throws TemplateException, MessagingException, IOException;
    void sendResetPasswordEmail(User user, String rawPassword) throws TemplateException, MessagingException, IOException;
}
