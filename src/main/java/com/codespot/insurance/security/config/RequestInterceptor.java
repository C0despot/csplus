package com.codespot.insurance.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.logging.Logger;

@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {

    private final
    Logger logger = Logger.getLogger(RequestInterceptor.class.getName());

    private final String[] urls;

    private final
    TokenStore tokenStore;

    @Autowired
    public RequestInterceptor(TokenStore tokenStore) {
        this.urls = new String[]{
                "/oauth/token",
                "/oauth/revoke",
                "/error",
                "/util/version",
                "/user/password/reset",
                "/index.html",
                "/js**",
                "/app**",
                "/css**",
                "/user**",
                "/font-awesome**",
                "/views**",
                "/img**",
                "/fonts**",
        };

        this.tokenStore = tokenStore;
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object object) throws IOException {
        String token = request.getParameter("access_token");

        Optional<String> optional = Arrays.stream(urls)
                .filter(x -> request.getRequestURI()
                        .replace("/insurance/", "")
                        .replace("/", "").equals(x.replace("/", "")))
                .findFirst();

        if (optional.isPresent()) {
            return true;
        } else {
            optional = Arrays.stream(urls)
                    .filter(x -> (request.getRequestURI() + "**")
                            .replace("/insurance/", "")
                            .replace("/", "")
                            .contains(
                                    x.replace("/", "")
                                            .replace("**", "")
                            )
                    )
                    .findFirst();
            if (optional.isPresent())
                return true;
        }
        if (token == null) {
            response.sendError(401, "User not authenticated");
            logger.severe(request.getMethod() + " Attempt not authorized to URL: " + request.getRequestURI());
            return false;
        } else {
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(token);
            if (accessToken == null) {
                response.sendError(401, "Token expired or revoked");
                logger.severe(request.getMethod() + " Attempt not authorized to URL: " + request.getRequestURI());
                return false;
            }
            return true;
        }
    }
}
