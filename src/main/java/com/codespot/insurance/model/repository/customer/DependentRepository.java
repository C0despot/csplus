package com.codespot.insurance.model.repository.customer;

import com.codespot.insurance.model.entity.customer.Dependent;
import com.codespot.insurance.model.entity.insurance.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DependentRepository extends JpaRepository<Dependent, Long> {
    List<Dependent> findAllByInsuranceEquals(Insurance insurance);
}
