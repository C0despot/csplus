package com.codespot.insurance.model.dto;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.Dependent;
import com.codespot.insurance.model.entity.insurance.Branch;
import com.codespot.insurance.model.entity.insurance.InsuranceCarrier;
import com.codespot.insurance.model.entity.insurance.Local;
import com.codespot.insurance.model.entity.insurance.Vehicle;
import com.codespot.insurance.security.model.entity.User;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class InsuranceDto {

    private Long id;

    private Customer customer;

    private String insuranceNumber;

    private Branch branch;

    private InsuranceCarrier carrier;

    private String currency;

    private Date startDate;

    private Date endDate;

    private String status;

    private Double prime;

    private String notes;

    private String localPlace;

    private String businessType;

    private String buildingType;

    private String bigNote;

    private Date modifiedDate;

    private Date createdDate;

    private List<Vehicle> vehicles;

    private List<Local> locals;

    private List<Dependent> dependents;

    private User modifiedBy;

    private User createdBy;
}
