package com.codespot.insurance.model.dto;

import lombok.Data;

import java.util.Calendar;

@Data
public class MonthlyReportDto {

    private Integer insurance;
    private Integer expiredInsurance;
    private Integer invoice;
    private Integer expiredInvoice;
    private Integer moneyMade;
    private Integer cashMade;
    private String month;

    public MonthlyReportDto(Integer insurance, Integer expiredInsurance) {
        this.insurance = insurance;
        this.expiredInsurance = expiredInsurance;
    }

    public MonthlyReportDto(Integer insurance, Integer expiredInsurance, Integer invoice, Integer expiredInvoice) {
        this.insurance = insurance;
        this.expiredInsurance = expiredInsurance;
        this.invoice = invoice;
        this.expiredInvoice = expiredInvoice;
    }

    public MonthlyReportDto(Integer insurance,
                            Integer expiredInsurance,
                            Integer invoice,
                            Integer expiredInvoice,
                            Integer moneyMade,
                            Integer cashMade,
                            String month) {
        this.insurance = insurance;
        this.expiredInsurance = expiredInsurance;
        this.invoice = invoice;
        this.expiredInvoice = expiredInvoice;
        this.moneyMade = moneyMade;
        this.cashMade = cashMade;
        this.month = month;
    }
}
