package com.codespot.insurance.model.repository;

import com.codespot.insurance.model.entity.payment.Claim;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClaimRepository extends JpaRepository<Claim, Long> {

    List<Claim> findAllByCustomerClientId(Long id);

    List<Claim> findAllByInsuranceId(Long id);

    Claim findByDocNoEquals(String value);
}
