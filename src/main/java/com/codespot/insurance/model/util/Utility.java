package com.codespot.insurance.model.util;

import lombok.experimental.UtilityClass;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@UtilityClass
public class Utility {

    public boolean isValidId(Long id) {
        return (id != null && !id.equals(0L));
    }

    public String capitalize(String sentence) {
        if (Objects.nonNull(sentence)) {
            List<String> words = Arrays.asList(sentence.toLowerCase().split(" "));
            StringBuilder sb = new StringBuilder();
            words.forEach(word -> {
                if (Objects.nonNull(word) && word.length() > 0)
                    sb.append(Character.toUpperCase(word.charAt(0))).append(word.substring(1)).append(" ");
            });
            return sb.toString().trim();
        }
        return "";
    }

    public String getCurrencyValue(Double amount) {
        if (Objects.isNull(amount))
            amount = 0D;
        return String.format("%,.2f", amount);
    }

    public double doubleValue(Object o) {
        if (Objects.isNull(o))
            return 0;
        return Double.parseDouble(o.toString());
    }

    public String phoneList(String... phones) {
        StringBuilder builder = new StringBuilder();
        Arrays.asList(phones).forEach(phone -> {
            if (Objects.nonNull(phone)) {
                builder.append(phone);
                builder.append(", ");
            }
        });
        return builder.toString().substring(0, builder.toString().length() - 2);
    }
}
