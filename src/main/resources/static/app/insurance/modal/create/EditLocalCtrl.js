angular
    .module('inspinia')
    .controller('EditLocalCtrl', function ($scope, $uibModalInstance, $stateParams, InsuranceService, local) {

        $scope.uibModalInstance = $uibModalInstance;
        $scope.localTitle = "Loading...";
        $scope.local = {};
        $scope.row = {};
        $scope.goods = [];
        $scope.isEditMode = true;
        setTimeout(function () {
            $scope.local = local;
            $scope.localTitle = `Edición de Local`.toLowerCase();
            $scope.goods = JSON.parse(local.goods);
            $scope.$apply();
        }, 100);

        $scope.addRow = function (row) {
            row.securedSum = numberFormatter(row.securedSum);
            $scope.goods.push(angular.copy(row));
            $scope.row = {};
        };

        $scope.save = function () {
          let local = angular.copy($scope.local);
          local.goods = JSON.stringify($scope.goods);
          $uibModalInstance.close(local)
        };
    });