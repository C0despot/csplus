angular
    .module(APP)
    .controller('EditUserModalCtrl', function ($scope, $uibModalInstance, UserService, NotificationService, User) {

        $scope.userTitle = 'Agregar Usuario';
        $scope.isEditMode = true;
        $scope.loading = false;
        $scope.uibModalInstance = $uibModalInstance;
        $scope.user = {
            role: 'EMPLOYEE',
            status: 'ACTIVE',
            birthDate: yearsBefore(18)
        };

        (() => {
            $scope.user = angular.copy(User);
        })();
        $scope.saveUser = () => {
            let user = angular.copy($scope.user);
            user.birthDate = new Date(user.birthDate);
            $scope.loading = true;
            UserService
                .save(user)
                .then(ignored => {
                    NotificationService.successMsj();
                    $uibModalInstance.close(true);
                }, (errorResp) => {
                    NotificationService.errorMsj(errorResp.data.message);
                })
                .finally(() => $scope.loading = false)
        };


        $scope.$watch("user.birthDate", date => {
            date = new Date(date);
            let now = new Date();
            if (now.getUTCMonth() > date.getUTCMonth())
                $scope.user.age = Math.abs(now.getUTCFullYear() - date.getUTCFullYear());
            else if (now.getUTCMonth() === date.getUTCMonth() && now.getUTCDate() > date.getUTCDate())
                $scope.user.age = Math.abs(now.getUTCFullYear() - date.getUTCFullYear());
            else
                $scope.user.age = Math.abs(now.getUTCFullYear() - date.getUTCFullYear() - 1);

        });
    });