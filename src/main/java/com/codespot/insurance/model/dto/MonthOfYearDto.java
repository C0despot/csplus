package com.codespot.insurance.model.dto;

import com.codespot.insurance.security.model.type.Types;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MonthOfYearDto {

    private Types.Month month;
    private Integer year;

    public String getName() {
        if (month.isAll())
            return String.format("%s", month.spanish());
        return String.format("%s, %s", month.spanish(), year.toString());
    }
}
