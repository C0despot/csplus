angular
    .module(APP)
    .controller('NewReceiptCtrl',
        function ($scope,
                  $state,
                  $stateParams,
                  ReceiptService,
                  InvoiceService,
                  InsuranceService,
                  NotificationService,
                  CustomerService) {

            $scope.edit = true;
            $scope.hidden = true;
            $scope.loading = false;
            $scope.greenLight = false;
            $scope.validating = false;
            $scope.receipt = {
                receiptNo: null,
                customer: null,
                startDate: new Date(),
                endDate: new Date(),
                beginningDate: new Date(),
                dueDate: nextYear(),
                invoice: {
                    insurance: {
                        insuranceNumber: null
                    }
                },
                payment: '0',
                paymentType: '0',
                user: getUser()
            };
            $scope.insurances = [];
            $scope.$on('$viewContentLoaded', () => {
                let customer = $stateParams.id;

                CustomerService
                    .getExtendedCustomerInfo(customer)
                    .then((response) => {
                        $scope.receipt.customer = angular.copy(response.data);
                    });
                InvoiceService
                    .allByCustomer(customer)
                    .then((response) => {
                        $scope.invoices = response.data;
                    });
                InsuranceService
                    .getByCustomer(customer)
                    .then((response) => {
                        $scope.insurances = response.data;
                        $scope.greenLight = true;
                    });

                InsuranceService
                    .nextIndex('RECEIPT')
                    .then((response) => {
                        $scope.receipt.receiptNo = response.data.result;
                    });
            });

            $scope.validateAmount = () => {
                if (!$scope.receipt.invoice)
                    return false;

                $scope.receipt.insurance = angular.copy($scope.receipt.invoice.insurance);
                let balance = angular.copy($scope.receipt.invoice.balance);
                let amount = angular.copy($scope.receipt.amount);
                balance = numberFormatter(balance);
                amount = numberFormatter(amount);
                if (balance < amount)
                    $("#amount").addClass('has-error');
                else
                    $("#amount").removeClass('has-error');
            };

            $scope.doSave = () => {
                let receipt = angular.copy($scope.receipt);
                receipt.startDate = new Date(receipt.startDate);
                receipt.endDate = new Date(receipt.endDate);

                receipt.amount = numberFormatter(receipt.amount);

                $scope.loading = true;
                ReceiptService
                    .saveOne(receipt)
                    .then(() => {
                        NotificationService.successMsj();
                        $state.go("customer.detail.step_four.list", {id: $stateParams.id});
                    }, (response) => {
                        NotificationService.errorMsj(response.data.message);
                    })
                    .finally(() => {
                        $scope.loading = false;
                    });
            };

            $scope.validateMe = () => {
                $scope.validating = true;
                InsuranceService
                    .validateNumber($scope.receipt.receiptNo, 'RECEIPT')
                    .then(() => {
                        $("#receiptNo").removeClass("has-error");
                    }, () => {
                        $("#receiptNo").addClass("has-error");
                    })
                    .finally(() => {
                        $scope.validating = false;
                    })
            }
        });