angular
    .module('inspinia')
    .controller('GoodsController', function ($scope, $uibModalInstance, $stateParams, InsuranceService, vehicles, NotificationService) {

        $scope.user = JSON.parse(localStorage.getItem("user"));
        $scope.$uibModalInstance = $uibModalInstance;
        $scope.GoodTitle = "Loading...";
        $scope.GoodTitle = "Lista de bienes asegurados";
        $scope.goods = angular.copy(vehicles);

        $scope.editModal = function (row) {
            $uibModalInstance.close({vehicle: row});
        };

        $scope.newModal = function () {
            $uibModalInstance.close({isNew: true});
        };

        $scope.showTooltip = function (plan) {
            NotificationService.showCoverage(plan);
        };

        $scope.delete = (id) => {
            NotificationService
                .confirm(
                    "¿Borrar vehículo?",
                    "Está seguro que desea borrar el local seleccionado?",
                    () => {
                        InsuranceService
                            .deleteElement(id, 'VEHICLE')
                            .then((ignored) => {
                                location.reload();
                            }, (ignored) => {
                            })
                    })
        };
    });