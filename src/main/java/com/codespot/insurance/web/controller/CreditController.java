package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.payment.Credit;
import com.codespot.insurance.service.credit.CreditService;
import com.codespot.insurance.web.resource.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "credit")
public class CreditController {

    private final
    CreditService creditService;

    @Autowired
    public CreditController(CreditService creditService) {
        this.creditService = creditService;
    }

    @GetMapping(value = "all")
    @Logger(transaction = Log.Transaction.GET_ALL_CREDIT_BY_CUSTOMER)
    public ResponseEntity<List<Credit>> getAllByCustomer(@RequestParam(value = "q") Long customer) {
        List<Credit> credits = this.creditService.findAllByCustomer(customer);
        return ResponseEntity.ok(credits);
    }

    @PostMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.SAVE_CREDIT_DOCUMENT)
    public ResponseEntity<Credit> saveOne(@RequestBody Credit credit) {
        credit = this.creditService.save(credit);
        return ResponseEntity.ok(credit);
    }

    @GetMapping(value = "one")
    @Logger(transaction = Log.Transaction.GET_CREDIT_INFO_BY_DOCUMENT)
    public ResponseEntity<Credit> getOne(@RequestParam(value = "document") Long docId) {
        Credit credit = this.creditService.getOne(docId);
        return ResponseEntity.ok(credit);
    }
}
