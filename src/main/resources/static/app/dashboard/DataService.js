(function () {
    'use strict';
    angular.module('inspinia')
        .service('DataService', DataService);
})();

DataService.$inject = ['$http', 'AuthService'];

function DataService($http, AuthService) {

    return {
        monthlyReport: function () {
            return $http.get(`stats/monthly?access_token=${getAccessToken()}&id=${AuthService.getUser().id}`);
        },
        loadCustomerData: function (pager, filter) {
            let pagerQs = `&size=${pager.size}&page=${pager.page}&sort=${pager.sort}&orderBy=${pager.orderBy}`;
            let filterQs = `&operation=${filter.operation}&value=${filter.value}&key=${filter.key}`;
            return $http.get(`stats/personal?access_token=${getAccessToken() + pagerQs + filterQs}`);
        },
        format: function (value, pattern) {
            let i = 0, v = value.toString();
            return pattern.replace(/9/g, () => v[i++]);
        },
        currentMonth: (index = '') => {
            let qs = `?access_token=${getAccessToken()}&index=${index}`;
            return $http.get("util/month/current" + qs);
        },
        getAvailableMonths: () => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.get("util/month/available" + qs);
        },
        moneyExchange: (exchange) => {
            let qs = `?access_token=${getAccessToken()}&value=${exchange.value}`;
            return $http.get(`stats/exchange/${exchange.from}/${exchange.to}/money` + qs);
        },
        trimFields: function (fieldArr, object) {
            let obj = angular.copy(object);
            fieldArr.forEach(function (field) {
                if (obj[field])
                    obj[field] = obj[field]
                        .split(" ")
                        .join("")
                        .split("(")
                        .join("")
                        .split(")")
                        .join("")
                        .split("Ext.")
                        .join("");
            });
            return obj;
        }
    }
}