package com.codespot.insurance.report.model;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.CustomerCurrentReport;
import com.codespot.insurance.model.entity.invoice.InvoiceReport;
import com.codespot.insurance.model.entity.payment.PaymentDocumentReport;
import com.codespot.insurance.model.util.Utility;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfOutputIntent;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DottedBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.zugferd.ZugferdConformanceLevel;
import com.itextpdf.zugferd.ZugferdDocument;
import lombok.NoArgsConstructor;
import org.apache.pdfbox.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Component
@NoArgsConstructor
@SuppressWarnings(value = "Duplicates")
public class AccountStatusReport {

    @Value(value = "${report.fonts.color}")
    private String ICC;

    @Value(value = "${report.fonts.regular}")
    private String REGULAR;

    @Value(value = "${report.fonts.bold}")
    private String BOLD_FONT;


    private final String NEWLINE = "\n";

    private final String COMPANY_NAME = "Gabriel del Rosario & Asociados, SRL.";

    public File buildPdf(List<CustomerCurrentReport> report) throws IOException {
        String url = "temp.pdf";
        new FileOutputStream(url).close();
        File file = new File(url);

        InputStream icc = new BufferedInputStream(new URL(ICC).openStream());
        InputStream regular = new BufferedInputStream(new URL(REGULAR).openStream());
        byte[] bytes = IOUtils.toByteArray(regular);

        ZugferdDocument pdfDocument = PdfDocument.of(file.getPath(), icc);
        PageSize pageSize = new PageSize(594, 841);
        AtomicReference<Document> document = new AtomicReference<>(new Document(pdfDocument, pageSize));
        document.get().setFont(PdfFontFactory.createFont(bytes, true)).setFontSize(12);
        document.get().add(getCompanyName());
        document.get().add(getAddress(report.get(0).getCustomer()));
        report.forEach(currentReport -> document.set(printInsuranceReport(currentReport, document.get())));
        document.get().close();
        return new File(url);
    }

    private Document printInsuranceReport(CustomerCurrentReport report, Document document) {
        document.add(getInsuranceHeader());
        document.add(printInsuranceBody(report));

        report.getInvoices().forEach(invoiceReport -> {
            document.add(getInvoiceHeader());
            document.add(printInvoiceBody(invoiceReport));
            invoiceReport
                    .getPaymentDocuments()
                    .forEach(paymentDocumentReport -> {
                        document.add(getPaymentDocHeader());
                        document.add(printPaymentBody(paymentDocumentReport));
                    });
        });
        return document;
    }

    private String getDateString(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return sdf.format(d);
    }

    private Table getCompanyName() {
        return TableBuilder
                .of(100f)
                .setBold()
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setBorder(Border.NO_BORDER)
                .setFontSize(12)
                .addCell(createCell(COMPANY_NAME)
                        .setHorizontalAlignment(HorizontalAlignment.CENTER)
                        .setTextAlignment(TextAlignment.CENTER));
    }

    private Table getInsuranceHeader() {
        return TableBuilder
                .of(30f, 20f, 20f, 15f, 15f)
                .setBorder(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(10)
                .addCell(createCell(Header.Insurance.INSURANCE_NUMBER)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.Insurance.START_DATE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.Insurance.END_DATE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.Insurance.BALANCE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.Insurance.CURRENCY)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)));
    }

    private Table printInsuranceBody(CustomerCurrentReport report) {
        return TableBuilder
                .of(30f, 20f, 20f, 15f, 15f)
                .setBorder(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                .setBorderTop(Border.NO_BORDER)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(10)
                .addCell(createCell(report.getInsurance())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(getDateString(report.getStartDate()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(getDateString(report.getEndDate()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(getCurrencyValue(report.getBalance()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(report.getCurrency().name())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)));
    }


    private Table getInvoiceHeader() {
        return TableBuilder
                .withDimension(97, 20f, 16f, 17f, 17f, 15f, 15f)
                .setBorder(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                .setBorderTop(Border.NO_BORDER)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(10)
                .addCell(createCell(Header.Invoice.INVOICE_NUMBER)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(Header.Invoice.MOV_DATE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(Header.Invoice.START_DATE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(Header.Invoice.END_DATE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(Header.Invoice.BALANCE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(Header.Invoice.PENDING)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)));
    }


    private Table printInvoiceBody(InvoiceReport report) {
        return TableBuilder
                .withDimension(97, 20f, 16f, 17f, 17f, 15f, 15f)
                .setBorder(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                .setBorderTop(Border.NO_BORDER)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(10)
                .addCell(createCell(report.getInvoice())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(getDateString(report.getEmissionDate()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(getDateString(report.getStartDate()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(getDateString(report.getEndDate()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(getCurrencyValue(report.getPrime()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)))
                .addCell(createCell(getCurrencyValue(report.getBalance()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.SUCCESS, 0.75f)));
    }


    private Table getPaymentDocHeader() {
        return TableBuilder
                .withDimension(94, 25f, 25f, 25f, 25f)
                .setBorder(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                .setBorderTop(Border.NO_BORDER)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(10)
                .addCell(createCell(Header.PaymentDoc.DOC_TYPE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)))
                .addCell(createCell(Header.PaymentDoc.DOC_NUM)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)))
                .addCell(createCell(Header.PaymentDoc.MOV_DATE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)))
                .addCell(createCell(Header.PaymentDoc.AMOUNT)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)));
    }


    private Table printPaymentBody(PaymentDocumentReport report) {
        return TableBuilder
                .withDimension(94, 25f, 25f, 25f, 25f)
                .setBorder(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                .setBorderTop(Border.NO_BORDER)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(10)
                .addCell(createCell(report.getType().name())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)))
                .addCell(createCell(report.getDocument())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)))
                .addCell(createCell(getDateString(report.getEmissionDate()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)))
                .addCell(createCell(getCurrencyValue(report.getAmount()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.WARNING, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.WARNING, 0.75f)));
    }


    private Table getAddress(Customer customer) {
        StringBuilder builder = new StringBuilder();
        builder.append("Avenida libertad No. 62")
                .append(NEWLINE)
                .append("Centro, San Cristobal.")
                .append(NEWLINE)
                .append("Tel. (809) 528 2145")
                .append(NEWLINE)
                .append("RNC: 114001623");

        String colOne = builder.toString();

        builder = new StringBuilder();
        builder.append(Utility.capitalize(customer.getName()))
                .append(NEWLINE)
                .append(Utility.capitalize(customer.getAddress()))
                .append(NEWLINE)
                .append("Tel. ")
                .append(customer.getPhone());

        String colTwo = builder.toString();

        return TableBuilder
                .of(40f, 60f)
                .setMarginTop(30)
                .setMarginBottom(50)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setBorder(Border.NO_BORDER)
                .setFontSize(11)
                .addCell(createCell(colOne)
                        .setTextAlignment(TextAlignment.LEFT))
                .addCell(createCell(colTwo)
                        .setTextAlignment(TextAlignment.RIGHT));
    }

    private Cell createCell(String text) {
        return new Cell().setPadding(0.8f)
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(text)
                        .setMultipliedLeading(1));
    }

    private String getCurrencyValue(Double amount) {
        return String.format("%,.2f", amount);
    }

    private static class TableBuilder {

        public static Table of(Float... columns) {
            UnitValue[] unitValues = new UnitValue[columns.length];
            for (int i = 0; i < columns.length; i++) {
                unitValues[i] = new UnitValue(UnitValue.PERCENT, columns[i]);
            }
            return new Table(unitValues).setWidth(UnitValue.PERCENT).setWidth(520);
        }

        public static Table withDimension(float x, Float... columns) {
            float width = (x / 100) * 520;
            float margin = ((100 - x) / 100) * 520;
            UnitValue[] unitValues = new UnitValue[columns.length];
            for (int i = 0; i < columns.length; i++) {
                unitValues[i] = new UnitValue(UnitValue.PERCENT, columns[i]);
            }
            return new Table(unitValues).setWidth(UnitValue.PERCENT).setWidth(width).setMarginLeft(margin);
        }
    }

    private static class PdfDocument {
        public static ZugferdDocument of(String dest, InputStream icc) throws FileNotFoundException {
            String identifier = "Custom";
            String registry = "http://www.color.org";
            String fontInfo = "sRGB IEC61966-2.1";
            return new ZugferdDocument(new PdfWriter(dest), ZugferdConformanceLevel.ZUGFeRDBasic, new PdfOutputIntent(identifier, null, registry, fontInfo, icc));
        }
    }

}
