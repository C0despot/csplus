angular
    .module('inspinia')
    .controller('DashboardCtrl', function ($scope, DataService, $state) {

        $scope.customers = [];
        $scope.pageHelper = 0;
        $scope.renewObj = {
            insurance: 0,
            expiredInsurance: 0,
            invoice: 0,
            expiredInvoice: 0,
            moneyMade: 0,
            cashMade: 0,
            month: ''
        };

        $scope.today = new Date();
        $scope.pager = {
            page: 0,
            size: 10,
            sort: DESC,
            orderBy: 'id',
            totalPages: 0
        };

        $scope.filter = {
            operation: ':',
            value: '',
            key: 'name'
        };

        $scope.currentMonth = "ENERO";

        $scope.highlightResult = () => {
            let key = $scope.filter.key;
            let value = $scope.filter.value;
            if (key === 'name' && value && value.length > 0)
                $('.cname').each(function () {
                    let text = $(this).html();
                    text = text.replace(value.toUpperCase(), `<span style="background-color:Yellow" >${value.toUpperCase()}</span>`);
                    $(this).html(text);
                });
        };

        $scope.$on('$viewContentLoaded', function () {
            DataService
                .monthlyReport()
                .then(function (response) {
                    $scope.renewObj = response.data;
                }, function (error) {

                });
            DataService.currentMonth()
                .then(function (response) {
                    $scope.currentMonth = response.data;
                });
            $scope.loadResult();
        });

        $scope.$watch('pager.page', function () {
            $scope.pageHelper = $scope.pager.page + 1;
            $scope.loadResult();
        });

        $scope.setPage = function () {
            $scope.pager.page = $scope.pageHelper - 1;
        };

        $scope.applyFilter = function (event) {
            let field = event.target.id;
            let value = event.target.value;
            $scope.doFilter(field, value);
        };
        $scope.getPct = function (numerator, denominator) {
            if (denominator === 0 || !denominator)
                return '-';
            else if (numerator === 0 || !numerator)
                return '0%';
            else return (numerator * 100 / denominator).toFixed(2) + "%";
        };

        $scope.doFilter = function (field, value) {
            $scope.filter.key = field;
            $scope.filter.value = value;
            $scope.pager.orderBy = field;
            $scope.pager.page = 0;
            $scope.loadResult();
        };

        $scope.loadResult = function () {
            let pager = $scope.pager;
            let filter = $scope.filter;
            DataService
                .loadCustomerData(pager, filter)
                .then(function (response) {
                    $scope.pager.totalPages = response.data.totalPages;
                    $scope.customers = angular.copy(response.data.content);
                    setTimeout(() => $scope.highlightResult(), 50);
                }, function (error) {

                })
        };

        $scope.viewDetails = function (id) {
            $state.go("customer.detail.step_one", {id: id})
        }
    });