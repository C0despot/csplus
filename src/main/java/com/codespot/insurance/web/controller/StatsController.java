package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.dto.MonthlyReportDto;
import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.customer.CustomerCurrentReport;
import com.codespot.insurance.model.entity.customer.CustomerDetail;
import com.codespot.insurance.model.entity.customer.CustomerHistoricalReport;
import com.codespot.insurance.security.model.type.Types;
import com.codespot.insurance.service.stats.DefaultStatisticService;
import com.codespot.insurance.service.stats.StatisticService;
import com.codespot.insurance.service.stats.command.StatsCommand;
import com.codespot.insurance.web.resource.Logger;
import com.condos.shared.data.page.C2Criteria;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(value = "stats")
public class StatsController {

    private final
    StatisticService statisticService;

    @Autowired
    public StatsController(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @GetMapping(value = "monthly")
    @Logger(transaction = Log.Transaction.STATS_MONTHLY_REPORT)
    public ResponseEntity<MonthlyReportDto> monthlyReport(@RequestParam(name = "id") Long userId) {
        MonthlyReportDto report = this.statisticService.getMonthlyReport(userId);
        return ResponseEntity.ok(report);
    }

    @GetMapping(value = "personal")
    @Logger(transaction = Log.Transaction.GET_CUSTOMER_PERSONAL_STATS)
    public ResponseEntity<Page<CustomerDetail>> personalStats(C2Criteria criteria, PageDto pageDto) {
        Page<CustomerDetail> customerDetails = this.statisticService.getCustomerBasicDetails(criteria, pageDto);
        return ResponseEntity.ok(customerDetails);
    }

    @GetMapping(value = "report/current")
    @Logger(transaction = Log.Transaction.CUSTOMER_CURRENT_STATUS_REPORT)
    public ResponseEntity currentReport(Long customer) {
        if (Objects.isNull(customer))
            return ResponseEntity.unprocessableEntity().body("Something missing.");
        List<CustomerCurrentReport> report = this.statisticService.getCurrentReport(customer);
        return ResponseEntity.ok(report);
    }

    @GetMapping(value = "report/historical")
    @Logger(transaction = Log.Transaction.CUSTOMER_HISTORICAL_STATUS_REPORT)
    public ResponseEntity historicalReport(Long customer) {
        if (Objects.isNull(customer))
            return ResponseEntity.unprocessableEntity().body("Something missing.");
        List<CustomerHistoricalReport> report = this.statisticService.getHistoricalReport(customer);
        return ResponseEntity.ok(report);
    }

    @PostMapping(value = "report/print")
    @Logger(transaction = Log.Transaction.PRINT_CUSTOMER_STATUS_REPORT)
    public ResponseEntity report(@RequestParam(value = "target") Types.Report reportType,
                                 Long number, Long customer) throws IOException {
        String url = this.statisticService.printReport(customer, number, reportType);
        return ResponseEntity.ok(String.format("{\"url\": \"%s\"}", url));
    }

    @GetMapping(value = "incomes")
    @Logger(transaction = Log.Transaction.GET_COMPANY_INCOMES)
    public ResponseEntity incomes(@NonNull Long userId) {
        DefaultStatisticService.Incomes incomes = this.statisticService.getIncomes(userId);
        return ResponseEntity.ok(incomes);
    }

    @GetMapping(value = "exchange/{from}/{to}/money")
    @Logger(transaction = Log.Transaction.MONEY_EXCHANGE)
    public ResponseEntity incomes(@NonNull Double value,
                                  @PathVariable StatsCommand.Exchange.Currency from,
                                  @PathVariable StatsCommand.Exchange.Currency to) {
        StatsCommand.Exchange exchanged = this.statisticService.moneyExchange(value, from, to);
        return ResponseEntity.ok(exchanged);
    }


}
