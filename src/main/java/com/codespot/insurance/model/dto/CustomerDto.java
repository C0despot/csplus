package com.codespot.insurance.model.dto;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.security.model.entity.User;
import lombok.Data;

import java.util.Date;

@Data
public class CustomerDto {

    private Long clientId;

    private String firstName;

    private String lastName;

    private String idCard;

    private String company;

    private String businessContact;

    private String rnc;

    private String address;

    private String sector;

    private String city;

    private String cellPhone;

    private String homePhone;

    private String workPhone;

    private String phone;

    private String name;

    private String email;

    private String customerContact;

    private String type;

    private String notes;

    private String status;

    private Double balance;

    private Customer.Locality locality;

    private Date modificationDate;

    private Date creationDate;

    private User modifiedBy;

    private User createdBy;

}
