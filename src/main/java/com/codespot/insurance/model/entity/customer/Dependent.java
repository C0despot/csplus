package com.codespot.insurance.model.entity.customer;

import com.codespot.insurance.model.entity.insurance.Insurance;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "DEPENDENT")
public class Dependent {

    @Id
    @Column(name = "DEPENDENT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "POLICY_LINKED")
    private Insurance insurance;

    @Column(name = "DEPENDENT_NAME")
    private String name;

    @Column(name = "DEPENDENT_AGE")
    private Short age;

    @Column(name = "FILIAL_NUMBER")
    private String filialNo;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "PLAN_TYPE")
    private String planType;

    @Column(name = "VALUE")
    private Double value;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "ID_CARD")
    private String idCard;

    @Column(name = "PARENT")
    private String parent;

    @Column(name = "SEX")
    private Character gender;

    @Column(name = "BIRTH_DATE")
    private Date birthDate;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "PLAN")
    private String plan;


    public static Dependent dummy(Long element) {
        Dependent dependent = new Dependent();
        dependent.setId(element);
        return dependent;
    }
}
