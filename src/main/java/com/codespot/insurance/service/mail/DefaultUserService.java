package com.codespot.insurance.service.mail;

import com.codespot.insurance.security.model.entity.User;
import com.condos.shared.util.ApiUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

@Service(value = "defaultUserService")
public class DefaultUserService implements MailService {


    private final
    JavaMailSender emailSender;

    private final
    Configuration freemarkerConfig;

    @Autowired
    public DefaultUserService(Configuration freemarkerConfig, JavaMailSender emailSender) {
        this.freemarkerConfig = freemarkerConfig;
        this.emailSender = emailSender;
    }

    @Override
    public void sendWelcomeEmail(User user, String password) throws TemplateException, MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        HashMap<String, String> model = new HashMap<>();
        model.put("username", user.getUsername());
        model.put("user", user.getName());
        model.put("password", password);
        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/");
        Template t = freemarkerConfig.getTemplate("templates/welcome_email.ftl");
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
        helper.setTo(user.getEmail());
        helper.setText(text, true);
        helper.setSubject(ApiUtil.Codec.capitalize("Bienvenido..!"));
        message.setFrom(new InternetAddress("info@gabrielyasociado.com"));

        emailSender.send(message);
    }

    public void sendResetPasswordEmail(User user, String password) throws TemplateException, MessagingException, IOException {

        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message);

        HashMap<String, String> model = new HashMap<>();
        model.put("user", user.getName());
        model.put("username", user.getUsername());
        model.put("password", password);
        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/");
        Template t = freemarkerConfig.getTemplate("templates/reset_password_email.ftl");
        String text = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
        helper.setTo(user.getEmail());
        helper.setText(text, true);
        helper.setSubject(ApiUtil.Codec.capitalize("Restaurar contraseña"));
        message.setFrom(new InternetAddress("info@gabrielyasociado.com"));

        emailSender.send(message);
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class EmailRequest {
        private String email;
        private String name;
        private String token;
    }
}
