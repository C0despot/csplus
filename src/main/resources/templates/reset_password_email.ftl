<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html!important; charset=UTF-8"/>
    <title>Set up a new password for Condos App</title>
    <!--
    The style block is collapsed on page load to save you some scrolling.
    Postmark automatically inlines all CSS properties for maximum email client
    compatibility. You can just update styles here, and Postmark does the rest.
    -->
    <style type="text/css" media="all">

        *:not(br):not(tr):not(html) {
            font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif !important;
            box-sizing: border-box !important;
        }

        body {
            width: 100% !important;
            height: 100% !important;
            margin: 0 !important;
            line-height: 1.4 !important;
            background-color: #F2F4F6 !important;
            color: #74787E !important;
            -webkit-text-size-adjust: none !important;
        }

        p,
        ul,
        ol,
        blockquote {
            line-height: 1.4 !important;
            text-align: left !important;
        }

        a {
            color: #3869D4 !important;
        }

        a img {
            border: none !important;
        }

        td {
            word-break: break-word !important;
        }

        /* Layout ------------------------------ */

        .email-wrapper {
            width: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            -premailer-width: 100% !important;
            -premailer-cellpadding: 0 !important;
            -premailer-cellspacing: 0 !important;
            background-color: #F2F4F6 !important;
        }

        .email-content {
            width: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            -premailer-width: 100% !important;
            -premailer-cellpadding: 0 !important;
            -premailer-cellspacing: 0 !important;
        }

        /* Masthead ----------------------- */

        .email-masthead {
            padding: 25px 0 !important;
            text-align: center !important;
        }

        .email-masthead_name {
            font-size: 22px !important;
            font-weight: bold !important;
            color: #bbbfc3 !important;
            text-decoration: none !important;
            text-shadow: 0 1px 0 white !important;
        }

        /* Body ------------------------------ */

        .email-body {
            width: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            -premailer-width: 100% !important;
            -premailer-cellpadding: 0 !important;
            -premailer-cellspacing: 0 !important;
            border-top: 1px solid #EDEFF2 !important;
            border-bottom: 1px solid #EDEFF2 !important;
            background-color: #FFFFFF !important;
        }

        .email-body_inner {
            width: 570px !important;
            margin: 0 auto !important;
            padding: 0 !important;
            -premailer-width: 570px !important;
            -premailer-cellpadding: 0 !important;
            -premailer-cellspacing: 0 !important;
            background-color: #FFFFFF !important;
        }

        .email-footer {
            width: 570px !important;
            margin: 0 auto !important;
            padding: 0 !important;
            -premailer-width: 570px !important;
            -premailer-cellpadding: 0 !important;
            -premailer-cellspacing: 0 !important;
            text-align: center !important;
        }

        .email-footer p {
            color: #AEAEAE !important;
        }

        .body-action {
            width: 100% !important;
            margin: 30px auto !important;
            padding: 0 !important;
            -premailer-width: 100% !important;
            -premailer-cellpadding: 0 !important;
            -premailer-cellspacing: 0 !important;
            text-align: center !important;
        }

        .body-sub {
            margin-top: 25px !important;
            padding-top: 25px !important;
            border-top: 1px solid #EDEFF2 !important;
        }

        .content-cell {
            padding: 35px !important;
        }

        .preheader {
            display: none !important;
            visibility: hidden !important;
            mso-hide: all !important;
            font-size: 1px !important;
            line-height: 1px !important;
            max-height: 0 !important;
            max-width: 0 !important;
            opacity: 0 !important;
            overflow: hidden !important;
        }

        .social td {
            padding: 0 !important;
            width: auto !important;
        }

        .purchase_heading p {
            margin: 0 !important;
            color: #9BA2AB !important;
            font-size: 12px !important;
        }

        .align-center {
            text-align: center !important;
        }

        /*Media Queries ------------------------------ */

        @media only screen and (max-width: 600px) {
            .email-body_inner,
            .email-footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }

        /* Buttons ------------------------------ */

        .button {
            background-color: #3869D4 !important;
            border: 10px solid #3869D4 !important;
            border-right-width: 18px !important;
            border-left-width: 18px !important;
            display: inline-block !important;
            color: #FFF !important;
            text-decoration: none !important;
            border-radius: 3px !important;
            box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16) !important;
            -webkit-text-size-adjust: none !important;
        }

        .button--green {
            background-color: #22BC66 !important;
            border: 10px solid #22BC66 !important;
            border-right-width: 18px !important;
            border-left-width: 18px !important;
            margin: 5px;
        }

        .button--blue {
            background-color: #3869D4 !important;
            border: 10px solid #3869D4 !important;
            border-right-width: 18px !important;
            border-left-width: 18px !important;
            margin: 5px;
        }

        h1 {
            margin-top: 0 !important;
            color: #2F3133 !important;
            font-size: 19px !important;
            font-weight: bold !important;
            text-align: left !important;
        }

        h2 {
            margin-top: 0 !important;
            color: #2F3133 !important;
            font-size: 16px !important;
            font-weight: bold !important;
            text-align: left !important;
        }

        h3 {
            margin-top: 0 !important;
            color: #2F3133 !important;
            font-size: 14px !important;
            font-weight: bold !important;
            text-align: left !important;
        }

        p {
            margin-top: 0 !important;
            color: #74787E !important;
            font-size: 16px !important;
            line-height: 1.5em !important;
            text-align: left !important;
        }

        p.sub {
            font-size: 12px !important;
            cursor: pointer !important;
        }
    </style>
</head>
<body>
<span class="preheader">Solicitud de nueva contraseña</span>
<table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="email-masthead">
                        <a href="http://gabrielyasociado.com"
                           target="_blank"
                           class="email-masthead_name">
                            Gabriel del Rosario y Asociados, SRL.
                        </a>
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td class="email-body" width="100%" cellpadding="0" cellspacing="0">
                        <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell">
                                    <h1>Hola ${user},</h1>
                                    <p>
                                        Hemos recibido una solicitud para cambiar tu clave de acceso a nuestra aplicacion,
                                        <a href="http://gabrielyasociado.com">Gabriel del Rosario & Asociados, SRL.</a>
                                        A continuación tendrás el acceso para poder iniciar sesión dentro de la
                                        aplicación nuevamente.
                                        <strong>
                                            Es recomendable que inmediatamente hayas iniciado sesión cambies de
                                            contraseña.
                                        </strong>
                                    </p>
                                    <table class="body-action" align="center" width="100%" cellpadding="0"
                                           cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <!-- Border based button
                                           https://litmus.com/blog/a-guide-to-bulletproof-buttons-in-email-design -->
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <a class="button button--blue">
                                                                        ${username}
                                                                        </a>
                                                                    </td>
                                                                    <td>
                                                                        <a class="button button--green">
                                                                        ${password}
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <p>
                                        Este correo fue generado desde nuestro
                                        <a href="http://gabrielyasociado.com/tech">Centro de Comunicación
                                            Electrónica</a>,
                                        y está destinado únicamente a la persona interesada. Si no ha solicitado
                                        una cuenta para registrarse simplemente puede ignorar este correo,
                                        o puede contactarnos a nuestro
                                        <a href="http://gabrielyasociado.com/support">Centro de Soporte</a>
                                        para cualquier infomación.
                                    </p>
                                    <p>Gracias,
                                        <br>El Equipo de Gabriel del Rosario y Asociados</p>
                                    <!-- Sub copy -->
                                    <table class="body-sub">
                                        <tr>
                                            <td>
                                                <p class="sub">
                                                    Si está teniendo problemas con las credenciales, comuniquese al
                                                    <a href="http://gabrielyasociado.com/tech">Centro de Comunicación
                                                        Electrónica</a>.
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="content-cell" align="center">
                                    <p class="sub align-center">
                                        Derechos Reservados:
                                        <strong>
                                            Gabriel del Rosario y Asociados, SRL. ©2017-2018.
                                        </strong>
                                    </p>
                                    <p class="sub align-center">
                                        Powered by: <strong>José A. Rodriguez L.</strong>
                                        <br>
                                        Nicolás Ureña de Mendoza, num. 61, Los Prados
                                        <br>
                                        Apartamento 1E.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>