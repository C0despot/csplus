package com.codespot.insurance.model.entity.customer;

import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.type.Types;
import com.condos.shared.annotation.RequiredField;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "CUSTOMER")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "CLIENT_NAME")
    @RequiredField(descr = "NOMBRE", dependsOn = "company")
    private String firstName;

    @Column(name = "CLIENT_LAST_NAME")
    @RequiredField(descr = "APELLIDO", dependsOn = "company")
    private String lastName;

    @Column(name = "CLIENT_ID_CARD")
    @RequiredField(descr = "CÉDULA o PASAPORTE", dependsOn = "rnc")
    private String idCard;

    @Column(name = "CLIENT_BUSS_NAME")
    @RequiredField(descr = "COMPAÑÍA", dependsOn = "firstName")
    private String company;

    @Column(name = "CLIENT_BUSS_CONTACT")
    private String businessContact;

    @Column(name = "CLIENT_BUSS_RNC")
    @RequiredField(descr = "RNC", dependsOn = "idCard")
    private String rnc;

    @Column(name = "ADDRESS")
    @RequiredField(descr = "DIRECCIÓN")
    private String address;

    @Column(name = "SECTOR")
    private String sector;

    @Column(name = "CITY")
    private String city;

    @Column(name = "PHONE_1")
    @RequiredField(descr = "TELÉFONO", dependsOn = {"homePhone", "workPhone"})
    private String cellPhone;

    @Column(name = "PHONE_2")
    @RequiredField(descr = "TELÉFONO", dependsOn = {"cellPhone", "workPhone"})
    private String homePhone;

    @Column(name = "PHONE_3")
    @RequiredField(descr = "TELÉFONO", dependsOn = {"homePhone", "cellPhone"})
    private String workPhone;

    @Column(name = "PHONE_NUMBER_3")
    @RequiredField(descr = "TELÉFONO")
    private String phone;

    @Column(name = "FULL_NAME")
    private String name;

    @Column(name = "CLIENT_EMAIL")
    private String email;

    @Column(name = "CLIENT_CONTACT")
    private String customerContact;

    @Column(name = "CLIENT_TYPE")
    @Enumerated(value = EnumType.STRING)
    @RequiredField(descr = "TIPO DE CLIENTE")
    private Types.Customer type;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "ACCOUNT_STATE")
    @RequiredField(descr = "TIPO DE LA CUENTA")
    private String status;

    @Column(name = "LOCALITY")
    @Enumerated(value = EnumType.STRING)
    @RequiredField(descr = "LOCALIDAD DE LA EMPRESA QUE DIGITA")
    private Locality locality;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Override
    public String toString() {
        return this.clientId.toString();
    }

    public boolean isEnterprise() {
        return Types.Customer.EMPRESA.equals(type);
    }

    public enum Locality {
        HAINA, SANCRI
    }

    public boolean isNew() {
        return clientId == null;
    }
}
