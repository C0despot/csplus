package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.insurance.InsuranceRenew;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface InsuranceRenewRepository extends JpaRepository<InsuranceRenew, Long>, JpaSpecificationExecutor<InsuranceRenew> {
    Page<InsuranceRenew> findAllByYearAndMonth(Integer year, Integer month, Pageable pageable);
}
