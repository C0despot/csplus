(function () {
    'use strict';
    angular.module('inspinia')
        .service('AuthService', AuthService);
})();

AuthService.$inject = ['$http'];

function AuthService($http) {
    let tokenUrl = 'oauth/token';
    return {
        getToken: function (user) {
            let qs = `?grant_type=password&username=${user.username}&password=${user.password}`;
            return $http.post(tokenUrl + qs, {});
        },
        getUser: function () {
            return JSON.parse(localStorage.getItem("user"));
        },
        getAccessToken: function () {
            let token = localStorage.getItem("token");
            if (token)
                return (JSON.parse(token)).access_token;
            return "none"
        },
        getUserInfo: function (user) {
            let qs = `?access_token=${this.getAccessToken()}`;
            return $http.get(`user/${user.username + qs}`);
        },
        logout: function () {
            let qs = `?access_token=${this.getAccessToken()}`;
            return $http.get(`oauth/revoke/${qs}`);
        }
    }
}