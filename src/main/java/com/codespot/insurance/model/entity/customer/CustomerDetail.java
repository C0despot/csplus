package com.codespot.insurance.model.entity.customer;

import com.codespot.insurance.model.listener.ReadOnlyEntity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CUSTOMER_VW")
@EntityListeners(ReadOnlyEntity.class)
public class CustomerDetail {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DEFAULT_PHONE_NUMBER")
    private String phone;

    @Column(name = "LOCATION")
    private String location;

    @Column(name = "CUSTOMER_TYPE")
    private String customerType;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "INSURANCES")
    private Short insurances;

    @Column(name = "BALANCE")
    private Double balance;

    @Column(name = "CUSTOMER_INDIVIDUAL_ID")
    private String customerIndividualId;

}
