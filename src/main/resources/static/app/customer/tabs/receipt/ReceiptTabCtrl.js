angular
    .module('inspinia')
    .controller('ReceiptTabCtrl', function ($scope, $state, $stateParams, ReceiptService) {

        $scope.insurance = 0;
        $scope.invoice = 0;
        $scope.receipts = [];
        $scope.currentReceipts = [];
        $scope.selectedItem = 0;
        $scope.pageIndexer = [];
        $scope.pageObj = [];
        $scope.showing = "101";
        $scope.state = $state;
        $scope.$on('$viewContentLoaded', function () {
            $scope.loadByCustomer();
        });

        $scope.loadByCustomer = function () {
            let id = $stateParams.id;
            let insurance = $scope.insurance;
            let invoice = $scope.invoice;

            ReceiptService
                .allByCustomer(id, insurance, invoice)
                .then(function (response) {
                    $scope.receipts = response.data;
                    $scope.range(0);
                }, function (error) {

                });
        };

        $scope.selectRow = function (index) {
            let value = $scope.currentReceipts[index].selected;
            angular.forEach($scope.currentReceipts, (receipt) => {
                receipt.selected = false;
            });
            $scope.currentReceipts[index].selected = value;

            $scope.selectedItem = value ? $scope.currentReceipts[index].id : 0;
        };

        $scope.range = function (nextPage) {
            let start = nextPage * 10;
            let beta = (nextPage * 10 + 10);
            let end = beta <= $scope.receipts.length ? beta : $scope.receipts.length;
            $scope.currentReceipts = $scope.receipts.slice(start, end);
            if ($scope.currentReceipts.length < 10) {
                for (let i = $scope.currentReceipts.length; i < 10; i++)
                    $scope.currentReceipts.push({})
            }
            let diff = ($scope.receipts.length / 10).toFixed(0);
            let classArr = [];

            for (let i = 0; i < diff; i++) {
                classArr.push({index: i + 1, class: i === nextPage ? 'active' : ''})
            }
            $scope.pageObj = classArr;
        };
    });