(function () {
    'use strict';
    angular.module(APP)
        .service('ReceiptService', ReceiptService);
})();

ReceiptService.$inject = ['$http'];

function ReceiptService($http) {

    return {
        allByCustomer: (customer, insurance = 0, invoice = 0) => {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}&insurance=${insurance}&invoice=${invoice}`;
            return $http.get("receipt/customer/all" + qs);
        },
        getOne: (customer, receipt) => {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}&receipt=${receipt}`;
            return $http.get("receipt/one" + qs);
        },
        saveOne: (receipt) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post("receipt" + qs, receipt);
        }
    }
}