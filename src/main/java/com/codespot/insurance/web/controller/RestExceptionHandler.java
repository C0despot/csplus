package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.condos.shared.exception.RestResponseException;
import com.condos.shared.model.C2Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private final
    LoggerActivityService activityService;

    @Autowired
    public RestExceptionHandler(LoggerActivityService activityService) {
        this.activityService = activityService;
    }

    @ExceptionHandler(value = {RestResponseException.class})
    protected ResponseEntity<Object> handler(RestResponseException e, WebRequest request) {
        activityService.log(e.getMessage(), LogActivity.Type.ERROR, LogActivity.Scope.of(e.getScope()), LogActivity.Action.of(e.getAction()), e.getDocument());
        return handleExceptionInternal(e, C2Error.builder().message(e.getMessage()).status(e.getStatusCode()).build(), new HttpHeaders(), e.getStatusCode(), request);
    }
}
