package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.insurance.Coverage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoverageRepository extends JpaRepository<Coverage, Long> {
}
