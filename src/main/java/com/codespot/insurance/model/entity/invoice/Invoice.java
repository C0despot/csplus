package com.codespot.insurance.model.entity.invoice;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.type.Types;
import com.condos.shared.annotation.RequiredField;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INVOICE")
public class Invoice {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "START_DATE")
    @RequiredField(descr = "FECHA DE ENTRADA")
    private Date startDate;

    @Column(name = "END_DATE")
    @RequiredField(descr = "FECHA DE EMISIÓN")
    private Date endDate;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    @RequiredField(descr = "CLIENTE")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "POLICY")
    @RequiredField(descr = "POLIZA")
    private Insurance insurance;

    @Column(name = "INVOICE_NUMBER")
    @RequiredField(descr = "NÚMERO DE FACTURA")
    private String invoiceNo;

    @Enumerated(EnumType.STRING)
    @Column(name = "INVOICE_TYPE")
    @RequiredField(descr = "TIPO DE FACTURA")
    private Types.Invoice type;

    @Column(name = "TOTAL_PRIME")
    @RequiredField(descr = "PRIMA")
    private Double prime;

    @Column(name = "BEGINNING_DATE")
    @RequiredField(descr = "FECHA DE INICIO")
    private Date beginningDate;

    @Column(name = "DEAD_LINE_DUE_DATE")
    @RequiredField(descr = "FECHA DE VENCIMIENTO")
    private Date dueDate;

    @Column(name = "SOME_NOTES")
    private String notes;

    @Column(name = "BALANCE")
    private Double balance;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @Transient
    private Boolean selected = false;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Override
    public String toString() {
        return this.id.toString();
    }

    @PrePersist
    void onPrePersist() {
        setCreatedBy(getModifiedBy());
        setCreationDate(new Date());
        setModificationDate(new Date());
    }

    @PreUpdate
    void onPreUpdate() {
        setModificationDate(new Date());
    }

    public boolean isNew(){
        return id == null;
    }
}
