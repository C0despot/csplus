angular
    .module('inspinia')
    .controller('LoginCtrl', function ($scope, $uibModal, AuthService, InsuranceService, $state, $window) {

        $scope.loading = false;
        $scope.user = {
            password: '',
            username: ''
        };
        isFirstLoaded = true;
        $scope.error = false;
        $scope.version = '1.0.0';
        $scope.environment = 'not-set';
        $scope.$on('$viewContentLoaded', function () {
            InsuranceService.appVersion()
                .then((resp) => {
                    $scope.version = resp.data.version;
                    $scope.environment = resp.data.environment;
                })

        });
        $scope.doLogin = function () {
            let user = angular.copy($scope.user);
            $scope.loading = true;
            AuthService
                .getToken(user)
                .then(function (response) {
                    let tokenObj = response.data;
                    localStorage.setItem("token", JSON.stringify(tokenObj));
                    AuthService
                        .getUserInfo(user)
                        .then(function (response) {
                            let userObj = response.data;
                            delete userObj['authorities'];
                            localStorage.setItem("user", JSON.stringify(userObj));
                            let url = localStorage.getItem("redirectTo");
                            if (!url)
                                $state.go("dashboards.dashboard_2");
                            if (url === "#/login") {
                                $state.go("dashboards.dashboard_2");
                            } else {
                                $window.location.href = url;
                            }
                        }, function (ignored) {
                            $scope.loading = false;
                            $scope.error = true;
                        })
                }, function (ignored) {
                    $scope.error = true;
                    $scope.loading = false;
                });
        };

        $scope.openResetPswModal = () => {
            $uibModal.open({
                templateUrl: 'app/auth/login/modal/reset.html',
                controller: 'ResetPasswordCtrl',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                windowClass: "animated fadeIn middle-modal"
            });
        }
    });