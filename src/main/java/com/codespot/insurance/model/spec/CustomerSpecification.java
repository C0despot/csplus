package com.codespot.insurance.model.spec;

import com.codespot.insurance.model.entity.customer.CustomerDetail;
import com.condos.shared.data.page.C2Criteria;
import com.condos.shared.util.DataUtil;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class CustomerSpecification implements Specification<CustomerDetail> {

    private C2Criteria criteria;

    public static CustomerSpecification of(C2Criteria criteria) {
        return new CustomerSpecification(criteria);
    }

    private CustomerSpecification(C2Criteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate
            (Root<CustomerDetail> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        return DataUtil.getPredicate(root, builder, criteria);
    }
}
