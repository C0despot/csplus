package com.codespot.insurance.model.entity.payment;

import com.codespot.insurance.model.entity.invoice.InvoiceReport;
import com.codespot.insurance.model.listener.ReadOnlyEntity;
import com.codespot.insurance.security.model.type.Types;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Entity
@Table(name = "PAYMENT_DOCUMENT_REPORT_VW")
@EntityListeners(value = {ReadOnlyEntity.class})
public class PaymentDocumentReport {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "DOCUMENT_TYPE")
    @Enumerated(value = EnumType.STRING)
    private Types.PaymentDocumentType type;

    @Column(name = "DOCUMENT")
    private String document;

    @Column(name = "EMISSION_DATE")
    private Date emissionDate;

    @Column(name = "AMOUNT")
    private Double amount;

    @JsonIgnore
    @JoinColumn(name = "INVOICE")
    @ManyToOne(fetch = FetchType.LAZY)
    private InvoiceReport invoice;


}
