package com.codespot.insurance.model.entity;

import com.codespot.insurance.security.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LOG_ACTIVITY")
public class LogActivity {

    @Id
    @Column(name = "LOG_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "MESSAGE")
    private String message;

    @ManyToOne
    @JoinColumn(name = "USER")
    private User user;

    @Column(name = "DOCUMENT")
    private String document;

    @Column(name = "SCOPE")
    @Enumerated(value = EnumType.STRING)
    private Scope scope;

    @Column(name = "ACTION")
    @Enumerated(value = EnumType.STRING)
    private Action action;

    @Column(name = "TYPE")
    @Enumerated(value = EnumType.STRING)
    private Type type;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "REMOTE_ADDRESS")
    private String ip;

    public String getScope() {
        return scope == null ? "- - -" : scope.spn();
    }

    public String getAction() {
        return action == null ? "- - -" : action.spn();
    }

    public String getType() {
        return type == null ? "- - -" : type.spn();
    }

    public String getActionClass() {
        return action == null ? "default" : action.style();
    }

    public String getTypeClass() {
        return type == null ? "default" : type.style();
    }

    public enum Scope {

        INSURANCE("PÓLIZA"),
        RECEIPT("RECIBO"),
        INVOICE("FACTURA"),
        ACCESS("ACCESO"),
        CREDIT("CRÉDITO"),
        LOCAL("LOCAL"),
        DEPENDENT("DEPENDIENTE"),
        VEHICLE("VEHICULO"),
        CUSTOMER("CLIENTE"),
        USER("USUARIO"),
        AUTH("Autenticación"),
        CLAIM("RECLAMO");

        private String spn;

        public String spn() {
            return spn;
        }

        Scope(String spn) {
            this.spn = spn;
        }

        public static Scope of(String str) {
            for (Scope scope : Scope.values()) {
                if (scope.name().equalsIgnoreCase(str))
                    return scope;
            }
            return null;
        }
    }

    public enum Action {
        CREATE("CREACIÓN", "success"),
        DELETE("BORRADO", "danger"),
        MODIFY("MODIFICACIÓN", "warning"),
        RENEW("RENOVACIÓN", "default"),
        READ("LECTURA", "info"),
        ACCESS("ACCESO", "primary"),
        CLOSED("CERRADO", "primary"),
        LOGIN("LOGIN", "primary");

        private String spn;

        public String spn() {
            return spn;
        }

        private String style;

        public String style() {
            return style;
        }

        Action(String spn, String style) {
            this.spn = spn;
            this.style = style;
        }

        public static Action of(String str) {
            for (Action action : Action.values()) {
                if (action.name().equalsIgnoreCase(str))
                    return action;
            }
            return null;
        }
    }

    public enum Type {
        INFO("INFO", "info"),
        WARNING("ADVERTENCIA", "warning"),
        ERROR("ERROR", "danger"),
        SUCCESS("EXITOSO", "success"),
        FAIL("FALLIDO", "danger");

        private String spn;

        public String spn() {
            return spn;
        }

        private String style;

        public String style() {
            return style;
        }

        Type(String spn, String style) {
            this.spn = spn;
            this.style = style;
        }
    }
}
