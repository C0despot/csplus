package com.codespot.insurance.model.dto;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.invoice.Invoice;

import java.util.Date;

public class ReceiptDto {

    private Long id;

    private String receiptNo;

    private Date startDate;

    private Date endDate;

    private Customer customer;

    private Insurance insurance;

    private Invoice invoice;

    private Double amount;

    private String payment;

    private String paymentType;

    private String currency;

    private String lastFour;

    private String concept;

    private String notes;

}
