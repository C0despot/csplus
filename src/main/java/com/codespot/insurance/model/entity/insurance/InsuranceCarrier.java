package com.codespot.insurance.model.entity.insurance;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "COMPANY")
public class InsuranceCarrier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMPANY_ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CODE")
    private String code;

    @Column(name = "CONTACT")
    private String contact;

    @Column(name = "PHONE_NUMBER")
    private String phone;

    @Column(name = "ACCOUNT_LIST")
    private String accountList;

    @Column(name = "PHONE_LIST")
    private String phoneList;


}
