package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.insurance.InsuranceCarrier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InsuranceCarrierRepository extends JpaRepository<InsuranceCarrier, Long> {
}