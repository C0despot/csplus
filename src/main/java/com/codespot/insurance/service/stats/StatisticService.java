package com.codespot.insurance.service.stats;


import com.codespot.insurance.model.dto.MonthlyReportDto;
import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.customer.CustomerCurrentReport;
import com.codespot.insurance.model.entity.customer.CustomerDetail;
import com.codespot.insurance.model.entity.customer.CustomerHistoricalReport;
import com.codespot.insurance.security.model.type.Types;
import com.codespot.insurance.service.stats.command.StatsCommand;
import com.condos.shared.data.page.C2Criteria;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

public interface StatisticService {

    MonthlyReportDto getMonthlyReport(long user);

    Types.Month getCurrentMonth();

    Page<CustomerDetail> getCustomerBasicDetails(C2Criteria criteria, PageDto page);

    List<CustomerCurrentReport> getCurrentReport(Long customer);

    List<CustomerHistoricalReport> getHistoricalReport(Long customer);

    String printReport(Long customer, Long report, Types.Report type) throws IOException;

    DefaultStatisticService.Incomes getIncomes(Long clientId);

    StatsCommand.Exchange moneyExchange(double qty, StatsCommand.Exchange.Currency from, StatsCommand.Exchange.Currency to);


}
