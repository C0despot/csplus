(function () {
    'use strict';
    angular.module('inspinia')
        .service('NotificationService', NotificationService);
})();

NotificationService.$inject = ['SweetAlert', '$filter'];

function NotificationService(SweetAlert, $filter) {

    return {
        successMsj: (msj = "Transaccion realizada con exito!") => {
            SweetAlert.swal({
                title: "Good job!",
                text: msj,
                type: "success"
            });
        },
        errorMsj: (msj = "Transaccion no pudo ser realizada con exito!") => {
            SweetAlert.swal({
                title: "Algo anda mal!",
                text: msj,
                type: "error"
            });
        },

        confirm: (title, text, callBack) => {
            SweetAlert.swal({
                    title: title,
                    text: text,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                },
                function (isConfirm) {
                    if (isConfirm)
                        callBack();
                }
            )
        },

        confirmPassword: (callBack) => {
            SweetAlert.swal({
                    title: 'Ingrese Contraseña',
                    type: "input",
                    inputPlaceholder: 'Ingrese Contraseña',
                    inputType: 'password',
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "Cancelar",
                },
                (data) => {
                    if (data) {
                        swal.close();
                        callBack(data);
                    }
                }
            )
        },

        showCoverage: (coverage) => {

            let html = `<div class="table-responsive">
                    <table class="table table-striped no-margin-bottom">
                        <thead>
                        <tr class="grid-view">
                            <th>Daños a la Propiedad Ajena</th>
                            <th>Lesiones Corporales o Muerte a Una Persona</th>
                            <th>Lesiones Corporales o Muerte a Mas de Una Persona</th>
                            <th>Riesgo al conductor</th>
                            <th>Riesgo Peones [Camiones - Camionetas]</th>
                            <th>Fianza Judicial</th>
                            <th>Plan Gastos Funerarios Conductor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="good in goods">
                            <td>
                                ${$filter('currency')(coverage.damage)}
                            </td>
                            <td>
                                ${$filter('currency')(coverage.justOneInjury)}
                            </td>
                            <td>
                                ${$filter('currency')(coverage.moreThanOneInjury)}
                            </td>
                            <td>
                                ${$filter('currency')(coverage.riskBack)}
                            </td>
                            <td>
                                ${$filter('currency')(coverage.risks)}
                            </td>
                            <td>
                                ${$filter('currency')(coverage.lawBill)}
                            </td>
                            <td>
                                ${$filter('currency')(coverage.deadExpenses)}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>`;
            SweetAlert.swal({
                title: `<span>${coverage.description}</span>`,
                type: 'info',
                html: true,
                customClass: 'swal-wide',
                text: html,
                showCloseButton: true,
            })
        }
    }
}