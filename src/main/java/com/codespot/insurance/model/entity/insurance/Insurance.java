package com.codespot.insurance.model.entity.insurance;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.type.Types;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;


@Data
@Entity
@Table(name = "INSURANCE")
public class Insurance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    private Customer customer;

    @Column(name = "POLICY_NUMBER")
    private String insuranceNumber;

    @ManyToOne
    @JoinColumn(name = "BRANCH_NAME")
    private Branch branch;

    @ManyToOne
    @JoinColumn(name = "POLICER")
    private InsuranceCarrier carrier;

    @Column(name = "CURRENCY")
    @Enumerated(EnumType.STRING)
    private Types.Currency currency;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "PRIME")
    private Double prime;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "LOCAL_PLACE")
    private String localPlace;

    @Column(name = "BUSINESS_TYPE")
    private String businessType;

    @Column(name = "BUILDING_TYPE")
    private String buildingType;

    @Column(name = "BIG_NOTE")
    private String bigNote;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Override
    public String toString() {
        return this.id.toString();
    }

    @PrePersist
    void onPrePersist() {
        setCreatedBy(getModifiedBy());
        setCreatedDate(new Date());
        setModifiedDate(new Date());
    }

    @PreUpdate
    void onPreUpdate() {
        setModifiedDate(new Date());
    }

    public boolean isNew() {
        return id == null;
    }
}
