package com.codespot.insurance.service.file;

import com.condos.shared.aws.FileRequest;
import com.condos.shared.aws.S3;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Service(value = "defaultFileService")
public class DefaultFileService implements FileService {

    private String MAIN_BUCKET_NAME;
    private String REPORT_BUCKET;
    private String AWS_ACCESS_KEY;
    private String AWS_SECRET_KEY;
    private String S3_BASE_URL;

    public DefaultFileService(@Value(value = "${aws.s3.mainBucket}") String mainBucket,
                              @Value(value = "${aws.s3.reportBucket}") String reportBucket,
                              @Value(value = "${aws.s3.baseUrl}") String s3BaseUrl,
                              @Value(value = "${aws.accessKey}") String accessKey,
                              @Value(value = "${aws.secretKey}") String secretKey) {
        this.MAIN_BUCKET_NAME = mainBucket;
        this.REPORT_BUCKET = reportBucket;
        this.AWS_ACCESS_KEY = accessKey;
        this.AWS_SECRET_KEY = secretKey;
        this.S3_BASE_URL = s3BaseUrl;
    }

    @Override
    public String s3Upload(File file, S3.Extension extension) {
        S3.Config config = S3
                .Config
                .builder()
                .AWS_ACCESS_KEY(AWS_ACCESS_KEY)
                .AWS_SECRET_KEY(AWS_SECRET_KEY)
                .BUCKET_NAME(REPORT_BUCKET)
                .S3_BASE_URL(S3_BASE_URL)
                .DEFAULT_BUCKET_NAME(MAIN_BUCKET_NAME)
                .build();

        return S3.Loader.upload(file, extension, config);
    }

    @Override
    public String s3Upload(MultipartFile file) {
        String temp = System.getProperty("java.io.tmpdir");
        String separator = System.getProperty("file.separator");
        File newFile = new File(temp + separator + file.getOriginalFilename());
        try {
            file.transferTo(newFile);
        } catch (IOException ignored) {
        }
        return s3Upload(newFile, S3.Extension.fromStr(Arrays.stream(newFile.getAbsolutePath().split("\\.")).reduce((a, b) -> b).orElse("txt")));
    }

    @Override
    public String fullS3Url() {
        return this.S3_BASE_URL + MAIN_BUCKET_NAME + REPORT_BUCKET;
    }

    @Override
    public File pdfDownload(String filename) throws IOException {
        S3.Config config = S3
                .Config
                .builder()
                .AWS_ACCESS_KEY(AWS_ACCESS_KEY)
                .AWS_SECRET_KEY(AWS_SECRET_KEY)
                .BUCKET_NAME(REPORT_BUCKET)
                .S3_BASE_URL(S3_BASE_URL)
                .DEFAULT_BUCKET_NAME(MAIN_BUCKET_NAME)
                .build();
        FileRequest request = FileRequest.builder().extension(FileRequest.FileExtension.PDF).fileName(filename).build();
        S3.Reader.downloadFile(request, config);
        return new File(request.getExtension().temp());
    }

}
