package com.codespot.insurance.model.dto;

import lombok.Data;

@Data
public class RemoveDocument {

    private Long documentNo;
    private Type type;
    private String password;


    public enum Type {
        CREDIT,
        CLAIM,
        CUSTOMER,
        INSURANCE,
        INVOICE,
        RECEIPT
    }
}
