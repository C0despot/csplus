package com.codespot.insurance.service.stats;

import com.codespot.insurance.model.dto.MonthlyReportDto;
import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.CustomerCurrentReport;
import com.codespot.insurance.model.entity.customer.CustomerDetail;
import com.codespot.insurance.model.entity.customer.CustomerHistoricalReport;
import com.codespot.insurance.model.repository.customer.CustomerCurrentReportRepository;
import com.codespot.insurance.model.repository.customer.CustomerDetailRepository;
import com.codespot.insurance.model.repository.customer.CustomerHistoricalReportRepository;
import com.codespot.insurance.model.spec.CustomerSpecification;
import com.codespot.insurance.model.util.Utility;
import com.codespot.insurance.report.model.AccountStatusReport;
import com.codespot.insurance.report.model.HistoricalAccountStatusReport;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.type.Types;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.service.customer.CustomerService;
import com.codespot.insurance.service.file.FileService;
import com.codespot.insurance.service.stats.command.StatsCommand;
import com.condos.shared.aws.S3;
import com.condos.shared.data.page.C2Criteria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

@Service(value = "defaultStatisticService")
public class DefaultStatisticService implements StatisticService {

    @PersistenceContext
    private EntityManager entityManager;

    private final
    UserService userService;

    private final
    CustomerDetailRepository customerDetailRepository;

    private final
    AccountStatusReport accountStatusReport;

    private final
    HistoricalAccountStatusReport historicalAccountStatusReport;

    private final
    CustomerCurrentReportRepository currentReportRepository;

    private final
    CustomerHistoricalReportRepository historicalReportRepository;

    private final
    CustomerService customerService;

    private final
    FileService fileService;

    private final
    StatsCommand statsCommand;

    @Autowired
    public DefaultStatisticService(UserService userService,
                                   CustomerDetailRepository customerDetailRepository,
                                   CustomerCurrentReportRepository currentReportRepository,
                                   CustomerService customerService,
                                   AccountStatusReport accountStatusReport,
                                   FileService fileService,
                                   CustomerHistoricalReportRepository historicalReportRepository,
                                   HistoricalAccountStatusReport historicalAccountStatusReport,
                                   StatsCommand statsCommand) {
        this.userService = userService;
        this.customerDetailRepository = customerDetailRepository;
        this.currentReportRepository = currentReportRepository;
        this.customerService = customerService;
        this.accountStatusReport = accountStatusReport;
        this.fileService = fileService;
        this.historicalReportRepository = historicalReportRepository;
        this.historicalAccountStatusReport = historicalAccountStatusReport;
        this.statsCommand = statsCommand;
    }

    @Override
    public MonthlyReportDto getMonthlyReport(long userId) {

        User user = this.userService.findById(userId);
        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("PENDING_INSURANCE_INVOICE_BALANCE_PROCEDURE");

        query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(2, Integer.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(3, Integer.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(4, Integer.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(5, Integer.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(6, Integer.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(7, Integer.class, ParameterMode.OUT);

        query.setParameter(1, user.getId());
        query.execute();

        Integer ins = (Integer) query.getOutputParameterValue(2);
        Integer expIns = (Integer) query.getOutputParameterValue(3);
        Integer inv = (Integer) query.getOutputParameterValue(4);
        Integer expInv = (Integer) query.getOutputParameterValue(5);
        Integer moneyMade = (Integer) query.getOutputParameterValue(6);
        Integer cashMade = (Integer) query.getOutputParameterValue(7);

        return new MonthlyReportDto(ins, expIns, inv, expInv, moneyMade, cashMade, getCurrentMonth().spanish());
    }

    @Override
    public Types.Month getCurrentMonth() {
        return Types.Month.of(Calendar.getInstance().get(Calendar.MONTH));
    }

    @Override
    public Page<CustomerDetail> getCustomerBasicDetails(C2Criteria criteria, PageDto page) {
        Pageable pageable = PageRequest.of(page.getPage(), page.getSize(), new Sort(page.getSort(), page.getOrderBy()));
        CustomerSpecification specification = CustomerSpecification.of(criteria);

        return this.customerDetailRepository.findAll(specification, pageable);
    }

    @Override
    public List<CustomerCurrentReport> getCurrentReport(Long customerId) {
        Customer customer = this.customerService.getOne(customerId);
        return this.currentReportRepository.findAllByCustomerEquals(customer);
    }

    @Override
    public List<CustomerHistoricalReport> getHistoricalReport(Long customerId) {
        Customer customer = this.customerService.getOne(customerId);
        return this.historicalReportRepository.findAllByCustomerEquals(customer);
    }

    @Override
    public String printReport(Long cusId, Long report, Types.Report type) throws IOException {
        Customer customer = this.customerService.getOne(cusId);
        if (Objects.isNull(report)) {
            switch (type) {
                case CURRENT:
                    List<CustomerCurrentReport> currentReports = this.currentReportRepository.findAllByCustomerEquals(customer);
                    File file = accountStatusReport.buildPdf(currentReports);
                    return this.fileService.s3Upload(file, S3.Extension.PDF);
                case STORYLINE:
                    List<CustomerHistoricalReport> historicalReports = this.historicalReportRepository.findAllByCustomerEquals(customer);
                    file = historicalAccountStatusReport.buildPdf(historicalReports);
                    return this.fileService.s3Upload(file, S3.Extension.PDF);

            }

        } else {
            switch (type) {
                case CURRENT:
                    List<CustomerCurrentReport> currentReports = this.currentReportRepository.findAllByIdEquals(report);
                    File file = accountStatusReport.buildPdf(currentReports);
                    return this.fileService.s3Upload(file, S3.Extension.PDF);
                case STORYLINE:
                    List<CustomerHistoricalReport> historicalReports = this.historicalReportRepository.findAllByIdEquals(report);
                    file = historicalAccountStatusReport.buildPdf(historicalReports);
                    return this.fileService.s3Upload(file, S3.Extension.PDF);
            }

        }
        return null;
    }

    @Override
    public Incomes getIncomes(Long clientId) {
        User user = this.userService.findById(clientId);

        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("GET_INCOMES_BY_CURRENCY");

        query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(2, Double.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(3, Double.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(4, Double.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(5, Double.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(6, Double.class, ParameterMode.OUT);
        query.registerStoredProcedureParameter(7, Double.class, ParameterMode.OUT);

        query.setParameter(1, user.getId());
        query.execute();

        Double doPeso = Utility.doubleValue(query.getOutputParameterValue(2));
        Double usDollar = Utility.doubleValue(query.getOutputParameterValue(3));
        Double euEuro = Utility.doubleValue(query.getOutputParameterValue(4));
        Double totalDoPeso = Utility.doubleValue(query.getOutputParameterValue(5));
        Double totalUsDollar = Utility.doubleValue(query.getOutputParameterValue(6));
        Double totalEuEuro = Utility.doubleValue(query.getOutputParameterValue(7));
        Double total = doPeso + usDollar * 50 + euEuro * 57;
        return Incomes.builder()
                .user(user)
                .doPeso(doPeso)
                .usDollar(usDollar)
                .euEuro(euEuro)
                .totalDoPeso(totalDoPeso)
                .totalEuEuro(totalEuEuro)
                .totalUsDollar(totalUsDollar)
                .total(total)
                .build();
    }

    @Override
    public StatsCommand.Exchange moneyExchange(double qty, StatsCommand.Exchange.Currency from, StatsCommand.Exchange.Currency to) {
        StatsCommand.Exchange exchange = this.statsCommand.doMoneyExchange(from, to);
        exchange.getResult().setAmount(qty * exchange.getResult().getValue());
        return exchange;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Incomes {
        private User user;
        private Double doPeso;
        private Double usDollar;
        private Double euEuro;
        private Double total;
        private Double totalDoPeso;
        private Double totalUsDollar;
        private Double totalEuEuro;
    }
}
