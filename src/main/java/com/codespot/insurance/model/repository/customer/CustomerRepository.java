package com.codespot.insurance.model.repository.customer;

import com.codespot.insurance.model.entity.customer.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

@SuppressWarnings("ALL")
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query(value = "(SELECT * " +
            "FROM CUSTOMER C " +
            "LEFT JOIN INSURANCE I ON C.CLIENT_ID=I.CLIENT " +
            "LEFT JOIN GOODS G ON I.ID = G.POLICY_LINKED " +
            "WHERE C.FULL_NAME LIKE :criteria " +
            "OR C.CLIENT_BUSS_RNC LIKE :criteria " +
            "OR C.CLIENT_ID_CARD LIKE :criteria " +
            "OR I.POLICY_NUMBER LIKE :criteria " +
            "OR G.CAR_REGISTER LIKE :criteria " +
            "OR G.CHASSIS_SERIAL LIKE :criteria " +
            "ORDER BY FULL_NAME LIMIT 10)" +
            "UNION ALL (SELECT * FROM CUSTOMER C " +
            "RIGHT JOIN INSURANCE I ON C.CLIENT_ID=I.CLIENT " +
            "RIGHT JOIN GOODS G ON I.ID = G.POLICY_LINKED " +
            "WHERE C.FULL_NAME LIKE :criteria " +
            "OR C.CLIENT_BUSS_RNC LIKE :criteria " +
            "OR C.CLIENT_ID_CARD LIKE :criteria " +
            "OR I.POLICY_NUMBER LIKE :criteria " +
            "OR G.CAR_REGISTER LIKE :criteria " +
            "OR G.CHASSIS_SERIAL LIKE :criteria " +
            "ORDER BY FULL_NAME LIMIT 10)",
            nativeQuery = true)
    List<Customer> findByFilterCriteria(@Param(value = "criteria") String criteria);

    List<Customer> findByRncEquals(String rnc);

    List<Customer> findByIdCardEquals(String id);
}
