angular
    .module('inspinia')
    .controller('EditGoodController', function ($scope, $uibModalInstance, $stateParams, InsuranceService, vehicle) {

        $scope.uibModalInstance = $uibModalInstance;
        $scope.GoodTitle = "Loading...";
        $scope.vehicle = {};
        $scope.coverages = [];
        $scope.isEditMode = true;
        setTimeout(function () {
            InsuranceService
                .loadCoverage()
                .then(function (response) {
                    $scope.coverages = response.data;
                    $scope.vehicle = vehicle;
                    let current = angular.copy(vehicle);
                    let brand = current.brand;
                    let model = current.model;
                    let year = current.year;
                    $scope.GoodTitle = `${brand} ${model} ${year}`.toLowerCase()
                });
        }, 100);
    });