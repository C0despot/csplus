package com.codespot.insurance.model.entity.insurance;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "COVERAGE")
public class Coverage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DAMAGE")
    private Double damage;

    @Column(name = "INJURIES_JUST_ONE")
    private Double justOneInjury;

    @Column(name = "INJURIES_MORE_THAN_ONE")
    private Double moreThanOneInjury;

    @Column(name = "RISK")
    private Double risks;

    @Column(name = "RISK_BACK")
    private Double riskBack;

    @Column(name = "LAW_BILL")
    private Double lawBill;

    @Column(name = "DEAD_EXPENSES")
    private Double deadExpenses;

}
