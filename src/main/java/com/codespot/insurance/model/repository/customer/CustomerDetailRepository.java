package com.codespot.insurance.model.repository.customer;

import com.codespot.insurance.model.entity.customer.CustomerDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerDetailRepository extends JpaRepository<CustomerDetail, Long>, JpaSpecificationExecutor<CustomerDetail> {

    @Query(value = "SELECT GET_BALANCE(:customer, :insurance)", nativeQuery = true)
    Double getCustomerBalance(@Param(value = "customer") Long customerId, @Param(value = "insurance") Long insurance);

    @Query(value = "SELECT SEQUENCE_GENERATOR(:tns)", nativeQuery = true)
    Long findNextTransactionId(@Param(value = "tns") String tns);

}
