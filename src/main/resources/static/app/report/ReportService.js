(function () {
    'use strict';
    angular.module(APP)
        .service('ReportService', ReportService);
})();

ReportService.$inject = ['$http'];

function ReportService($http) {

    let reportUrl = 'stats/report';
    return {
        customerCurrentReport: (customer) => {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}`;
            return $http.get(reportUrl + "/current" + qs);
        },
        customerHistoricalReport: (customer) => {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}`;
            return $http.get(reportUrl + "/historical" + qs);
        },
        printReport(target, customer, number = '') {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}&target=${target}&number=${number}`;
            return $http.post(reportUrl + "/print" + qs);
        }
    };
}