angular
    .module('inspinia')
    .controller('NewLocalCtrl', function ($scope, $uibModalInstance, $stateParams, InsuranceService) {

        $scope.uibModalInstance = $uibModalInstance;
        $scope.localTitle = "Loading...";
        $scope.local = {
            version: (new Date()).getMilliseconds()
        };
        $scope.row = {};
        $scope.goods = [];
        $scope.isEditMode = false;
        $scope.edit = true;

        setTimeout(function () {
            $scope.localTitle = `Agregar Nuevo Local`.toLowerCase();
            $scope.$apply();
        }, 100);

        $scope.addRow = function (row) {
            row.securedSum = numberFormatter(row.securedSum);
            $scope.goods.push(angular.copy(row));
            $scope.row = {};
        };

        $scope.save = function () {
            let local = angular.copy($scope.local);
            local.goods = JSON.stringify($scope.goods);
            $uibModalInstance.close(local)
        };
    });