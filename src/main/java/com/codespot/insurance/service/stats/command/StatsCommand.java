package com.codespot.insurance.service.stats.command;

import com.codespot.insurance.model.util.Utility;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Date;

@Component
public class StatsCommand {

    private final
    RestTemplate restTemplate;

    private final
    String currencyApiUrl;

    private final
    String currencySecret;

    @Autowired
    public StatsCommand(RestTemplate restTemplate,
                        @Value(value = "${app.currency.api.url}") String currencyApiUrl,
                        @Value(value = "${app.currency.api.secret}") String currencySecret) {
        this.restTemplate = restTemplate;
        this.currencyApiUrl = currencyApiUrl;
        this.currencySecret = currencySecret;
    }

    public Exchange doMoneyExchange(Exchange.Currency from, Exchange.Currency to) {
        String url = String.format("%s/%s/%s/json?quantity=%s&key=%s", currencyApiUrl, from.name(), to.name(), 1, currencySecret);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Exchange> response = restTemplate.exchange(url, HttpMethod.GET, entity, Exchange.class);
        return response.getBody();
    }

    @Data
    public static class Exchange {

        private Result result;
        private String status;

        @Data
        public static class Result {
            private Date updated;
            private Currency source;
            private Currency target;
            private Double value;
            private Double quantity;
            private Double amount;

            public String getCalculated() {
                return Utility.getCurrencyValue(amount);
            }
        }

        public enum Currency {
            USD("Dólar"),
            DOP("Peso"),
            EUR("Euro");

            private String spanish;

            public String spanish() {
                return spanish;
            }

            Currency(String spanishDesc) {
                this.spanish = spanishDesc;
            }
        }
    }
}
