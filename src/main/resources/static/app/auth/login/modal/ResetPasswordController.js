angular
    .module(APP)
    .controller('ResetPasswordCtrl', function ($scope, $uibModalInstance, UserService, NotificationService) {

        $scope.$uibModalInstance = $uibModalInstance;
        $scope.email = '';
        $scope.loading = false;
        isFirstLoaded = true;

        $scope.sendResetEmail = (email) => {
            $scope.loading = true;
            UserService
                .sendResetPasswordEmail(email)
                .then(() => {
                    NotificationService.successMsj("Se ha enviado un nuevo acceso al correo marcado.");
                    $uibModalInstance.close();
                }, () => {
                    NotificationService.errorMsj("Lo sentimos, no se ha encontrado el usuario.");
                })
                .finally(()=>{
                    isFirstLoaded = true;
                    $scope.loading = false;
                });
        }

    });