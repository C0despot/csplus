angular
    .module('inspinia')
    .controller('NewDependentCtrl', function ($scope, $uibModalInstance, $stateParams, InsuranceService) {

        $scope.uibModalInstance = $uibModalInstance;
        $scope.dependentTitle = "Loading...";
        $scope.dependent = {
            version: (new Date()).getMilliseconds()
        };
        $scope.isEditMode = false;
        $scope.edit = true;

        setTimeout(function () {
            $scope.dependentTitle = `Agregar Nuevo Dependiente`.toLowerCase();
            $scope.$apply();
        }, 100);

        $scope.save = function () {
            let dependent = angular.copy($scope.dependent);
            $uibModalInstance.close(dependent)
        };
    });