package com.codespot.insurance.security.config;

import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.repository.UserRepository;
import com.codespot.insurance.service.logger.LoggerActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Service(value = "userDetailsService")
public class CustomAuthenticationProvider implements UserDetailsService {

    private final
    UserRepository userRepository;

    private final
    LoggerActivityService activityService;

    @Autowired
    public CustomAuthenticationProvider(UserRepository userRepository, LoggerActivityService activityService) {
        this.userRepository = userRepository;
        this.activityService = activityService;
    }


    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = this.userRepository.findUser(username);
        if (Objects.isNull(user))
            activityService.warning("Usuario no registrado, ".concat(username), LogActivity.Scope.ACCESS, LogActivity.Action.ACCESS, username);
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().name()));
        return new org.springframework.security.core.userdetails.User(user.getId().toString(), user.getPassword(), grantedAuthorities);
    }
}