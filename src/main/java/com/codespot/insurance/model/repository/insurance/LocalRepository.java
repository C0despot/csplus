package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.insurance.Local;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocalRepository extends JpaRepository<Local, Long> {

    List<Local> findAllByInsuranceEquals(Insurance insurance);
}
