package com.codespot.insurance.job;

import com.codespot.insurance.service.invoice.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class InvoiceBalanceJobs {

    private final
    InvoiceService invoiceService;

    @Autowired
    public InvoiceBalanceJobs(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @Scheduled(cron = "0 * * * * *")
    public void hourly(){
        invoiceService.updateBalances();
    }
}
