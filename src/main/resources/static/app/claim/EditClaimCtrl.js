angular
    .module(APP)
    .controller('EditClaimCtrl', function ($scope, $uibModal, $state, $stateParams, InvoiceService, InsuranceService, NotificationService) {

        $scope.edit = false;
        $scope.loading = false;
        $scope.greenLight = false;
        $scope.claim = {
            docNo: null,
            customer: null,
            insurance: null,
            startDate: new Date(),
            endDate: new Date(),
            beginningDate: new Date(),
            dueDate: nextYear(),
            prime: 0
        };

        $scope.updateCarrier = (insurance) => {
            if (insurance) {
                $scope.claim.log = angular.copy(insurance.carrier);
            }
            else
                $scope.claim.log = null;
        };

        $scope.insurances = [];
        $scope.$on('$viewContentLoaded', function () {
            let customer = $stateParams.id;
            let claim = $stateParams.claim;

            InsuranceService
                .getByCustomer(customer)
                .then(function (response) {
                    $scope.insurances = response.data;
                    InsuranceService
                        .Claim()
                        .getOne(claim)
                        .then(function (response) {
                            $scope.claim = angular.copy(response.data);
                            $scope.greenLight = true;
                        }, function (error) {

                        });
                });

            InsuranceService
                .nextIndex('CLAIMS')
                .then(function (response) {
                    setTimeout(() => {
                        if ($scope.claim.docNo === null) {
                            $scope.claim.docNo = response.data.result;
                        }
                    }, 1000);
                }, function () {
                })

        });

        $scope.doSave = () => {
            let claim = angular.copy($scope.claim);
            claim.creationDate = new Date(claim.creationDate);
            $scope.loading = true;
            InsuranceService
                .Claim()
                .save(claim)
                .then(() => {
                    NotificationService.successMsj();
                    $state.go("customer.detail.step_six.list", {id: $stateParams.id});
                }, (errorResp) => {
                    NotificationService.errorMsj(errorResp.data.message);
                })
                .finally(() => {
                    $scope.loading = false;
                });
        };

        $scope.validateMe = function () {
            InsuranceService
                .validateNumber($scope.claim.docNo, 'INVOICE')
                .then(function () {
                    $("#docNo").removeClass("has-error");
                }, function () {
                    $("#docNo").addClass("has-error");
                })
        };

        $scope.showAuditedItem = () => {
            let claim = angular.copy($scope.claim);
            $uibModal.open({
                templateUrl: 'app/report/audit_modal.html',
                controller: 'AuditController',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                windowClass: "animated fadeIn",
                resolve: {
                    document: function () {
                        return {
                            id: claim.docNo,
                            type: 'RECLAMACIÓN',
                            createdBy: claim.createdBy,
                            createdDate: dateFormatter(claim.creationDate),
                            modifiedDate: dateFormatter(claim.modificationDate),
                            modifiedBy: claim.modifiedBy
                        }
                    }
                }
            });
        };

        $scope.deleteItem = () => {
            const invoice = angular.copy($scope.claim);
            NotificationService
                .confirmPassword(
                    (password) => {
                        let deleteObj = {
                            documentNo: invoice.id,
                            type: 'CLAIM',
                            password: password
                        };
                        InsuranceService
                            .deleteObj(deleteObj)
                            .then(() => {
                                $state.go("customer.detail.step_six.list", {id: invoice.customer.clientId});
                            });
                    })
        }
    });