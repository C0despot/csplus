(function () {
    'use strict';
    angular.module(APP)
        .service('CreditService', CreditService);
})();

CreditService.$inject = ['$http'];

function CreditService($http) {

    return {
        getOne: (customer, doc) => {
            let qs = `?access_token=${getAccessToken()}&document=${doc}&customer=${customer}`;
            return $http.get('credit/one' + qs);
        },
        saveOne: (credit) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post('credit' + qs, credit);
        }

    }
}