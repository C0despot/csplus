angular
    .module(APP)
    .controller('HistoricalCustomerReportCtrl', function ($scope, NotificationService, ReportService, CustomerService, $window) {

        $scope.target = false;
        $scope.data = null;
        $scope.window = $window;
        $scope.printing = false;
        $scope.customers = [];
        $scope.customer = {
            selected: null
        };

        $scope.toggle = function (scope) {
            scope.toggle();
        };


        $scope.refreshCustomers = function (value) {
            return CustomerService
                .filter(value)
                .then((response) => {
                    $scope.customers = response.data;
                });
        };

        $scope.loadDocuments = (customer) => {
            $scope.data = null;
            ReportService
                .customerHistoricalReport(customer.clientId)
                .then((response) => {
                    if (response.data.length > 0)
                        $scope.data = angular.copy(response.data);
                })
        };

        $scope.printAllForCurrent = () => {
            $scope.printing = true;
            ReportService
                .printReport('STORYLINE', $scope.customer.selected.clientId)
                .then((resp) => {
                    $scope.window.open("/report/pdf/" + resp.data.url + '?access_token=' + getAccessToken());
                })
                .finally(() => $scope.printing = false
                )
        };

        $scope.printOne = (id) => {
            $scope.printing = true;
            ReportService
                .printReport('STORYLINE', $scope.customer.selected.clientId, id)
                .then((resp) => {
                    $scope.window.open("/report/pdf/" + resp.data.url + '?access_token=' + getAccessToken());
                })
                .finally(() => $scope.printing = false
                )
        }
    });