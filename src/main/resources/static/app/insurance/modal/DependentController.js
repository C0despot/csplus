angular
    .module(APP)
    .controller('DependentController', function ($scope, $uibModalInstance, $stateParams, InsuranceService, dependents, NotificationService) {

        $scope.$uibModalInstance = $uibModalInstance;
        $scope.dependentTitle = "Loading...";
        $scope.dependentTitle = "Lista de Dependientes";
        $scope.dependents = angular.copy(dependents);
        $scope.user = JSON.parse(localStorage.getItem("user"));

        $scope.editModal = function (dependent) {
            $uibModalInstance.close({dependent: dependent});
        };

        $scope.newModal = function () {
            $uibModalInstance.close({isNew: true});
        };

        $scope.showTooltip = function (plan) {
            NotificationService.showCoverage(plan);
        };

        $scope.delete = (id) => {
            NotificationService
                .confirm(
                    "¿Borrar dependiente?",
                    "Está seguro que desea borrar el local seleccionado?",
                    () => {
                        InsuranceService
                            .deleteElement(id, 'DEPENDENT')
                            .then((ignored) => {
                                location.reload();
                            }, (ignored) => {
                            })
                    })
        };
    });