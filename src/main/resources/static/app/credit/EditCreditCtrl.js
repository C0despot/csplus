angular
    .module(APP)
    .controller('EditCreditCtrl',
        function ($scope, $state, $stateParams, $uibModal, CreditService, InvoiceService, InsuranceService, NotificationService) {

            $scope.edit = false;
            $scope.hidden = false;
            $scope.loading = false;
            $scope.greenLight = false;
            $scope.validating = false;
            $scope.doc = {
                docNo: null,
                customer: null,
                startDate: new Date(),
                endDate: new Date(),
                beginningDate: new Date(),
                dueDate: new Date(),
                invoice: {
                    insurance: {
                        insuranceNumber: null
                    }
                }
            };
            $scope.insurances = [];
            $scope.$on('$viewContentLoaded', () => {
                let customer = $stateParams.id;
                let credit = $stateParams.credit;

                InvoiceService
                    .allByCustomer(customer)
                    .then((response) => {
                        $scope.invoices = response.data;
                    });
                CreditService
                    .getOne(customer, credit)
                    .then((response) => {
                        $scope.doc = response.data;
                    });
                InsuranceService
                    .getByCustomer(customer)
                    .then((response) => {
                        $scope.insurances = response.data;
                    });
            });

            $scope.validateAmount = () => {
                if (!$scope.doc.invoice)
                    return false;

                $scope.doc.insurance = angular.copy($scope.doc.invoice.insurance);
                let balance = angular.copy($scope.doc.invoice.balance);
                let amount = angular.copy($scope.doc.amount);
                balance = numberFormatter(balance);
                amount = numberFormatter(amount);
                if (balance < amount)
                    $("#amount").addClass('has-error');
                else
                    $("#amount").removeClass('has-error');
            };

            $scope.doSave = () => {
                let credit = angular.copy($scope.doc);
                credit.startDate = new Date(credit.startDate);
                credit.endDate = new Date(credit.endDate);
                credit.beginDate = new Date(credit.beginDate);
                credit.dueDate = new Date(credit.dueDate);

                credit.amount = numberFormatter(credit.amount);

                $scope.loading = true;
                CreditService
                    .saveOne(credit)
                    .then(() => {
                        NotificationService.successMsj();
                        $state.go("customer.detail.step_five.list", {id: $stateParams.id});
                    }, () => {
                        NotificationService.errorMsj();
                    })
                    .finally(() => {
                        $scope.loading = false;
                    });
            };

            $scope.validateMe = () => {
                $scope.validating = true;
                InsuranceService
                    .validateNumber($scope.doc.docNo, 'CREDIT')
                    .then(() => {
                        $("#docNo").removeClass("has-error");
                    }, () => {
                        $("#docNo").addClass("has-error");
                    })
                    .finally(() => {
                        $scope.validating = false;
                    })
            };

            $scope.showAuditedItem = () => {
                let credit = angular.copy($scope.doc);
                $uibModal.open({
                    templateUrl: 'app/report/audit_modal.html',
                    controller: 'AuditController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'md',
                    windowClass: "animated fadeIn",
                    resolve: {
                        document: function () {
                            return {
                                id: credit.docNo,
                                type: 'CREDITO',
                                createdBy: credit.createdBy,
                                createdDate: dateFormatter(credit.creationDate),
                                modifiedDate: dateFormatter(credit.modificationDate),
                                modifiedBy: credit.modifiedBy
                            }
                        }
                    }
                });
            };

            $scope.deleteItem = () => {
                const credit = angular.copy($scope.doc);
                NotificationService
                    .confirmPassword(
                        (password) => {
                            let deleteObj = {
                                documentNo: credit.id,
                                type: 'CREDIT',
                                password: password
                            };
                            InsuranceService
                                .deleteObj(deleteObj)
                                .then(() => {
                                    $state.go("customer.detail.step_five.list", {id: credit.customer.clientId});
                                });
                        })
            }
        });