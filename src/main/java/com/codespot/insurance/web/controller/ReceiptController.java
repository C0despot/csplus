package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.payment.Receipt;
import com.codespot.insurance.service.receipt.ReceiptService;
import com.codespot.insurance.web.resource.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "receipt")
public class ReceiptController {

    private final
    ReceiptService receiptService;

    @Autowired
    public ReceiptController(ReceiptService receiptService) {
        this.receiptService = receiptService;
    }

    @GetMapping(value = "customer/all")
    @Logger(transaction = Log.Transaction.GET_ALL_RECEIPT_BY_CUSTOMER)
    public ResponseEntity getCustomerAllReceipts(@RequestParam(name = "customer") Long customer,
                                                 @RequestParam(name = "insurance", required = false) Long insurance,
                                                 @RequestParam(name = "invoice", required = false) Long invoice) {
        List<Receipt> receipts = this.receiptService.findAllByCustomerInsuranceInvoice(customer, insurance, invoice);
        return ResponseEntity.ok(receipts);
    }

    @GetMapping(value = "one")
    @Logger(transaction = Log.Transaction.GET_RECEIPT_INFO)
    public ResponseEntity getOne(@RequestParam(name = "customer") Long customer,
                                 @RequestParam(name = "receipt") Long rid) {
        Receipt receipt = this.receiptService.getOne(rid);
        return ResponseEntity.ok(receipt);
    }

    @PostMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.SAVE_RECEIPT_INFO)
    public ResponseEntity save(@RequestBody Receipt receipt) {
        receipt = this.receiptService.save(receipt);
        return ResponseEntity.ok(receipt);
    }
}
