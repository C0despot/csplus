package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.dto.DateFilter;
import com.codespot.insurance.model.dto.InsuranceDto;
import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.dto.RemoveDocument;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.insurance.*;
import com.codespot.insurance.model.entity.payment.Claim;
import com.codespot.insurance.service.claim.ClaimService;
import com.codespot.insurance.service.insurance.InsuranceService;
import com.codespot.insurance.web.resource.Logger;
import com.condos.shared.data.page.C2Criteria;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "insurance")
public class InsuranceController {

    private final
    InsuranceService insuranceService;

    private final
    ClaimService claimService;

    @Autowired
    public InsuranceController(InsuranceService customerService, ClaimService claimService) {
        this.insuranceService = customerService;
        this.claimService = claimService;
    }


    @GetMapping(value = "all")
    @Logger(transaction = Log.Transaction.GET_ALL_INSURANCE_BY_CUSTOMER)
    public ResponseEntity getAllByCustomer(@RequestParam(name = "customer") Long customer) {
        List<InsuranceDetail> insuranceDetails = this.insuranceService.getAll(customer);
        return ResponseEntity.ok(insuranceDetails);
    }

    @GetMapping(value = "one")
    @Logger(transaction = Log.Transaction.GET_INSURANCE_DETAIL)
    public ResponseEntity getOne(@RequestParam(value = "customer") Long customerId,
                                 @RequestParam(value = "insurance") Long insuranceId) {
        InsuranceDto insurance = this.insuranceService.getByCustomerAndInsurance(customerId, insuranceId);
        return ResponseEntity.ok(insurance);
    }

    @GetMapping(value = "branch")
    @Logger(transaction = Log.Transaction.GET_BRANCHES)
    public ResponseEntity getBranches() {
        List<Branch> branches = this.insuranceService.getAvailableBranches();
        return ResponseEntity.ok(branches);
    }

    @GetMapping(value = "coverage")
    @Logger(transaction = Log.Transaction.GET_ALL_INSURANCE_COVERAGE)
    public ResponseEntity getCoverageList() {
        List<Coverage> coverages = this.insuranceService.getAvailableCoverage();
        return ResponseEntity.ok(coverages);
    }

    @GetMapping(value = "carrier")
    @Logger(transaction = Log.Transaction.GET_INSURANCE_AVAILABLE_CARRIERS)
    public ResponseEntity getCarriers() {
        List<InsuranceCarrier> insuranceCarriers = this.insuranceService.getAvailableCarriers();
        return ResponseEntity.ok(insuranceCarriers);
    }

    @PostMapping(value = "carrier")
    @Logger(transaction = Log.Transaction.SAVE_INSURANCE_CARRIER)
    public ResponseEntity saveCarrier(@RequestBody InsuranceCarrier carrier) {
        carrier = this.insuranceService.saveCarrier(carrier);
        return ResponseEntity.ok(carrier);
    }

    @GetMapping(value = "carrier/all")
    @Logger(transaction = Log.Transaction.GET_ALL_INSURANCE_CARRIERS)
    public ResponseEntity getAllCarriers(PageDto pageDto) {
        Page<InsuranceCarrier> insuranceCarriers = this.insuranceService.getAllCarriers(pageDto);
        return ResponseEntity.ok(insuranceCarriers);
    }

    @GetMapping(value = "vehicle")
    @Logger(transaction = Log.Transaction.GET_ALL_INSURANCE_VEHICLES)
    public ResponseEntity getVehicles(@RequestParam(name = "insurance") Long insurance) {
        List<Vehicle> vehicles = this.insuranceService.getVehicles(insurance);
        return ResponseEntity.ok(vehicles);
    }

    @GetMapping(value = "vehicle/one")
    @Logger(transaction = Log.Transaction.GET_VEHICLE_INFO)
    public ResponseEntity getOneVehicle(@RequestParam(name = "id") Long id) {
        Vehicle vehicle = this.insuranceService.getOneVehicle(id);
        return ResponseEntity.ok(vehicle);
    }

    @PostMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.SAVE_VEHICLE)
    public ResponseEntity saveInsurance(@RequestBody InsuranceDto insuranceDto) {
        Insurance insurance = this.insuranceService.save(insuranceDto);
        return ResponseEntity.ok(insurance);
    }

    @GetMapping(value = "all/customer")
    @Logger(transaction = Log.Transaction.GET_ALL_INSURANCE_PRIMITIVE)
    public ResponseEntity getAllPrimitive(@RequestParam(name = "customer") Long customer) {
        List<Insurance> insurances = this.insuranceService.getAllPrimitive(customer);
        return ResponseEntity.ok(insurances);
    }

    @GetMapping(value = "local")
    @Logger(transaction = Log.Transaction.GET_ALL_INSURANCE_LOCALS)
    public ResponseEntity getLocals(@RequestParam(name = "insurance") Long insurance) {
        List<Local> locals = this.insuranceService.getLocals(insurance);
        return ResponseEntity.ok(locals);
    }

    @PostMapping(value = "branch")
    @Logger(transaction = Log.Transaction.CREATE_NEW_BRANCH)
    public ResponseEntity saveInsuranceBranch(@RequestParam(value = "description") String description) {
        Branch branch = this.insuranceService.saveBranch(description);
        return ResponseEntity.ok(branch);
    }

    @GetMapping(value = "renew")
    @Logger(transaction = Log.Transaction.GET_ALL_RENEW_DOCUMENTS)
    public ResponseEntity renew(@NonNull PageDto page,
                                @NonNull DateFilter dateFilter,
                                @NonNull C2Criteria criteria) {
        Page<InsuranceRenew> renews = this.insuranceService.getRenew(page, criteria, dateFilter);
        return ResponseEntity.ok(renews);
    }

    @PostMapping(value = "renew/print")
    @Logger(transaction = Log.Transaction.PRINT_RENEW_LIST)
    public ResponseEntity print(@NonNull PageDto page,
                                @NonNull DateFilter dateFilter,
                                @NonNull C2Criteria criteria) throws IOException {
        String url = this.insuranceService.printRenew(page, criteria, dateFilter);
        return ResponseEntity.ok(String.format("{\"url\": \"%s\"}", url));
    }

    @DeleteMapping(value = "document")
    @Logger(transaction = Log.Transaction.DELETE_INSURANCE_DOCUMENT)
    public ResponseEntity deleteDocument(@RequestBody RemoveDocument documentInfo) {
        this.insuranceService.deleteDocument(documentInfo);
        return ResponseEntity.ok().build();
    }


    @DeleteMapping(value = "element")
    @Logger(transaction = Log.Transaction.DELETE_INSURANCE_ELEMENT)
    public ResponseEntity deleteElement(Long element, ElementType type) {
        this.insuranceService.deleteElement(element, type);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "claim")
    @Logger(transaction = Log.Transaction.GET_CLAIM_INFO)
    public ResponseEntity getOne(Long id) {
        Claim claim = this.claimService.getOne(id);
        return ResponseEntity.ok(claim);
    }

    @PostMapping(value = "claim")
    @Logger(transaction = Log.Transaction.SAVE_CLAIM_INFO)
    public ResponseEntity save(@RequestBody Claim claim) {
        claim = this.claimService.save(claim);
        return ResponseEntity.ok(claim);
    }

    @GetMapping(value = "claim/all")
    @Logger(transaction = Log.Transaction.GET_ALL_CLAIMS)
    public ResponseEntity getAll(Long customer) {
        List<Claim> claims = this.claimService.getAllByCustomer(customer);
        return ResponseEntity.ok(claims);
    }

    public enum ElementType {
        LOCAL,
        VEHICLE,
        DEPENDENT
    }
}
