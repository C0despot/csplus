package com.codespot.insurance.service.claim;

import com.codespot.insurance.model.entity.payment.Claim;

import java.util.List;

public interface ClaimService {

    List<Claim> getAllByCustomer(Long customerId);

    List<Claim> getAllByInsurance(Long insurance);

    Claim getOne(Long claimId);

    Claim save(Claim claim);

    void remove(Claim claim);

    void remove(List<Claim> claims);
}
