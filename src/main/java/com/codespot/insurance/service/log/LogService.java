package com.codespot.insurance.service.log;

import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.LogDetail;
import com.condos.shared.data.page.C2Criteria;
import org.springframework.data.domain.Page;

/**
 * @author Jose An Rodriguez
 */
public interface LogService {

    Log one(String id);

    void store(String id, Log.Transaction transaction, String ip, String params, String method);

    void detail(LogDetail detail, String log);

    Page<LogDetail> transactionLogs(PageDto pageDto, C2Criteria criteria);

}
