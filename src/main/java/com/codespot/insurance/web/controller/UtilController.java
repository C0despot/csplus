package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.dto.MonthOfYearDto;
import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.LogDetail;
import com.codespot.insurance.security.model.type.Types;
import com.codespot.insurance.service.log.LogService;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.codespot.insurance.service.stats.StatisticService;
import com.codespot.insurance.service.utility.UtilityService;
import com.codespot.insurance.web.resource.Logger;
import com.codespot.insurance.web.resource.Transaction;
import com.condos.shared.data.page.C2Criteria;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping(value = "util")
public class UtilController {

    private final
    UtilityService utilityService;

    private final
    StatisticService statisticService;

    private final
    LoggerActivityService loggerActivityService;

    private final
    LogService logService;

    private final
    Environment environment;

    @Autowired
    public UtilController(UtilityService utilityService, StatisticService statisticService, LoggerActivityService loggerActivityService, LogService logService, Environment environment) {
        this.utilityService = utilityService;
        this.statisticService = statisticService;
        this.loggerActivityService = loggerActivityService;
        this.logService = logService;
        this.environment = environment;
    }

    @GetMapping(value = "validate")
    @Logger(transaction = Log.Transaction.VALIDATE_TRANSACTION_CONSTRAINS)
    public ResponseEntity validateConstrains(@RequestParam(name = "num") String constrainNumber,
                                             @RequestParam(name = "tns") Transaction transaction) {
        boolean exists = this.utilityService.validateConstrains(constrainNumber, transaction);

        if (exists)
            return ResponseEntity.unprocessableEntity().body(transaction.getTransactionErrorMessage());

        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "sequence")
    @Logger(transaction = Log.Transaction.GET_NEXT_SEQUENCE_VALUE)
    public ResponseEntity nextSequence(@RequestParam(name = "tns") Transaction transaction) {
        Long sequence = this.utilityService.nextSequence(transaction);
        Transaction.TransactionValue transactionValue = transaction.getTransactionValue(sequence);

        if (Objects.nonNull(transactionValue))
            return ResponseEntity.ok(transactionValue);

        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "month/current")
    @Logger(transaction = Log.Transaction.GET_CURRENT_MONTH_NAME)
    public ResponseEntity getMonth(Integer index) {
        if (Objects.isNull(index)) {
            Types.Month month = this.statisticService.getCurrentMonth();
            return ResponseEntity.ok(month);
        }
        Types.Month month = Types.Month.of(index);
        return ResponseEntity.ok(Objects.requireNonNull(month));
    }

    @GetMapping(value = "month/available")
    @Logger(transaction = Log.Transaction.GET_AVAILABLE_YEAR_MONTHS)
    public ResponseEntity getAvailableYear() {
        List<MonthOfYearDto> monthOfYears = new ArrayList<>();
        monthOfYears.add(MonthOfYearDto.builder().year(0).month(Types.Month.ALL).build());
        for (int i = 2017; i <= LocalDate.now().getYear(); i++) {
            int year = i;
            Arrays.asList(Types.Month.values())
                    .forEach(month -> {
                        MonthOfYearDto monthOfYear = MonthOfYearDto.builder().year(year).month(month).build();
                        monthOfYears.add(monthOfYear);
                    });
        }
        return ResponseEntity.ok(monthOfYears);
    }

    @GetMapping(value = "logs")
    @Logger(transaction = Log.Transaction.GET_ALL_TRANSACTIONAL_LOGS)
    public ResponseEntity getLogs(PageDto pageDto, C2Criteria criteria) {
        Page<LogActivity> logs = loggerActivityService.logs(pageDto, criteria);
        return ResponseEntity.ok(logs);
    }

    @GetMapping(value = "transactions")
    @Logger(transaction = Log.Transaction.GET_ALL_TRANSACTIONAL_LOGS)
    public ResponseEntity getTransactionLogs(PageDto pageDto, C2Criteria criteria, Boolean all) {
        Page<LogDetail> logs = logService.transactionLogs(pageDto, criteria);
        return ResponseEntity.ok(logs);
    }

    @GetMapping(value = "version")
    public ResponseEntity<AppEnv> appVersion(@Value(value = "${app.version}") String appVersion) {
        String[] profiles = environment.getActiveProfiles();
        return ResponseEntity
                .ok(AppEnv.builder()
                        .date(new Date())
                        .environment(profiles[0])
                        .version(appVersion)
                        .build());
    }

    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    static class AppEnv {
        public String version;
        public String environment;
        public Date date;
    }
}
