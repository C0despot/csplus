package com.codespot.insurance.service.logger;

import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.repository.LogActivityRepository;
import com.codespot.insurance.model.spec.LogSpecification;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.repository.UserRepository;
import com.condos.shared.data.page.C2Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service(value = "defaultLoggerActivityService")
public class DefaultLoggerActivityService implements LoggerActivityService {

    private final
    LogActivityRepository logActivityRepository;

    private final
    UserRepository userRepository;

    private final
    HttpServletRequest request;

    @Autowired
    public DefaultLoggerActivityService(LogActivityRepository logActivityRepository, UserRepository userRepository, HttpServletRequest servletRequest) {
        this.logActivityRepository = logActivityRepository;
        this.userRepository = userRepository;
        this.request = servletRequest;
    }

    @Override
    public void log(String msg, LogActivity.Type type, LogActivity.Scope scope, LogActivity.Action action, String doc) {
        LogActivity activity = LogActivity
                .builder()
                .type(type)
                .action(action)
                .date(new Date())
                .scope(scope)
                .document(doc)
                .message(msg)
                .user(user())
                .ip(request.getRemoteAddr())
                .build();
        this.logActivityRepository.save(activity);
    }

    @Override
    public void success(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc) {
        log(msg, LogActivity.Type.SUCCESS, scope, action, doc);
    }

    @Override
    public void error(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc) {
        log(msg, LogActivity.Type.ERROR, scope, action, doc);
    }

    @Override
    public void info(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc) {
        log(msg, LogActivity.Type.INFO, scope, action, doc);
    }

    @Override
    public void warning(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc) {
        log(msg, LogActivity.Type.WARNING, scope, action, doc);
    }

    @Override
    public void fail(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc) {
        log(msg, LogActivity.Type.FAIL, scope, action, doc);
    }

    @Override
    public Page<LogActivity> logs(PageDto pager, C2Criteria criteria) {
        LogSpecification specification = LogSpecification.of(criteria);
        Pageable pageable = PageRequest.of(pager.getPage(), pager.getSize(), new Sort(pager.getSort(), pager.getOrderBy()));
        return this.logActivityRepository.findAll(specification, pageable);
    }

    private User user() {
        try {
            String id = SecurityContextHolder.getContext().getAuthentication().getName();
            return this.userRepository.findById(Long.parseLong(id)).orElse(null);
        } catch (Exception e) {
            return null;
        }
    }
}
