package com.codespot.insurance.model.dto;

import com.codespot.insurance.security.model.type.Types;
import lombok.Data;

@Data
public class DateFilter {

    private Types.Month month;
    private Integer year;

}
