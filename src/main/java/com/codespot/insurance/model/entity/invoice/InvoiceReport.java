package com.codespot.insurance.model.entity.invoice;

import com.codespot.insurance.model.entity.customer.CustomerCurrentReport;
import com.codespot.insurance.model.entity.payment.PaymentDocumentReport;
import com.codespot.insurance.model.listener.ReadOnlyEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Entity
@Table(name = "INVOICE_REPORT_VW")
@EntityListeners(ReadOnlyEntity.class)
public class InvoiceReport {

    @Id
    @Column(name = "ID")
    private Long id;

    @JsonIgnore
    @JoinColumn(name = "INSURANCE")
    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerCurrentReport insurance;

    @Column(name = "INVOICE")
    private String invoice;

    @Column(name = "EMISSION_DATE")
    private Date emissionDate;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "PRIME")
    private Double prime;

    @Column(name = "BALANCE")
    private Double balance;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "invoice")
    private List<PaymentDocumentReport> paymentDocuments;

}
