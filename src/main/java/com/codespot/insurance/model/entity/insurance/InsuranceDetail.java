package com.codespot.insurance.model.entity.insurance;


import com.codespot.insurance.model.entity.customer.Customer;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "INSURANCE_DETAIL_VW")
public class InsuranceDetail {

    @Id
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER")
    private Customer customer;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "PRIME")
    private Double prime;

    @ManyToOne
    @JoinColumn(name = "BRANCH")
    private Branch branch;

    @Column(name = "INSURANCE_NO")
    private String insuranceNo;

    @Column(name = "CARRIER")
    private String carrier;

    @Column(name = "BALANCE")
    private Double balance;

    @Transient
    private Boolean selected = false;
}
