angular
    .module(APP)
    .controller('AuditController', function ($scope, $uibModalInstance, $stateParams, InsuranceService, document, NotificationService) {

        $scope.$uibModalInstance = $uibModalInstance;
        $scope.title = "Audición de Documento";
        $scope.document = angular.copy(document);

        $scope.editModal = function (dependent) {
            $uibModalInstance.close({dependent: dependent});
        };

        $scope.newModal = function () {
            $uibModalInstance.close({isNew: true});
        };

        $scope.showTooltip = function (plan) {
            NotificationService.showCoverage(plan);
        }
    });