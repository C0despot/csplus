package com.codespot.insurance.security.model.entity;

import com.codespot.insurance.model.util.Utility;
import com.codespot.insurance.security.model.type.Types;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USER")
public class User implements UserDetails {

    @Id
    @Column(name = "USER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "UUID")
    private String uuid;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "PHONE_NUMBER")
    private String phone;

    @Column(name = "EMAIL_ADDRESS")
    private String email;

    @Column(name = "NAME")
    private String name;

    @Column(name = "IDENTIFICATION")
    private String identification;

    @Column(name = "BIRTH_DATE")
    private Date birthDate;

    @JsonIgnore
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "VALIDATED")
    private Boolean validated;

    @Column(name = "LOCKED")
    private Boolean locked;

    @Column(name = "ROLE")
    @Enumerated(value = EnumType.STRING)
    private Types.Role role;

    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "MODIFIED_DATE")
    private Date modifiedDate;

    @Column(name = "GENDER")
    private String gender;

    @Column(name = "PROFILE_IMAGE")
    String image;

    @Column(name = "REMOVE_GRANT")
    private Boolean removeGrant;

    @Column(name = "ROLE_DESCRIPTION")
    private String roleDescription;

    @Column(name = "STATUS")
    @Enumerated(value = EnumType.STRING)
    private Types.UserStatus status;

    @Transient
    public String getRoleName(){
        return this.role.value();
    }

    @Transient
    public String getStatusName(){
        return this.status.value();
    }

    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(this.role.name()));
        return grantedAuthorities;
    }

    public User (String id){
        this.id = Long.parseLong(id);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return validated;
    }

    public boolean hasRemoveGrant() {
        return removeGrant;
    }

    public String getRoleDescription() {
        return Utility.capitalize(roleDescription);
    }

    public boolean isNew(){
        return Objects.isNull(id);
    }
}