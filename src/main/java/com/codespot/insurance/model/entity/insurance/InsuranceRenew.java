package com.codespot.insurance.model.entity.insurance;

import com.codespot.insurance.model.entity.customer.Customer;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "INSURANCE_RENEW_VW")
public class InsuranceRenew {

    @Id
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER")
    private Customer customer;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CUSTOMER_INDIVIDUAL_ID")
    private String identification;

    @Column(name = "DEFAULT_PHONE_NUMBER")
    private String phone;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "PRIME")
    private Double prime;

    @Column(name = "YEAR")
    private Integer year;

    @Column(name = "MONTH")
    private Integer month;

    @ManyToOne
    @JoinColumn(name = "BRANCH")
    private Branch branch;

    @Column(name = "INSURANCE_NO")
    private String insuranceNo;

    @ManyToOne
    @JoinColumn(name = "CARRIER")
    private InsuranceCarrier carrier;

    @Column(name = "BALANCE")
    private Double balance;

    @Transient
    private Boolean selected = false;

}
