package com.codespot.insurance.model.repository.customer;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.CustomerHistoricalReport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerHistoricalReportRepository extends JpaRepository<CustomerHistoricalReport, Long> {

    List<CustomerHistoricalReport> findAllByCustomerEquals(Customer customer);

    List<CustomerHistoricalReport> findAllByIdEquals(Long id);

}
