package com.codespot.insurance.report.model;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import lombok.experimental.UtilityClass;

public class Header {

    @UtilityClass
    public static class Insurance {
        public String INSURANCE_NUMBER = "Número Póliza";
        public String START_DATE = "Inicio Vigencia";
        public String END_DATE = "Fin Vigencia";
        public String BALANCE = "Balance";
        public String CURRENCY = "Moneda";
    }

    @UtilityClass
    public static class InsuranceRenew {
        public String CUSTOMER = "Cliente";
        public String PHONE = "Teléfono";
        public String INSURANCE_NO = "Póliza";
        public String COMPANY = "Aseguradora";
        public String BRANCH = "Ramo";
        public String DUE_DATE = "Vencimiento";
        public String PRIME = "Prima";
        public String BALANCE = "Balance";
    }

    @UtilityClass
    public static class Invoice {
        public String DOC_TYPE = "Tipo de Doc";
        public String INVOICE_NUMBER = "Número Factura";
        public String MOV_DATE = "Movimiento";
        public String START_DATE = "Inicio Vigencia";
        public String END_DATE = "Fin Vigencia";
        public String BALANCE = "Facturado";
        public String PENDING = "Pendiente";
        public String VALUE = "FACTURA";
    }

    @UtilityClass
    public static class PaymentDoc {
        public String DOC_TYPE = "Tipo Documento";
        public String DOC_NUM = "Número Documento";
        public String MOV_DATE = "Fecha Movimiento";
        public String AMOUNT = "Monto Acreditado";
    }

    @UtilityClass
    public static class ColorProvider {
        public Color PRIMARY = new DeviceRgb(26, 179, 145);
        public Color SUCCESS = new DeviceRgb(28, 132, 198);
        public Color WARNING = new DeviceRgb(248, 172, 89);
        public Color WHITE = new DeviceRgb(255, 255, 255);
        public Color BLACK = new DeviceRgb(0, 0, 0);
        public Color LIGHT_GRAY = new DeviceRgb(60, 60, 60);
    }
}
