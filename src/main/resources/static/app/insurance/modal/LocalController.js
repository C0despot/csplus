angular
    .module('inspinia')
    .controller('LocalController', function ($scope, $rootScope, $uibModalInstance, $stateParams, InsuranceService, locals, NotificationService) {

        $scope.user = JSON.parse(localStorage.getItem("user"));
        $scope.$uibModalInstance = $uibModalInstance;
        $scope.localTitle = "Loading...";
        $scope.localTitle = "Lista de locales asegurados";
        $scope.locals = angular.copy(locals);

        $scope.editModal = function (local) {
            $uibModalInstance.close({local: local});
        };

        $scope.newModal = function () {
            $uibModalInstance.close({isNew: true});
        };

        $scope.showTooltip = function (plan) {
            NotificationService.showCoverage(plan);
        };

        $scope.delete = (id) => {
            NotificationService
                .confirm(
                    "¿Borrar local?",
                    "Está seguro que desea borrar el local seleccionado?",
                    () => {
                        InsuranceService
                            .deleteElement(id, 'LOCAL')
                            .then((ignored) => {
                                location.reload();
                            }, (ignored) => {
                            })
                    })
        };

    });