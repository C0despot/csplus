angular
    .module(APP)
    .controller('PasswordController', function ($scope, $uibModalInstance, UserService, NotificationService) {

        $scope.user = JSON.parse(localStorage.getItem("user"));
        $scope.$uibModalInstance = $uibModalInstance;
        $scope.email = '';
        $scope.loading = false;

        $scope.password = {
            newPsw: '',
            confirm: '',
            user: $scope.user
        };

        $scope.changePsw = (password) => {
            $scope.loading = true;
            UserService
                .doSetPassword(password)
                .then((response) => {
                    let userObj = response.data;
                    delete userObj['authorities'];
                    localStorage.setItem("user", JSON.stringify(userObj));
                    NotificationService.successMsj("Se ha confirmado su nueva contraseña.");
                    $uibModalInstance.close();
                }, () => {
                    NotificationService.errorMsj("Lo sentimos, ambas contrañas no coinciden.");
                })
                .finally(()=>{
                    $scope.loading = false;
                });
        }

    });