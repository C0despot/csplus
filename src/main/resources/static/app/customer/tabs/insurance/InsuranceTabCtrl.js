angular
    .module(APP)
    .controller('InsuranceTabCtrl', function ($scope, $state, $stateParams, CustomerService) {

        $scope.insurances = [];
        $scope.selectedItem = 0;
        $scope.selected = null;
        $scope.state = $state;

        $scope.$on('$viewContentLoaded', function () {
            let id = $stateParams.id;
            CustomerService
                .getPlans(id)
                .then(function (response) {
                    $scope.insurances = response.data;
                }, function (error) {

                });
        });

        $scope.selectRow = function (index) {
            let value = $scope.insurances[index].selected;
            angular.forEach($scope.insurances, function (insurance) {
                insurance.selected = false;
            });
            $scope.insurances[index].selected = value;
            $scope.selected = $scope.insurances[index].id;
            $scope.selectedItem = value ? $scope.insurances[index].id : 0;
        }

    });