package com.codespot.insurance.model.entity.customer;

import com.codespot.insurance.model.entity.invoice.InvoiceReport;
import com.codespot.insurance.model.listener.ReadOnlyEntity;
import com.codespot.insurance.security.model.type.Types;
import lombok.Getter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Entity
@EntityListeners(ReadOnlyEntity.class)
@Table(name = "CUSTOMER_HISTORICAL_REPORT_VW")
public class CustomerHistoricalReport {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "INSURANCE")
    private String insurance;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER")
    private Customer customer;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "BALANCE")
    private Double balance;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "CURRENCY")
    private Types.Currency currency;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "insurance")
    private List<InvoiceReport> invoices;
}
