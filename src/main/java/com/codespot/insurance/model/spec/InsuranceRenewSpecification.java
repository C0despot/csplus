package com.codespot.insurance.model.spec;

import com.codespot.insurance.model.entity.insurance.InsuranceRenew;
import com.condos.shared.data.page.C2Criteria;
import com.condos.shared.util.DataUtil;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class InsuranceRenewSpecification implements Specification<InsuranceRenew> {

    private C2Criteria criteria;

    public static InsuranceRenewSpecification of(C2Criteria criteria) {
        return new InsuranceRenewSpecification(criteria);
    }

    private InsuranceRenewSpecification(C2Criteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate
            (Root<InsuranceRenew> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        return DataUtil.getPredicate(root, builder, criteria);
    }
}