package com.codespot.insurance.model.entity.insurance;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "LOCAL")
public class Local {

    @Id
    @Column(name = "LOCAL_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "LOCAl_NAME")
    private String name;

    @Column(name = "LOCAL_TYPE")
    private String type;

    @Column(name = "BUILDING_TYPE")
    private String buildingType;

    @Column(name = "LOCAL_ADDRESS")
    private String address;

    @Column(name = "GOOD_LIST")
    private String goods;

    @ManyToOne
    @JoinColumn(name = "POLICY_LINKED")
    private Insurance insurance;


    public static Local dummy(Long element) {
        Local local = new Local();
        local.setId(element);
        return local;
    }
}
