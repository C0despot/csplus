angular
    .module('inspinia')
    .controller('CustomerGeneralInfoCtrl', function ($scope, CustomerService, $stateParams, NotificationService) {

        $scope.title = 'Información General';
        $scope.isAddMode = $stateParams.id === 'new';
        $scope.customer = {
            id: $stateParams.id === 'new' ? null : $stateParams.id
        };
        $scope.$on('$stateChangeStart', function (event, toState) {
            $scope.title = toState.data.pageTitle;
            if (!$scope.isAddMode) {
                let id = $stateParams.id;
                CustomerService
                    .getExtendedCustomerInfo(id)
                    .then(function (response) {
                        $scope.customer = response.data;
                    }, function (errorResp) {
                        NotificationService.errorMsj(errorResp.data.message);
                    });
            }
        });

        $scope.$on('$viewContentLoaded', function () {
            if (!$scope.isAddMode) {
                let id = $stateParams.id;
                CustomerService
                    .getExtendedCustomerInfo(id)
                    .then(function (response) {
                        $scope.customer = response.data;
                    }, function (errorResp) {
                        NotificationService.errorMsj(errorResp.data.message);
                    });
            }
        });

    });