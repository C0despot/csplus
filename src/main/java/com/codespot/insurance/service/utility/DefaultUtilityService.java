package com.codespot.insurance.service.utility;

import com.codespot.insurance.model.repository.*;
import com.codespot.insurance.model.repository.customer.CustomerDetailRepository;
import com.codespot.insurance.model.repository.insurance.InsuranceRepository;
import com.codespot.insurance.web.resource.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service(value = "defaultUtilityService")
public class DefaultUtilityService implements UtilityService {


    private final
    CustomerDetailRepository customerDetailRepository;

    private final
    InsuranceRepository insuranceRepository;

    private final
    InvoiceRepository invoiceRepository;

    private final
    ReceiptRepository receiptRepository;

    private final
    CreditRepository creditRepository;

    private final
    ClaimRepository claimRepository;

    @Autowired
    public DefaultUtilityService(InvoiceRepository invoiceRepository, InsuranceRepository insuranceRepository, ReceiptRepository receiptRepository, CustomerDetailRepository customerDetailRepository, CreditRepository creditRepository, ClaimRepository claimRepository) {
        this.invoiceRepository = invoiceRepository;
        this.insuranceRepository = insuranceRepository;
        this.receiptRepository = receiptRepository;
        this.customerDetailRepository = customerDetailRepository;
        this.creditRepository = creditRepository;
        this.claimRepository = claimRepository;
    }

    @Override
    public boolean validateConstrains(String value, Transaction transaction) {

        switch (transaction) {
            case INSURANCE:
                return Objects.nonNull(this.insuranceRepository.findByInsuranceNumberEquals(value));
            case INVOICE:
                return Objects.nonNull(this.invoiceRepository.findByInvoiceNoEquals(value));
            case RECEIPT:
                return Objects.nonNull(this.receiptRepository.findByReceiptNoEquals(value));
            case CREDIT:
                return Objects.nonNull(this.creditRepository.findByDocNoEquals(value));
            case CLAIMS:
                return Objects.nonNull(this.claimRepository.findByDocNoEquals(value));
        }
        return false;
    }

    @Override
    public long nextSequence(Transaction transaction) {
        return this.customerDetailRepository.findNextTransactionId(transaction.getTransactionIndexer().name()) + 1;
    }
}
