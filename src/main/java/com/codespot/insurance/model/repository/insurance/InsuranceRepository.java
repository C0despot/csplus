package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InsuranceRepository extends JpaRepository<Insurance, Long> {

    Insurance findByCustomerEqualsAndIdEquals(Customer customer, Long id);

    List<Insurance> findAllByCustomerClientId(Long id);

    Insurance findByInsuranceNumberEquals(String number);
}
