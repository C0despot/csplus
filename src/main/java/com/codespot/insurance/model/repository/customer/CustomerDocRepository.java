package com.codespot.insurance.model.repository.customer;

import com.codespot.insurance.model.entity.customer.CustomerDoc;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerDocRepository extends JpaRepository<CustomerDoc, String> {

    List<CustomerDoc> findAllByDocumentContaining(String doc, Pageable pageable);
}
