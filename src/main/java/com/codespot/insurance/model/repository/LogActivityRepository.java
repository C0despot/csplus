package com.codespot.insurance.model.repository;

import com.codespot.insurance.model.entity.LogActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LogActivityRepository extends JpaRepository<LogActivity, Long>, JpaSpecificationExecutor<LogActivity> {
}
