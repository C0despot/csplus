package com.codespot.insurance.security.model;

import com.codespot.insurance.security.model.entity.User;
import lombok.Data;

@Data
public class PasswordChange {
    private String newPsw;
    private String current;
    private String confirm;
    private User user;
}
