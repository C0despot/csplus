angular
    .module(APP)
    .controller('CreditTabCtrl',
        function ($scope, $state, $stateParams, CustomerService) {

            $scope.credits = [];
            $scope.selectedItem = 0;
            $scope.selected = null;
            $scope.state = $state;

            $scope.$on('$viewContentLoaded', () => {
                let id = $stateParams.id;
                CustomerService
                    .getCredits(id)
                    .then((response) => {
                        $scope.credits = response.data;
                    });
            });

            $scope.selectRow = (index) => {
                let value = $scope.credits[index].selected;
                angular.forEach($scope.credits, (credit) => {
                    credit.selected = false;
                });
                $scope.credits[index].selected = value;
                $scope.selected = $scope.credits[index].id;
                $scope.selectedItem = value ? $scope.credits[index].id : 0;
            };

            $scope.creditType = (index) => {
                return CREDIT_TYPES[index].toUpperCase();
            };

        });