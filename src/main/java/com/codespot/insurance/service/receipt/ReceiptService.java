package com.codespot.insurance.service.receipt;

import com.codespot.insurance.model.entity.payment.Receipt;

import java.util.List;

public interface ReceiptService {

    List<Receipt> findAllByCustomerInsuranceInvoice(Long customer, Long insurance, Long invoice);

    Receipt getOne(Long receipt);

    Receipt save(Receipt receipt);
}
