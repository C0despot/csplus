#!/usr/bin/env bash
read -p "Enter commit message: "  message;
git add .;
git commit -m "$message";
git push origin HEAD;
mvn clean install compile -DskipTests=true;
eb deploy;

