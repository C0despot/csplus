package com.codespot.insurance.security.web;

import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.model.type.Types;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.web.resource.Logger;
import com.condos.shared.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "oauth")
public class AuthController {

    private final
    TokenStore tokenStore;

    private final
    UserService userService;

    @Autowired
    public AuthController(TokenStore tokenStore, UserService userService) {
        this.tokenStore = tokenStore;
        this.userService = userService;
    }

    @GetMapping(value = "revoke")
    @Logger(transaction = Log.Transaction.REVOKE_ACCESS_TOKEN)
    public ResponseEntity logout(String access_token) {
        if (access_token != null) {
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(access_token);
            tokenStore.removeAccessToken(accessToken);
            return ResponseEntity.status(401).body("Sesion cerrada exitosamente!");
        }
        throw new UnauthorizedException("Token expirado o inválido!", access_token, LogActivity.Scope.AUTH.name(), LogActivity.Action.CLOSED.name());
    }

    @GetMapping(value = "validate")
    @Logger(transaction = Log.Transaction.VALIDATE_ACCESS_TOKEN)
    public ResponseEntity token(@RequestParam(name = "access_token") String at) {
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(at);
        if (accessToken == null) {
            return ResponseEntity.status(401).build();
        }
        return ResponseEntity.status(206).body("OK");
    }

    @GetMapping(value = "authorities")
    @Logger(transaction = Log.Transaction.VALIDATE_ACCOUNT_AUTHORITIES)
    public ResponseEntity validateAuthorities(String uuid) {
        User user = this.userService.findByUuid(uuid);
        if (user.getRole().equals(Types.Role.ADMIN))
            return ResponseEntity.ok(Types.Role.ADMIN);
        throw new UnauthorizedException(String.format("Usuario [%s] no está autorizado!", uuid), uuid, LogActivity.Scope.INSURANCE.name(), LogActivity.Action.READ.name());
    }

}
