angular
    .module(APP)
    .controller('EditReceiptCtrl',
        function ($scope, $state, $uibModal, $stateParams, ReceiptService, InvoiceService, InsuranceService, NotificationService, CustomerService) {

            $scope.edit = true;
            $scope.hidden = false;
            $scope.loading = false;
            $scope.greenLight = false;
            $scope.validating = false;
            $scope.receipt = {
                receiptNo: null,
                customer: null,
                startDate: new Date(),
                endDate: new Date(),
                beginningDate: new Date(),
                dueDate: new Date(),
                invoice: {
                    insurance: {
                        insuranceNumber: null
                    }
                },
                user: getUser()
            };
            $scope.insurances = [];
            $scope.$on('$viewContentLoaded', () => {
                let customer = $stateParams.id;
                let receipt = $stateParams.receipt;

                InvoiceService
                    .allByCustomer(customer)
                    .then((response) => {
                        $scope.invoices = response.data;
                    });
                ReceiptService
                    .getOne(customer, receipt)
                    .then((response) => {
                        $scope.receipt = response.data;
                    });
                InsuranceService
                    .getByCustomer(customer)
                    .then((response) => {
                        $scope.insurances = response.data;
                    });
            });

            $scope.validateAmount = () => {
                if (!$scope.receipt.invoice)
                    return false;

                $scope.receipt.insurance = angular.copy($scope.receipt.invoice.insurance);
                let balance = angular.copy($scope.receipt.invoice.balance);
                let amount = angular.copy($scope.receipt.amount);
                balance = numberFormatter(balance);
                amount = numberFormatter(amount);
                if (balance < amount)
                    $("#amount").addClass('has-error');
                else
                    $("#amount").removeClass('has-error');
            };

            $scope.doSave = () => {
                console.dir($scope.receipt);
                let receipt = angular.copy($scope.receipt);
                receipt.startDate = new Date(receipt.startDate);
                receipt.endDate = new Date(receipt.endDate);

                receipt.amount = numberFormatter(receipt.amount);

                $scope.loading = true;
                ReceiptService
                    .saveOne(receipt)
                    .then(() => {
                        NotificationService.successMsj();
                        $state.go("customer.detail.step_four.list", {id: $stateParams.id});
                    }, (response) => {
                        NotificationService.errorMsj(response.data.message);
                    })
                    .finally(() => {
                        $scope.loading = false;
                    });
            };

            $scope.validateMe = () => {
                $scope.validating = true;
                InsuranceService
                    .validateNumber($scope.receipt.receiptNo, 'RECEIPT')
                    .then(() => {
                        $("#receiptNo").removeClass("has-error");
                    }, () => {
                        $("#receiptNo").addClass("has-error");
                    })
                    .finally(() => {
                        $scope.validating = false;
                    })
            };

            $scope.showAuditedItem = () => {
                let receipt = angular.copy($scope.receipt);
                $uibModal.open({
                    templateUrl: 'app/report/audit_modal.html',
                    controller: 'AuditController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'md',
                    windowClass: "animated fadeIn",
                    resolve: {
                        document: function () {
                            return {
                                id: receipt.receiptNo,
                                type: 'RECIBO',
                                createdBy: receipt.createdBy,
                                createdDate: dateFormatter(receipt.creationDate),
                                modifiedDate: dateFormatter(receipt.modificationDate),
                                modifiedBy: receipt.modifiedBy
                            }
                        }
                    }
                });
            };

            $scope.deleteItem = () => {
                const receipt = angular.copy($scope.receipt);
                NotificationService
                    .confirmPassword(
                        (password) => {
                            let deleteObj = {
                                documentNo: receipt.id,
                                type: 'RECEIPT',
                                password: password
                            };
                            InsuranceService
                                .deleteObj(deleteObj)
                                .then(() => {
                                    $state.go("customer.detail.step_four.list", {id: receipt.customer.clientId});
                                });
                        })
            }
        });