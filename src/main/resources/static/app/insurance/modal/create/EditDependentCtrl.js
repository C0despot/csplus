angular
    .module('inspinia')
    .controller('EditDependentCtrl', function ($scope, $uibModalInstance, $stateParams, InsuranceService, dependent) {

        $scope.uibModalInstance = $uibModalInstance;
        $scope.dependentTitle = "Loading...";
        $scope.dependent = {};
        $scope.coverages = [];
        $scope.isEditMode = true;
        setTimeout(function () {
            $scope.dependent = dependent;
            $scope.dependentTitle = `Edición de Dependiente`.toLowerCase();
            $scope.$apply();
        }, 100);
    });