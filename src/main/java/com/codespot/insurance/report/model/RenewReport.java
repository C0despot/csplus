package com.codespot.insurance.report.model;

import com.codespot.insurance.model.entity.insurance.InsuranceRenew;
import com.codespot.insurance.model.util.Utility;
import com.codespot.insurance.security.model.type.Types;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfOutputIntent;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.DottedBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.zugferd.ZugferdConformanceLevel;
import com.itextpdf.zugferd.ZugferdDocument;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.pdfbox.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

@Component
@NoArgsConstructor
@SuppressWarnings(value = "Duplicates")
public class RenewReport {

    @Value(value = "${report.fonts.color}")
    String ICC;

    @Value(value = "${report.fonts.regular}")
    String REGULAR;

    @Value(value = "${report.fonts.bold}")
    String BOLD_FONT;

    private final
    String FILE_DEST = "src/main/resources/static";

    private final
    String NEWLINE = "\n";

    private final
    String COMPANY_NAME = "Gabriel del Rosario & Asociados, SRL.";

    public File buildPdf(List<InsuranceRenew> report, Types.Month month, Integer year) throws IOException {
        String url = "temp.pdf";
        new FileOutputStream(url).close();
        File file = new File(url);
        InputStream icc = new BufferedInputStream(new URL(ICC).openStream());
        InputStream regular = new BufferedInputStream(new URL(REGULAR).openStream());
        byte[] bytes = IOUtils.toByteArray(regular);
        ZugferdDocument pdfDocument = PdfDocument.of(file.getPath(), icc);
        PageSize pageSize = new PageSize(841, 594);
        AtomicReference<Document> document = new AtomicReference<>(new Document(pdfDocument, pageSize));
        document.get().setFont(PdfFontFactory.createFont(bytes, true)).setFontSize(12);
        document.get().add(getCompanyName());
        document.get().add(getAddress(month, year));
        document.set(printInsuranceRenew(report, document.get()));
        document.get().close();
        return new File(url);
    }

    private Document printInsuranceRenew(List<InsuranceRenew> renews, Document document) {
        document.add(getInsuranceHeader());
        renews.forEach(renew -> document.add(printInsuranceBody(renew)));
        return document;
    }

    private String getDateString(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        return sdf.format(d);
    }

    private Table getCompanyName() {
        return TableBuilder
                .of(100f)
                .setBold()
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setBorder(Border.NO_BORDER)
                .setFontSize(14)
                .addCell(createCell(COMPANY_NAME)
                        .setFontColor(Header.ColorProvider.SUCCESS)
                        .setHorizontalAlignment(HorizontalAlignment.CENTER)
                        .setTextAlignment(TextAlignment.CENTER));
    }

    private Table getInsuranceHeader() {
        return TableBuilder
                .of(27f, 10f, 11f, 9f, 17f, 8f, 10f, 8f)
                .setBorder(new DottedBorder(Header.ColorProvider.PRIMARY, 0.25f))
                .setBorderBottom(Border.NO_BORDER)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(9)
                .setBold()
                .addCell(createCell(Header.InsuranceRenew.CUSTOMER)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.InsuranceRenew.PHONE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.InsuranceRenew.INSURANCE_NO)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.InsuranceRenew.BRANCH)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.InsuranceRenew.COMPANY)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.InsuranceRenew.PRIME)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.InsuranceRenew.DUE_DATE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Header.InsuranceRenew.BALANCE)
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)));
    }

    private Table printInsuranceBody(InsuranceRenew renew) {
        return TableBuilder
                .of(27f, 10f, 11f, 9f, 17f, 8f, 10f, 8f)
                .setBorder(new DottedBorder(Header.ColorProvider.PRIMARY, 0.25f))
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setFontSize(9)
                .addCell(createCell(Utility.capitalize(renew.getCustomer().getName()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(renew.getCustomer().getPhone())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(renew.getInsuranceNo().toUpperCase())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(renew.getBranch().getDescription().toUpperCase())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(renew.getCarrier().getName().toUpperCase())
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Utility.getCurrencyValue(renew.getPrime()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(getDateString(renew.getEndDate()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)))
                .addCell(createCell(Utility.getCurrencyValue(renew.getBalance()))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBorderLeft(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f))
                        .setBorderRight(new DottedBorder(Header.ColorProvider.PRIMARY, 0.75f)));
    }


    private Table getAddress(Types.Month month, Integer year) {
        StringBuilder builder = new StringBuilder();
        builder.append("Avenida libertad No. 62")
                .append(NEWLINE)
                .append("Centro, San Cristobal.")
                .append(NEWLINE)
                .append("Tel. (809) 528 2145")
                .append(NEWLINE)
                .append("RNC: 114001623");

        String colOne = builder.toString();

        builder = new StringBuilder();
        builder.append("Reporte de Renovaciones")
                .append(NEWLINE)
                .append(Utility.capitalize(String.format("%s, %s", month.spanish(), year.toString())))
                .append(NEWLINE);

        String colTwo = builder.toString();

        return TableBuilder
                .of(40f, 60f)
                .setMarginTop(30)
                .setMarginBottom(50)
                .setFontColor(Header.ColorProvider.LIGHT_GRAY)
                .setBorder(Border.NO_BORDER)
                .setFontSize(11)
                .addCell(createCell(colOne)
                        .setTextAlignment(TextAlignment.LEFT))
                .addCell(createCell(colTwo)
                        .setTextAlignment(TextAlignment.RIGHT));
    }

    private Cell createCell(String text) {
        if (Objects.isNull(text))
            text = "- - -";
        return new Cell().setPadding(0.8f)
                .setBorder(Border.NO_BORDER)
                .add(new Paragraph(text)
                        .setMultipliedLeading(1));
    }

    private static class TableBuilder {

        public static Table of(Float... columns) {
            UnitValue[] unitValues = new UnitValue[columns.length];
            for (int i = 0; i < columns.length; i++) {
                unitValues[i] = new UnitValue(UnitValue.PERCENT, columns[i]);
            }
            return new Table(unitValues).setWidth(UnitValue.PERCENT).setWidth(760);
        }
    }

    private static class PdfDocument {
        public static ZugferdDocument of(String dest, InputStream icc) throws FileNotFoundException {
            return new ZugferdDocument(
                    new PdfWriter(dest)
                    , ZugferdConformanceLevel.ZUGFeRDBasic,
                    new PdfOutputIntent(
                            "Custom",
                            null,
                            "http://www.color.org",
                            "sRGB IEC61966-2.1",
                            icc));
        }
    }

}
