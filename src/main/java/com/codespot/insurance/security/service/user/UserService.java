package com.codespot.insurance.security.service.user;

import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.security.model.PasswordChange;
import com.codespot.insurance.security.model.entity.User;
import com.condos.shared.data.page.C2Criteria;
import freemarker.template.TemplateException;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;

public interface UserService {

    Page<User> findAll(PageDto page, C2Criteria criteria);

    User findById(Long id);

    User loggedUser();

    boolean isValidPassword(String password);

    User findByUuid(String uuid);

    User findByName(String email);

    User deleteUserById(Long id);

    Boolean isUserExist(User user);

    User saveUser(User user) throws TemplateException, IOException, MessagingException;

    User updateUser(User user, Long id);

    User updateProfilePicture(MultipartFile file, Long userId);

    User changePassword(PasswordChange passwordChange);

    User setPassword(PasswordChange passwordChange);

    void resetPassword(String email) throws TemplateException, IOException, MessagingException;

    void restorePassword(String email);

    User loggedUserOrNull();

    String password(Long id);
}
