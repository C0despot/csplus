package com.codespot.insurance.model.repository.log;


import com.codespot.insurance.model.entity.Log;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Jose Anibal Rodriguez
 */
public interface LogRepository extends JpaRepository<Log, String> {
}
