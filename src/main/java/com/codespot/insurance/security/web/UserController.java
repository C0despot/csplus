package com.codespot.insurance.security.web;

import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.security.model.PasswordChange;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.web.resource.Logger;
import com.condos.shared.data.page.C2Criteria;
import com.condos.shared.exception.BadRequestException;
import com.condos.shared.exception.NotFoundException;
import com.condos.shared.exception.UserAlreadyExistsException;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.io.IOException;

@RestController
@RequestMapping(value = "user")
public class UserController {

    private final
    UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.GET_ALL_USERS)
    public ResponseEntity listAllUsers(PageDto page, C2Criteria criteria) {
        Page<User> users = userService.findAll(page, criteria);
        return ResponseEntity.ok(users);
    }

    @GetMapping(value = "{email}")
    @Logger(transaction = Log.Transaction.GET_USER_BY_EMAIL)
    public ResponseEntity getUser(@PathVariable(name = "email") String email) {
        User user = userService.findByName(email);
        if (user == null) {
            throw new NotFoundException(String.format("Usuario [%s] no pudo ser encontrado!", email), email, LogActivity.Scope.USER.name(), LogActivity.Action.READ.name());
        }
        return ResponseEntity.ok(user);
    }

    @PostMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.CREATE_NEW_USER)
    public ResponseEntity createUser(@RequestBody User user) throws TemplateException, IOException, MessagingException {
        if (user.isNew() && userService.isUserExist(user)) {
            throw new UserAlreadyExistsException("Usuario ya fue registrado, ", user.getUsername(), LogActivity.Scope.USER.name(), LogActivity.Action.MODIFY.name());
        }
        user = userService.saveUser(user);
        return ResponseEntity.ok(user);
    }


    @PostMapping(value = "update")
    @Logger(transaction = Log.Transaction.UPDATE_USER_INFO)
    public ResponseEntity updateUser(@RequestBody(required = false) User user, Long userId) {

        if (user == null || userId == null) {
            throw new BadRequestException("Parametro requerido, no fue encontrado", "user", LogActivity.Scope.USER.name(), LogActivity.Action.MODIFY.name());
        }
        user = userService.updateUser(user, userId);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping(value = "{id}")
    @Logger(transaction = Log.Transaction.DELETE_USER_INFO)
    public ResponseEntity deleteUser(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        if (user == null) {
            throw new NotFoundException(String.format("Usuario [%s] no pudo ser encontrado!", id.toString()), id.toString(), LogActivity.Scope.USER.name(), LogActivity.Action.READ.name());
        }
        user = userService.deleteUserById(id);
        return ResponseEntity.ok(user);
    }

    @PostMapping(value = "restore")
    @Logger(transaction = Log.Transaction.RESTORE_USER_ACCOUNT_MAIL)
    public ResponseEntity restoreEmail(String email) {
        this.userService.restorePassword(email);
        return ResponseEntity.ok("{}");
    }

    @PostMapping(value = "image")
    @Logger(transaction = Log.Transaction.CHANGE_PROFILE_IMAGE)
    public ResponseEntity changeProfilePict(MultipartFile file) {
        User user = this.userService.updateProfilePicture(file, null);
        return ResponseEntity.ok(user);
    }

    @PostMapping(value = "password")
    @Logger(transaction = Log.Transaction.CHANGE_USER_PASSWORD)
    public ResponseEntity changePassword(@RequestBody PasswordChange passwordChange) {
        User user = this.userService.changePassword(passwordChange);
        return ResponseEntity.ok(user);
    }

    @PostMapping(value = "password/set")
    @Logger(transaction = Log.Transaction.SET_USER_PASSWORD)
    public ResponseEntity setPassword(@RequestBody PasswordChange passwordChange) {
        User user = this.userService.setPassword(passwordChange);
        return ResponseEntity.ok(user);
    }

    @GetMapping(value = "password/reset")
    @Logger(transaction = Log.Transaction.RESET_USER_PASSWORD)
    public ResponseEntity resetPassword(@RequestParam String email) throws TemplateException, IOException, MessagingException {
        this.userService.resetPassword(email);
        return ResponseEntity.ok().build();
    }

}