angular
    .module('inspinia')
    .controller('NewGoodController', function ($scope, $uibModalInstance, $stateParams, InsuranceService) {

        $scope.uibModalInstance = $uibModalInstance;
        $scope.GoodTitle = "Loading...";
        $scope.vehicle = {
            version: (new Date()).getMilliseconds()
        };
        $scope.coverages = [];
        $scope.isEditMode = false;
        $scope.edit = true;
        setTimeout(function () {
            InsuranceService
                .loadCoverage()
                .then(function (response) {
                    $scope.coverages = response.data;
                    $scope.GoodTitle = `Agregar nuevo vehículo`.toLowerCase()
                });
        }, 100);
    });