package com.codespot.insurance.web.resource;


import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.LogDetail;
import com.codespot.insurance.service.log.LogService;
import com.condos.shared.exception.RestResponseException;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Jose An Rodriguez
 */

@Aspect
@Component
public class LogAspect {

    private final HttpServletRequest request;
    private final LogService logService;
    private String logId;

    @Autowired
    public LogAspect(HttpServletRequest request, LogService logService) {
        this.request = request;
        this.logService = logService;
    }

    @Before("@annotation(log)")
    public void before(Logger log) {
        logId = nextId();
        String params = "{Job}";
        String method = "JOB";
        String ip = "127.0.0.1";
        if (log.isHttp()) {
            params = getJsonParameterList();
            method = request.getMethod();
            ip = request.getRemoteAddr();
        }
        logService.store(logId, log.transaction(), ip, params, method);
    }

    @AfterReturning(value = "@annotation(com.codespot.insurance.web.resource.Logger)", returning = "result")
    public void onReturn(Object result) {
        Date date = new Date();
        int code = 200;
        String className =  result == null ? "Running internal job.":result.getClass().getName();
        if (result instanceof ResponseEntity) {
            code = ((ResponseEntity) result).getStatusCode().value();
            Object body = ((ResponseEntity) result).getBody();
            if (body != null)
                className = body.getClass().getName();
        }


        LogDetail detail = LogDetail
                .builder()
                .date(date)
                .id(logId)
                .status(code)
                .result(Log.Result.SUCCESS.name())
                .message(className.replace("Com.Codespot", ""))
                .build();
        logService.detail(detail, logId);
    }

    @AfterThrowing(value = "@annotation(com.codespot.insurance.web.resource.Logger)", throwing = "e")
    public void onException(Exception e) {
        Date date = new Date();
        LogDetail detail = LogDetail
                .builder()
                .date(date)
                .id(nextId())
                .status(e instanceof RestResponseException ? ((RestResponseException) e).getStatusCode().value() : 500)
                .result(Log.Result.FAILURE.name())
                .message(e.getMessage())
                .build();
        logService.detail(detail, logId);
    }

    private String getJsonParameterList() {
        Map<String, String[]> parameterMap = request.getParameterMap();
        if (parameterMap == null)
            return null;
        Map<String, String> map = new HashMap<>();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            map.put(entry.getKey(), entry.getValue()[0]);
        }
        String password = "password";
        if (map.containsKey(password)) {
            map.replace(password, "...");
        }
        return new JSONObject(map).toString();
    }

    private String nextId() {
        return UUID.randomUUID().toString();
    }
}
