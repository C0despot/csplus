angular
    .module(APP)
    .controller('InsuranceCtrl',
        function ($scope, $stateParams, InsuranceService, $uibModal, $state, NotificationService, CustomerService) {

            $scope.insurance = {
                currency: 'PESOS',
                startDate: new Date(),
                status: 'VIGENTE',
                endDate: nextYear()
            };
            $scope.selectedItem = 0;
            $scope.branches = [];
            $scope.carriers = [];
            $scope.edit = false;
            $scope.vehicles = [];
            $scope.locals = [];
            $scope.dependents = [];
            $scope.loading = false;
            $scope.customer = $stateParams.id;
            $scope.isEditMode = false;
            $scope.greenLight = false;
            $scope.step = 0;
            $scope.message = null;
            $scope.validating = false;

            $scope.$watch('insurance.branch', function () {
                let branch = $scope.insurance.branch;
                if (!branch) return;
                if (branch.id === 100000001) {
                    $scope.message = 'LOCALES ASEGURADOS';
                    $scope.step = 1;
                } else if (branch.id === 100000000) {
                    $scope.message = 'BIENES ASEGURADOS';
                    $scope.step = 0;
                } else if (branch.id === 100000002) {
                    $scope.message = 'BIENES ASEGURADOS';
                    $scope.step = 0;
                } else if (branch.id === 100000003) {
                    $scope.message = 'LISTA DE DEPENDIENTES';
                    $scope.step = 2;
                } else {
                    $scope.message = null;
                }
            });

            $scope.$on('$viewContentLoaded', function () {
                $scope.loadDependencies();
            });

            $scope.loadDependencies = function () {
                let customer = $stateParams.id;
                let insurance = $stateParams.insurance;

                if (!$scope.isEditMode) {
                    $scope.validating = true;
                    InsuranceService
                        .nextIndex()
                        .then((response) => {
                            if (!$scope.insurance.insuranceNumber)
                                $scope.insurance.insuranceNumber = response.data.result;
                        })
                        .finally(() => {
                            $scope.validating = false;
                        });
                }

                InsuranceService
                    .getCarriers()
                    .then(function (response) {
                        $scope.carriers = response.data;
                        InsuranceService
                            .getBranches()
                            .then(function (response) {
                                $scope.branches = response.data;
                                if (insurance) {
                                    $scope.isEditMode = true;
                                    InsuranceService
                                        .getOne(customer, insurance)
                                        .then(function (response) {
                                            let insurance = angular.copy(response.data);
                                            $scope.vehicles = insurance.vehicles;
                                            $scope.locals = insurance.locals;
                                            $scope.dependents = insurance.dependents;
                                            $scope.insurance = insurance;
                                            $scope.greenLight = true;
                                        });
                                } else {
                                    let id = $stateParams.id;
                                    $scope.edit = true;
                                    CustomerService
                                        .getExtendedCustomerInfo(id)
                                        .then(function (response) {
                                            $scope.insurance.customer = angular.copy(response.data);
                                            $scope.greenLight = true;
                                        }, function (error) {
                                        });
                                }
                            });
                    });
            };

            $scope.save = function () {
                let insurance = angular.copy($scope.insurance);
                insurance.startDate = new Date(insurance.startDate);
                insurance.endDate = new Date(insurance.endDate);
                insurance.prime = numberFormatter(insurance.prime);
                insurance.vehicles = angular.copy($scope.vehicles);
                insurance.locals = angular.copy($scope.locals);
                insurance.dependents = angular.copy($scope.dependents);
                switch (insurance.branch.description) {
                    case 'INCENDIO': {
                        if (insurance.locals.length < 1) {
                            NotificationService.errorMsj('Debe insertar al menos un local');
                            return;
                        }
                        break;
                    }
                    case 'AUTOMOVIL': {
                        if (insurance.vehicles.length < 1) {
                            NotificationService.errorMsj('Debe insertar al menos un automovil');
                            return;
                        }
                        break;
                    }
                    case 'SALUD': {
                        if (insurance.dependents.length < 1) {
                            NotificationService.errorMsj('Debe insertar al menos un dependiente');
                            return;
                        }
                        break;
                    }
                }
                $scope.loading = true;
                InsuranceService
                    .save(insurance)
                    .then(function (response) {
                        NotificationService.successMsj();
                        $state.go("customer.detail.step_three.add", {insurance: response.data.id});
                    }, function () {
                        NotificationService.errorMsj();
                    })
                    .finally(function () {
                        $scope.loading = false;
                    });
            };

            $scope.validateMe = function () {
                // if (!$scope.isEditMode) {
                $scope.validating = true;
                InsuranceService
                    .validateNumber($scope.insurance.insuranceNumber, 'INSURANCE')
                    .then(function () {
                        $("#validate").removeClass("has-error");
                    }, function () {
                        $("#validate").addClass("has-error");
                    })
                    .finally(() => {
                        $scope.validating = false;
                    });
                // }
            };

            $scope.showModal = function () {
                let step = $scope.step;
                console.log(step);
                if (step === 0)
                    $scope.showGoods();
                else if (step === 1)
                    $scope.showLocals();
                else if (step === 2)
                    $scope.showDependents();
            };

            $scope.showDependents = function () {
                let dependents = $scope.dependents;
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/dependent_modal.html',
                    controller: 'DependentController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn",
                    resolve: {
                        dependents: function () {
                            return dependents
                        }
                    }
                });

                modalInstance.result.then(function (resp) {
                    if (resp.dependent) {
                        $scope.editDependent(resp.dependent);
                    } else if (resp.isNew) {
                        $scope.addDependent();
                    }
                })
            };


            $scope.editDependent = function (dependent) {
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/create/dependent_modal.html',
                    controller: 'EditDependentCtrl',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn",
                    resolve: {
                        dependent: function () {
                            return dependent
                        }
                    }
                });

                modalInstance.result.then(function (dependent) {
                    if (dependent) {
                        dependent.startDate = new Date(dependent.startDate);
                        dependent.birthDate = new Date(dependent.birthDate);
                        dependent.value = numberFormatter(dependent.value);
                        let jsonIndex = getJsonIndex($scope.dependents, dependent);
                        if (jsonIndex) {
                            $scope.dependents [jsonIndex.index] = dependent;
                        } else
                            $scope.dependents.push(dependent);
                    }
                    $scope.showDependents();
                })
            };

            $scope.addDependent = function () {

                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/create/dependent_modal.html',
                    controller: 'NewDependentCtrl',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn"
                });

                modalInstance.result.then(function (dependent) {
                    if (dependent) {
                        dependent.startDate = new Date(dependent.startDate);
                        dependent.birthDate = new Date(dependent.birthDate);
                        dependent.value = numberFormatter(dependent.value);
                        let jsonIndex = getJsonIndex($scope.dependents, dependent);
                        if (jsonIndex) {
                            $scope.dependents [jsonIndex.index] = dependent;
                        } else
                            $scope.dependents.push(dependent);
                    }
                    $scope.showDependents();
                })
            };

            $scope.showLocals = function () {
                let locals = $scope.locals;
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/local_modal.html',
                    controller: 'LocalController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn",
                    resolve: {
                        locals: function () {
                            return locals
                        }
                    }
                });

                modalInstance.result.then(function (resp) {
                    if (resp.local) {
                        $scope.editLocal(resp.local);
                    } else if (resp.isNew) {
                        $scope.addLocal();
                    }
                })
            };

            $scope.editLocal = function (local) {
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/create/local_modal.html',
                    controller: 'EditLocalCtrl',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn",
                    resolve: {
                        local: function () {
                            return local
                        }
                    }
                });

                modalInstance.result.then(function (local) {
                    if (local) {
                        let jsonIndex = getJsonIndex($scope.locals, local);
                        if (jsonIndex) {
                            $scope.locals [jsonIndex.index] = local;
                        } else
                            $scope.locals.push(local);
                    }
                    $scope.showLocals();
                })
            };

            $scope.addLocal = function () {
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/create/local_modal.html',
                    controller: 'NewLocalCtrl',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn"
                });

                modalInstance.result.then(function (local) {
                    if (local) {
                        let jsonIndex = getJsonIndex($scope.locals, local);
                        if (jsonIndex) {
                            $scope.locals [jsonIndex.index] = local;
                        } else
                            $scope.locals.push(local);
                    }
                    $scope.showLocals();
                })
            };

            $scope.showGoods = function () {
                let vehicles = $scope.vehicles;
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/goods_modal.html',
                    controller: 'GoodsController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn",
                    resolve: {
                        vehicles: function () {
                            return vehicles
                        }
                    }
                });

                modalInstance.result.then(function (resp) {
                    if (resp.vehicle) {
                        $scope.editGood(resp.vehicle);
                    } else if (resp.isNew) {
                        $scope.addGood();
                    }
                })
            };

            $scope.editGood = function (vehicle) {
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/create/good_modal.html',
                    controller: 'EditGoodController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn",
                    resolve: {
                        vehicle: function () {
                            return vehicle
                        }
                    }
                });

                modalInstance.result.then(function (vehicle) {
                    if (vehicle) {
                        vehicle.prime = numberFormatter(vehicle.prime);
                        vehicle.coverageAmount = numberFormatter(vehicle.coverageAmount);
                        vehicle.deducible = numberFormatter(vehicle.deducible);
                        let jsonIndex = getJsonIndex($scope.vehicles, vehicle);
                        if (jsonIndex) {
                            $scope.vehicles [jsonIndex.index] = vehicle;
                        } else
                            $scope.vehicles.push(vehicle);
                    }
                    $scope.showGoods();
                })
            };

            $scope.addGood = () => {
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/insurance/modal/create/good_modal.html',
                    controller: 'NewGoodController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'lg',
                    windowClass: "animated fadeIn"
                });

                modalInstance.result.then(function (vehicle) {
                    if (vehicle) {
                        vehicle.prime = numberFormatter(vehicle.prime);
                        vehicle.coverageAmount = numberFormatter(vehicle.coverageAmount);
                        vehicle.deducible = numberFormatter(vehicle.deducible);
                        let jsonIndex = getJsonIndex($scope.vehicles, vehicle);
                        if (jsonIndex) {
                            $scope.vehicles [jsonIndex.index] = vehicle;
                        } else
                            $scope.vehicles.push(vehicle);
                    }
                    $scope.showGoods();
                })
            };

            $scope.showAuditedItem = () => {
                let insurance = angular.copy($scope.insurance);
                $uibModal.open({
                    templateUrl: 'app/report/audit_modal.html',
                    controller: 'AuditController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'md',
                    windowClass: "animated fadeIn",
                    resolve: {
                        document: function () {
                            return {
                                id: insurance.insuranceNumber,
                                type: 'POLIZA - ' + insurance.branch.description,
                                createdBy: insurance.createdBy,
                                createdDate: dateFormatter(insurance.createdDate),
                                modifiedDate: dateFormatter(insurance.modifiedDate),
                                modifiedBy: insurance.modifiedBy
                            }
                        }
                    }
                });
            };

            $scope.deleteItem = () => {
                const insurance = angular.copy($scope.insurance);
                NotificationService
                    .confirmPassword(
                        (password) => {
                            let deleteObj = {
                                documentNo: insurance.id,
                                type: 'INSURANCE',
                                password: password
                            };
                            InsuranceService
                                .deleteObj(deleteObj)
                                .then(() => {
                                    $state.go("customer.detail.step_two.list", {id: insurance.customer.clientId});
                                });
                        })
            }
        });