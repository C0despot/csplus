package com.codespot.insurance.service.log;


import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.LogDetail;
import com.codespot.insurance.model.repository.log.LogDetailRepository;
import com.codespot.insurance.model.repository.log.LogRepository;
import com.codespot.insurance.model.spec.LogDetailSpecification;
import com.codespot.insurance.security.model.entity.User;
import com.codespot.insurance.security.service.user.UserService;
import com.condos.shared.data.page.C2Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * @author Jose An Rodriguez
 */

@Service(value = "defaultLogService")
public class DefaultLogService implements LogService {

    private final
    LogRepository logRepository;

    private final
    LogDetailRepository detailRepository;

    private final
    UserService userService;

    @Autowired
    public DefaultLogService(LogRepository logRepository, UserService userService, LogDetailRepository repository) {
        this.logRepository = logRepository;
        this.userService = userService;
        this.detailRepository = repository;
    }

    @Override
    public Log one(String id) {
        return this.logRepository.findById(id).orElse(null);
    }

    @Override
    public void store(String id, Log.Transaction transaction, String ip, String params, String method) {

        User user = userService.loggedUserOrNull();
        Log log = Log.builder()
                .date(new Date())
                .id(id)
                .params(params)
                .ipAddress(ip)
                .type(transaction)
                .user(user)
                .method(method)
                .build();
        try {
            this.logRepository.save(log);
        } catch (Exception ignored) {
            try {
                Thread.sleep(1000);
                log.setId(UUID.randomUUID().toString());
                this.logRepository.save(log);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void detail(LogDetail detail, String log) {

        Log one = one(log);
        if (one != null && one.getUser() != null) {
            User user = one.getUser();
            String psw = userService.password(user.getId());
            user.setPassword(psw);
            one.setUser(user);
        }
        detail.setLog(one);
        try {
            this.detailRepository.save(detail);
        } catch (Exception ignored) {
            try {
                Thread.sleep(1000);
                detail.setId(UUID.randomUUID().toString());
                this.detailRepository.save(detail);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Page<LogDetail> transactionLogs(PageDto pager, C2Criteria criteria) {
        LogDetailSpecification specification = LogDetailSpecification.of(criteria);
        Pageable pageable = PageRequest.of(pager.getPage(), pager.getSize(), new Sort(Sort.Direction.DESC, "date"));
        return this.detailRepository.findAll(specification, pageable);
    }

}
