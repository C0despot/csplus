package com.codespot.insurance.model.entity.payment;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.security.model.entity.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "CREDIT")
public class Credit {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CREDIT_TYPE")
    private String type;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "POLICY")
    private Insurance insurance;

    @ManyToOne
    @JoinColumn(name = "INVOICE_NUMBER")
    private Invoice invoice;

    @Column(name = "BEGINNING_DATE")
    private Date beginDate;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "END_DATE")
    private Date endDate;

    @Column(name = "DEAD_DUE_DATE")
    private Date dueDate;

    @Column(name = "SOME_NOTES")
    private String notes;

    @Column(name = "DOCUMENT_NUMBER")
    private String docNo;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "CONCEPT")
    private String concept;

    @Column(name = "PRIME")
    private Double prime;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @Transient
    private boolean selected = false;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Override
    public String toString() {
        return this.id.toString();
    }

    @PrePersist
    void onPrePersist() {
        setCreatedBy(getModifiedBy());
        setCreationDate(new Date());
        setModificationDate(new Date());
    }

    @PreUpdate
    void onPreUpdate() {
        setModificationDate(new Date());
    }


    public boolean isNew() {
        return id == null;
    }
}
