package com.codespot.insurance.service.file;

import com.condos.shared.aws.S3;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface FileService {

    String s3Upload(File file, S3.Extension extension);
    String s3Upload(MultipartFile file);
    String fullS3Url();
    File pdfDownload(String filename) throws IOException;
}
