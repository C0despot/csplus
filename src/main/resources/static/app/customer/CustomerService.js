(function () {
    'use strict';
    angular.module('inspinia')
        .service('CustomerService', CustomerService);
})();

CustomerService.$inject = ['$http'];

function CustomerService($http) {

    return {
        getExtendedCustomerInfo: (id) => {
            let qs = `?access_token=${getAccessToken()}&customer=${id}`;
            return $http.get("customer" + qs);
        },
        saveBasicInfo: (customer) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post("customer" + qs, customer);
        },
        getPlans: (customer) => {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}`;
            return $http.get("customer/plans" + qs);
        },
        filter: (value) => {
            let qs = `?access_token=${getAccessToken()}&q=${value}`;
            return $http.get("customer/filter" + qs);
        },
        docs: (value) => {
            let qs = `?access_token=${getAccessToken()}&q=${value}`;
            return $http.get("customer/docs" + qs);
        },
        buildRedirectUri: (obj) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post("customer/redirect" + qs, obj);
        },
        getCredits: (customer) => {
            let qs = `?access_token=${getAccessToken()}&q=${customer}`;
            return $http.get("credit/all" + qs);
        }
    }
}