package com.codespot.insurance.model.repository.customer;


import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.CustomerCurrentReport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerCurrentReportRepository extends JpaRepository<CustomerCurrentReport, Long> {
    List<CustomerCurrentReport> findAllByCustomerEquals(Customer customer);
    List<CustomerCurrentReport> findAllByIdEquals(Long id);
}
