angular
    .module(APP)
    .controller('RenewCtrl', function ($scope, $uibModal, $state, InsuranceService, NotificationService, DataService, $window) {

        $scope.window = $window;
        $scope.insurances = [];
        $scope.user = JSON.parse(localStorage.getItem("user"));
        $scope.carriers = [];
        $scope.months = [];
        $scope.year = (new Date()).getFullYear();
        $scope.month = null;
        $scope.dateFilter = null;
        $scope.name = '';
        $scope.date = null;
        $scope.insuranceNo = '';
        $scope.insuranceIndividualId = '';
        $scope.printing = false;

        $scope.search = {
            insuranceNo: '',
            name: '',
            insuranceIndividualId: ''
        };

        $scope.pager = {
            page: 0,
            size: 10,
            sort: ASC,
            orderBy: 'id',
            totalPages: 0
        };

        $scope.filter = {
            operation: ':',
            value: '',
            key: 'insuranceNo'
        };

        $scope.updateFilter = (obj) => {
            if (obj)
                $scope.dateFilter = angular.copy(obj);
            $scope.loadResult();
        };
        $scope.$on('$viewContentLoaded', function () {
            DataService
                .currentMonth()
                .then((resp) => {
                    $scope.month = resp.data;
                });
            DataService
                .getAvailableMonths()
                .then((resp) => {
                    $scope.months = resp.data;
                    $scope.months.forEach(object => {
                        if (object.month === 'ALL' && object.year === 0) {
                            $scope.date = object;
                            $scope.dateFilter = object;
                        }
                    });
                    $scope.loadResult();
                });

            InsuranceService
                .getCarriers()
                .then((response) => {
                    $scope.carriers = response.data;
                });

        });


        $scope.$watch('pager.page', function () {
            $scope.pageHelper = $scope.pager.page + 1;
            $scope.loadResult();
        });

        $scope.setPage = function () {
            $scope.pager.page = $scope.pageHelper - 1;
        };

        $scope.applyFilter = function (event) {
            let field = event.target.id;
            let value = event.target.value;
            $scope.doFilter(field, value);
        };
        $scope.getPct = function (numerator, denominator) {
            if (denominator === 0 || !denominator)
                return '-';
            else if (numerator === 0 || !numerator)
                return '0%';
            else return (numerator * 100 / denominator).toFixed(2) + "%";
        };

        $scope.clean = () => {
            $state.reload();
        };

        $scope.doFilter = function (field, value) {
            $scope.filter.key = field;
            $scope.filter.value = value;
            $scope.pager.orderBy = field;
            $scope.pager.page = 0;
            $scope.search = {
                insuranceNo: '',
                name: '',
                insuranceIndividualId: ''
            };
            $scope.search [field] = value;
            $scope.loadResult();
        };

        $scope.loadResult = function () {
            let pager = $scope.pager;
            let filter = $scope.filter;

            if ($scope.dateFilter)
                InsuranceService.loadRenew(pager, filter, $scope.dateFilter)
                    .then((response) => {
                        $scope.insurances = angular.copy(response.data.content);
                        $scope.pager.totalPages = response.data.totalPages;
                    }, () => {
                        NotificationService.errorMsj();
                    })
        };

        $scope.printAllForCurrentSearch = () => {
            let pager = $scope.pager;
            let filter = $scope.filter;
            $scope.printing = true;

            InsuranceService.printRenew(pager, filter, $scope.dateFilter)
                .then((response) => {
                    $scope.window.open("/report/pdf/" + response.data.url + '?access_token=' + getAccessToken());
                }, () => {
                    NotificationService.errorMsj();
                })
                .finally(() => $scope.printing = false);
        };
        $scope.openModal = (insurance) => {
            let modalInstance = $uibModal.open({
                templateUrl: 'app/renew/modal/renew.html',
                controller: 'RenewProcessCtrl',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                windowClass: "animated fadeIn",
                resolve: {
                    insurance: function () {
                        return insurance
                    }
                }
            });

            modalInstance.result.then((resp) => {
                if (resp)
                    $state.reload();
            })
        };
    });