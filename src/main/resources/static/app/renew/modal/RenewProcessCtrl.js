angular
    .module(APP)
    .controller('RenewProcessCtrl', function ($scope, $uibModalInstance, $stateParams, InsuranceService, insurance, NotificationService) {

        $scope.loading = false;

        $scope.insurance = {
            insuranceNumber: 'Loading...'
        };

        $scope.renew = {
            invoiceNo: null,
            prime: 0,
            startDate: new Date(),
            endDate: new Date(),
            insurance: null
        };

        $scope.$uibModalInstance = $uibModalInstance;

        $scope.setDefaultValues = () => {
            let value = new Date($scope.insurance.startDate);
            value.setFullYear(value.getFullYear() + 1);
            $scope.renew.startDate = angular.copy(value);

            value = new Date($scope.insurance.endDate);
            value.setFullYear(value.getFullYear() + 1);
            $scope.renew.endDate = angular.copy(value);

            $scope.renew.prime = angular.copy($scope.insurance.prime);
            $scope.renew.insurance = angular.copy($scope.insurance);
        };


        $scope.doSave = function () {
            let renew = angular.copy($scope.renew);
            renew.startDate = new Date(renew.startDate);
            renew.endDate = new Date(renew.endDate);
            renew.prime = numberFormatter(renew.prime);
            renew.insurance = angular.copy($scope.insurance);
            $scope.loading = true;
            InsuranceService
                .execRenew(renew)
                .then((resp) => {
                    NotificationService.successMsj();
                    $uibModalInstance.close(resp.data);
                }, () => {
                    NotificationService.errorMsj();
                })
                .finally(() => {
                    $scope.loading = false;
                });
        };

        $scope.validateMe = () => {
            InsuranceService
                .validateNumber($scope.renew.invoiceNo, 'INVOICE')
                .then(() => {
                    $("#invoiceNo").removeClass("has-error");
                }, () => {
                    $("#invoiceNo").addClass("has-error");
                })
        };

        (() => {
            InsuranceService
                .getOne(0, insurance)
                .then((response) => {
                    $scope.insurance = response.data;
                    $scope.setDefaultValues();
                });
            InsuranceService
                .nextIndex('INVOICE')
                .then(function (response) {
                    $scope.renew.invoiceNo = response.data.result;
                }, function () {
                })
        })();

    });