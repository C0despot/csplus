(function () {
    'use strict';
    angular.module(APP)
        .service('UserService', UserService);
})();

UserService.$inject = ['$http'];

function UserService($http) {

    let userUrl = 'user';
    let authUrl = 'oauth';
    return {
        validateAuthorities: () => {
            let qs = `?access_token=${getAccessToken()}&uuid=${getUUID()}`;
            return $http.get(authUrl + "/authorities" + qs);
        },
        loadUsers: (pager, filter) => {
            let pagerQs = `&size=${pager.size}&page=${pager.page}&sort=${pager.sort}&orderBy=${pager.orderBy}`;
            let filterQs = `&operation=${filter.operation}&value=${filter.value}&key=${filter.key}`;
            let qs = `?access_token=${getAccessToken()}` + pagerQs + filterQs;
            return $http.get(userUrl + qs);
        },
        save: (user) => {
            let qs = `?access_token=${getAccessToken()}&uuid=${getUUID()}`;
            return $http.post(userUrl + qs, user);
        },
        getIncomes: (id) => {
            let qs = `?access_token=${getAccessToken()}&userId=${id}`;
            return $http.get("stats/incomes" + qs, id);
        },
        doChangePassword: (obj) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post(userUrl + "/password" + qs, obj);
        },
        changeUserImage: (image) => {
            let fd = new FormData();
            fd.append('file', image);
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post(userUrl + '/image' + qs, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
        },
        doSetPassword: (obj) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post(userUrl + "/password/set" + qs, obj);
        },
        sendResetPasswordEmail: (email) => {
            return $http.get(userUrl + "/password/reset?email=" + email);
        },
        logs: (pager, filter) => {
            let pagerQs = `&size=${pager.size}&page=${pager.page}&sort=${pager.sort}&orderBy=${pager.orderBy}`;
            let filterQs = `&operation=${filter.operation}&value=${filter.value}&key=${filter.key}`;
            let qs = `?access_token=${getAccessToken()}` + pagerQs + filterQs;
            return $http.get('util/logs' + qs);
        },
        transactions: (pager, filter) => {
            let pagerQs = `&size=${pager.size}&page=${pager.page}&sort=${pager.sort}&orderBy=${pager.orderBy}`;
            let filterQs = `&operation=${filter.operation}&value=${filter.value}&key=${filter.key}`;
            let qs = `?access_token=${getAccessToken()}` + pagerQs + filterQs;
            return $http.get('util/transactions' + qs);
        },
    };
}