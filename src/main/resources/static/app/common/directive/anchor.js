angular.module('inspinia')
    .directive('anchorDisable', function () {
        return {

            link: function (scope, elem, attr) {
                let currentValue = !!scope.$eval(attr.myDisabled),
                    current = elem[0],
                    next = elem[0].cloneNode(true);

                let nextElem = angular.element(next);

                nextElem.on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                });

                nextElem.css('background', 'rgba(117, 119, 121, 0.41)');
                nextElem.css('color', 'white');
                nextElem.css('cursor', 'not-allowed');
                nextElem.attr('tabindex', -1);

                scope.$watch(attr.anchorDisable, function (value) {
                    value = !!value;
                    if (currentValue !== value) {
                        currentValue = value;
                        current.parentNode.replaceChild(next, current);
                        let temp = current;
                        current = next;
                        next = temp;
                    }
                })
            }
        }
    });