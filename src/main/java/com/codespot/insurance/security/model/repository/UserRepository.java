package com.codespot.insurance.security.model.repository;

import com.codespot.insurance.security.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    @Query(value = "SELECT * FROM USER WHERE (USERNAME = :username OR EMAIL_ADDRESS = :username) AND STATUS = 'ACTIVE' LIMIT 1", nativeQuery = true)
    User findUser(@Param("username") String username);

    @Query(value = "SELECT * FROM USER WHERE (USERNAME = :username OR EMAIL_ADDRESS = :email) AND STATUS = 'ACTIVE' LIMIT 1", nativeQuery = true)
    User findUser(@Param("username") String username,
                  @Param("email") String email);

    Optional<User> findByUuidEquals(String uuid);

    @Query(value = "SELECT U.PASSWORD FROM USER U WHERE U.USER_ID = :id", nativeQuery = true)
    String password(@Param("id") Long id);
}
