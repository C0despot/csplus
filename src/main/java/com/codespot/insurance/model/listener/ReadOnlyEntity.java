package com.codespot.insurance.model.listener;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

public class ReadOnlyEntity {

    @PrePersist
    void onPrePersist(Object o) {
        throw new RuntimeException("Read Only Entity Cannot Be Created.");
    }

    @PreUpdate
    void onPreUpdate(Object o) {
        throw new RuntimeException("Read Only Entity Cannot Be Updated.");
    }

    @PreRemove
    void onPreRemove(Object o) {
        throw new RuntimeException("Read Only Entity Cannot Be Removed.");
    }

}
