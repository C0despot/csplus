(function () {
    'use strict';
    angular.module(APP)
        .service('InsuranceService', InsuranceService);
})();

InsuranceService.$inject = ['$http'];

function InsuranceService($http) {

    return {
        getOne: function (customer, insurance) {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}&insurance=${insurance}`;
            return $http.get('insurance/one' + qs);
        },
        getCarriers: function () {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.get('insurance/carrier' + qs);
        },
        loadCarriers: (pager) => {
            let pagerQs = `&size=${pager.size}&page=${pager.page}&sort=${pager.sort}&orderBy=${pager.orderBy}`;
            let qs = `?access_token=${getAccessToken()}` + pagerQs;
            return $http.get('insurance/carrier/all' + qs);
        },
        saveCarrier: (carrier) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post('insurance/carrier' + qs, carrier);
        },
        getBranches: function () {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.get('insurance/branch' + qs);
        },
        loadCoverage: function () {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.get('insurance/coverage' + qs);
        },
        saveBranch: function (desc) {
            let qs = `?access_token=${getAccessToken()}&description=${desc}`;
            return $http.post('insurance/branch' + qs);
        },
        getLocals: function (insurance) {
            let qs = `?access_token=${getAccessToken()}&insurance=${insurance}`;
            return $http.get('insurance/local' + qs);
        },
        save: function (insurance) {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post('insurance' + qs, insurance);
        },
        execRenew: function (renewObj) {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post('invoice/renew' + qs, renewObj);
        },
        getByCustomer: function (customer) {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}`;
            return $http.get('insurance/all/customer' + qs);
        },
        validateNumber(num, tns = 'INSURANCE') {
            let qs = `?access_token=${getAccessToken()}&num=${num}&tns=${tns}`;
            return $http.get('util/validate' + qs);
        },
        nextIndex(tns = 'INSURANCE') {
            let qs = `?access_token=${getAccessToken()}&tns=${tns}`;
            return $http.get('util/sequence' + qs);
        },
        loadRenew(pager, filter, date) {
            let pagerQs = `&size=${pager.size}&page=${pager.page}&sort=${pager.sort}&orderBy=${pager.orderBy}`;
            let filterQs = `&operation=${filter.operation}&value=${filter.value}&key=${filter.key}`;
            let dateQS = `&month=${date.month}&year=${date.year}`;
            let qs = `?access_token=${getAccessToken()}` + pagerQs + filterQs + dateQS;
            return $http.get("insurance/renew" + qs);
        },
        printRenew(pager, filter, date) {
            let pagerQs = `&size=${pager.size}&page=${pager.page}&sort=${pager.sort}&orderBy=${pager.orderBy}`;
            let filterQs = `&operation=${filter.operation}&value=${filter.value}&key=${filter.key}`;
            let dateQS = `&month=${date.month}&year=${date.year}`;
            let qs = `?access_token=${getAccessToken()}` + pagerQs + filterQs + dateQS;
            return $http.post("insurance/renew/print" + qs);
        },
        appVersion() {
            return $http.get("util/version");
        },
        deleteObj(doc) {
            let qs = `?access_token=${getAccessToken()}`;
            return $http({
                url: 'insurance/document' + qs,
                method: 'DELETE',
                data: doc,
                headers: {
                    "Content-Type": "application/json;charset=utf-8"
                }
            });
        },
        Claim() {
            return {
                getOne(id) {
                    let url = `insurance/claim?access_token=${getAccessToken()}&id=${id}`;
                    return $http.get(url);
                },
                getAll(id) {
                    let url = `insurance/claim/all?access_token=${getAccessToken()}&customer=${id}`;
                    return $http.get(url);
                },
                save(claim) {
                    let url = `insurance/claim?access_token=${getAccessToken()}`;
                    return $http.post(url, claim);
                }

            }
        },
        deleteElement(id, type = 'LOCAL') {
            let qs = `?access_token=${getAccessToken()}&type=${type}&element=${id}`;
            return $http({
                url: 'insurance/element' + qs,
                method: 'DELETE',
                headers: {
                    "Content-Type": "application/json;charset=utf-8"
                }
            });
        },

    }
}