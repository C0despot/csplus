package com.codespot.insurance.model.dto;

import lombok.Data;
import org.springframework.data.domain.Sort;

@Data
public class PageDto {
    private int page;
    private int size = 10;
    private Sort.Direction sort = Sort.Direction.DESC;
    private String orderBy = "id";
}
