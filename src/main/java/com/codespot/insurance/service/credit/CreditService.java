package com.codespot.insurance.service.credit;

import com.codespot.insurance.model.entity.payment.Credit;

import java.util.List;

public interface CreditService {

    List<Credit> findAllByCustomer(Long customer);

    Credit getOne(Long id);

    Credit save(Credit credit);
}
