angular
    .module(APP)
    .controller('CompanyCtrl', function ($scope, NotificationService, $state, UserService, $uibModal, InsuranceService) {

        $scope.carriers = [];

        $scope.pager = {
            page: 0,
            size: 10,
            sort: DESC,
            orderBy: 'name',
            totalPages: 0
        };

        $scope.filter = {
            operation: ':',
            value: '',
            key: 'name'
        };

        $scope.$on('$viewContentLoaded', () => {
            UserService
                .validateAuthorities()
                .then((ignored) => {
                    },
                    () => {
                        $state.go("error.unauthorized");
                    });
            InsuranceService
                .loadCarriers($scope.pager, $scope.filter)
                .then((response) => {
                    $scope.carriers = angular.copy(response.data.content);
                    $scope.pager.totalPages = response.data.totalPages;
                }, () => {
                    NotificationService.errorMsj();
                })
        });


        $scope.addNewModal = () => {
            let modalInstance = $uibModal.open({
                templateUrl: 'app/company/modal/company.html',
                controller: 'CreateCompanyModalCtrl',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                windowClass: "animated fadeIn middle-modal",
            });

            modalInstance.result.then((resp) => {
                if (resp)
                    $state.reload();
            })
        };

        $scope.editUserModal = (carrier) => {
            let modalInstance = $uibModal.open({
                templateUrl: 'app/company/modal/company.html',
                controller: 'EditCompanyModalCtrl',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                windowClass: "animated fadeIn middle-modal",
                resolve: {
                    Carrier: function () {
                        return carrier;
                    }
                }
            });

            modalInstance.result.then((resp) => {
                if (resp)
                    $state.reload();
            })
        };

        $scope.$watch('pager.page', () => {
            $scope.pageHelper = $scope.pager.page + 1;
            $scope.loadResult();
        });

        $scope.setPage = () => {
            $scope.pager.page = $scope.pageHelper - 1;
        };

        $scope.applyFilter = function (event) {
            let field = event.target.id;
            let value = event.target.value;
            $scope.doFilter(field, value);
        };
        $scope.getPct = function (numerator, denominator) {
            if (denominator === 0 || !denominator)
                return '-';
            else if (numerator === 0 || !numerator)
                return '0%';
            else return (numerator * 100 / denominator).toFixed(2) + "%";
        };

        $scope.doFilter = function (field, value) {
            $scope.filter.key = field;
            $scope.filter.value = value;
            $scope.pager.orderBy = field;
            $scope.pager.page = 0;
            $scope.loadResult();
        };

        $scope.loadResult = () => {
            let pager = $scope.pager;
            let filter = $scope.filter;

            InsuranceService
                .loadCarriers(pager, filter)
                .then((response) => {
                    $scope.carriers = angular.copy(response.data.content);
                    $scope.pager.totalPages = response.data.totalPages;
                }, () => {
                    NotificationService.errorMsj();
                })
        };

    });