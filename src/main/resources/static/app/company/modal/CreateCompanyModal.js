angular
    .module(APP)
    .controller('CreateCompanyModalCtrl', function ($scope, $uibModalInstance, InsuranceService, NotificationService) {

        $scope.userTitle = 'Agregar Compañía';
        $scope.isEditMode = false;
        $scope.loading = false;
        $scope.edit = true;
        $scope.uibModalInstance = $uibModalInstance;
        $scope.log = {
        };

        $scope.saveCarrier = () => {
            let carrier = angular.copy($scope.carrier);
            $scope.loading = true;
            InsuranceService
                .saveCarrier(carrier)
                .then(ignored => {
                    NotificationService.successMsj();
                    $uibModalInstance.close(true);
                }, () => {
                    NotificationService.errorMsj();
                })
                .finally(() => $scope.loading = false)
        };

    });