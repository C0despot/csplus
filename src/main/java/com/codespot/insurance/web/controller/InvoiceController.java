package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.dto.RenewDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.service.invoice.InvoiceService;
import com.codespot.insurance.web.resource.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "invoice")
public class InvoiceController {

    private final
    InvoiceService invoiceService;

    @Autowired
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping(value = "customer/all")
    @Logger(transaction = Log.Transaction.GET_ALL_INVOICE_BY_CUSTOMER)
    public ResponseEntity getInvoiceByCustomer(@RequestParam(name = "customer") Long customer,
                                               @RequestParam(name = "insurance", required = false) Long insurance) {
        List<Invoice> invoices = this.invoiceService.getAllCustomerInvoice(customer, insurance);
        return ResponseEntity.ok(invoices);
    }

    @GetMapping(value = "one")
    @Logger(transaction = Log.Transaction.GET_INVOICE_INFO)
    public ResponseEntity getOne(@RequestParam(name = "invoice") Long invoiceId) {
        Invoice invoice = this.invoiceService.getOne(invoiceId);
        return ResponseEntity.ok(invoice);
    }

    @PostMapping(value = {"", "/"})
    @Logger(transaction = Log.Transaction.SAVE_INVOICE_INFO)
    public ResponseEntity save(@RequestBody Invoice invoice) {
        invoice = this.invoiceService.save(invoice);
        return ResponseEntity.ok(invoice);
    }

    @PostMapping(value = "renew")
    @Logger(transaction = Log.Transaction.INVOICE_RENEW_PROCESS)
    public ResponseEntity renew(@RequestBody RenewDto renewDto) {
        Invoice invoice = this.invoiceService.renew(renewDto);
        return ResponseEntity.ok(invoice);
    }
}
