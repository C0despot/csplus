package com.codespot.insurance.model.repository;

import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.model.entity.payment.Credit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CreditRepository extends JpaRepository<Credit, Long> {

    List<Credit> findAllByCustomerClientId(Long id);

    Credit findByDocNoEquals(String docNo);

    List<Credit> findAllByInvoiceEquals(Invoice invoice);
}
