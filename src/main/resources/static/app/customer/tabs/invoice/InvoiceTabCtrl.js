angular
    .module('inspinia')
    .controller('InvoiceTabCtrl', function ($scope, $state, $stateParams, InvoiceService) {

        $scope.insurance = 0;
        $scope.state = $state;
        $scope.invoices = [];
        $scope.selectedItem = 0;

        $scope.$on('$viewContentLoaded', function () {
            $scope.loadByCustomer();
        });

        $scope.loadByCustomer = function () {
            let id = $stateParams.id;
            let insurance = $scope.insurance;

            InvoiceService
                .allByCustomer(id, insurance)
                .then(function (response) {
                    $scope.invoices = response.data;
                }, function (error) {

                });
        };


        $scope.selectRow = function (index) {
            let value = $scope.invoices[index].selected;
            angular.forEach($scope.invoices, function (invoice) {
                invoice.selected = false;
            });
            $scope.invoices[index].selected = value;

            $scope.selectedItem = value ? $scope.invoices[index].id : 0;
        }

    });