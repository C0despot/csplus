package com.codespot.insurance.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Jose Anibal Rodriguez
 */

@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LOG_DETAIL")
public class LogDetail {

    @Id
    @Column(name = "ID")
    private String id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "LOG_ID")
    private Log log;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "RESULT")
    private String result;

    @Column(name = "DATE")
    private Date date;

    @Transient
    public String getActionClass() {
        if (log != null) {
            switch (log.getMethod()) {
                case "POST":
                    return "primary";
                case "DELETE":
                    return "danger";
                case "PUT":
                    return "success";
                default:
                    return "default";
            }
        }
        return "default";
    }

    @Transient
    public String getResultClass() {
        if (result != null) {
            if ("SUCCESS".equals(result)) {
                return "success";
            }
            return "danger";
        }
        return "danger";
    }

}
