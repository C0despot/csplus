package com.codespot.insurance.model.entity.payment;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.insurance.InsuranceCarrier;
import com.codespot.insurance.security.model.entity.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "CLAIMS")
public class Claim {

    @Id
    @Column(name = "CLAIM_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DOCUMENT_NUMBER")
    private String docNo;

    @Column(name = "ADJUSTMENT")
    private String adjustment;

    @Column(name = "MECHANICAL")
    private String mechanical;

    @ManyToOne
    @JoinColumn(name = "CLIENT")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "POLICY")
    private Insurance insurance;

    @ManyToOne
    @JoinColumn(name = "POLICY_COMPANY")
    private InsuranceCarrier carrier;

    @Column(name = "SOME_NOTES")
    private String notes;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @Transient
    private boolean selected = false;

    @ManyToOne
    @JoinColumn(name = "MODIFIED_BY")
    private User modifiedBy;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    private User createdBy;

    @Override
    public String toString() {
        return getId().toString();
    }

    @PrePersist
    void onPrePersist() {
        setCreatedBy(getModifiedBy());
        setCreationDate(new Date());
        setModificationDate(new Date());
    }

    @PreUpdate
    void onPreUpdate() {
        setModificationDate(new Date());
    }

    public boolean isNew() {
        return id == null;
    }
}
