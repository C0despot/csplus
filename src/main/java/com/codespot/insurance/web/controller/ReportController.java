package com.codespot.insurance.web.controller;

import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.service.file.FileService;
import com.codespot.insurance.web.resource.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
@RequestMapping(value = "report", method = RequestMethod.GET)
public class ReportController {

    private final
    FileService fileService;

    @Autowired
    public ReportController(FileService fileService) {
        this.fileService = fileService;
    }

    @RequestMapping(value = "pdf/{file}")
    @Logger(transaction = Log.Transaction.REPORT_PDF_INFO)
    public ResponseEntity<byte[]> getPdf(@PathVariable(value = "file") String filename) throws IOException {

        File file = fileService.pdfDownload(filename);
        byte[] contents = Files.readAllBytes(Paths.get(file.getPath()));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        return new ResponseEntity<>(contents, headers, HttpStatus.OK);
    }
}
