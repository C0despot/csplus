angular
    .module(APP)
    .controller('ClaimTabCtrl',
        function ($scope, $state, $stateParams, InsuranceService) {

            $scope.claims = [];
            $scope.selectedItem = 0;
            $scope.selected = null;
            $scope.state = $state;

            $scope.$on('$viewContentLoaded', () => {
                let id = $stateParams.id;
                InsuranceService
                    .Claim()
                    .getAll(id)
                    .then((response) => {
                        $scope.claims = response.data;
                    });
            });

            $scope.selectRow = (index) => {
                let value = $scope.claims[index].selected;
                angular.forEach($scope.claims, (claim) => {
                    claim.selected = false;
                });
                $scope.claims[index].selected = value;
                $scope.selected = $scope.claims[index].id;
                $scope.selectedItem = value ? $scope.claims[index].id : 0;
            };

        });