package com.codespot.insurance.model.entity.insurance;

import com.codespot.insurance.security.model.type.Types;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "GOODS")
public class Vehicle {

    @Id
    @Column(name = "GOODS_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "YEAR")
    private String year;

    @Column(name = "BRAND")
    private String brand;

    @Column(name = "MODEL")
    private String model;

    @Column(name = "CHASSIS_SERIAL")
    private String chassis;

    @Column(name = "COVERT_AMOUNT")
    private Double coverageAmount;

    @Column(name = "COLOR")
    private String color;

    @Column(name = "CYLINDER")
    private String cylinder;

    @Column(name = "PASSENGER_AMOUNT")
    private String passengers;

    @Column(name = "WEIGHT")
    private String weight;

    @Column(name = "SERVICES")
    private String service;

    @Column(name = "CAR_REGISTER")
    private String licensePlate;

    @Column(name = "CARE_TYPE")
    @Enumerated(value = EnumType.STRING)
    private Types.Care careType;

    @Column(name = "PRIME")
    private Double prime;

    @ManyToOne
    @JoinColumn(name = "COVERT")
    private Coverage coverage;

    @Column(name = "VIAL_CARES")
    private String vialCare;

    @Column(name = "MOBIL_CENTER")
    private String mobileCenter;

    @Column(name = "DEDUCIBLE")
    private Double deducible;

    @Column(name = "AIR_EMERGENCY")
    private String air;

    @Column(name = "RENT_CAR")
    private String rentCar;

    @Column(name = "GOOD_LIST_MAPPER")
    private String goodMapper;

    @ManyToOne
    @JoinColumn(name = "POLICY_LINKED")
    private Insurance insurance;

    @Column(name = "SELF_DAMAGE")
    private String selfDamage;

    @Column(name = "NOTES")
    private String notes;

    public static Vehicle dummy(Long element) {
        Vehicle vehicle = new Vehicle();
        vehicle.setId(element);
        return vehicle;
    }
}
