package com.codespot.insurance.service.invoice;

import com.codespot.insurance.model.dto.RenewDto;
import com.codespot.insurance.model.entity.Log;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.model.repository.InvoiceRepository;
import com.codespot.insurance.model.util.Utility;
import com.codespot.insurance.security.model.type.Types;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.service.customer.CustomerService;
import com.codespot.insurance.service.insurance.InsuranceService;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.codespot.insurance.web.resource.Logger;
import com.condos.shared.exception.NotFoundException;
import com.condos.shared.util.FieldUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service(value = "defaultInvoiceService")
public class DefaultInvoiceService implements InvoiceService {


    private final
    InvoiceRepository invoiceRepository;

    private final
    CustomerService customerService;

    private final
    InsuranceService insuranceService;

    private final
    UserService userService;

    private final
    LoggerActivityService activityService;

    @Autowired
    public DefaultInvoiceService(InvoiceRepository invoiceRepository, CustomerService customerService, InsuranceService insuranceService, UserService userService, LoggerActivityService activityService) {
        this.invoiceRepository = invoiceRepository;
        this.customerService = customerService;
        this.insuranceService = insuranceService;
        this.userService = userService;
        this.activityService = activityService;
    }

    @Override
    public List<Invoice> getAllCustomerInvoice(Long customerId, Long insuranceId) {
        List<Invoice> invoices;
        Customer customer = this.customerService.getOne(customerId);

        if (Utility.isValidId(insuranceId)) {
            Insurance insurance = this.insuranceService.getOne(insuranceId);
            invoices = this.invoiceRepository.findAllByCustomerEqualsAndInsuranceEquals(customer, insurance);
        } else {
            invoices = this.invoiceRepository.findAllByCustomerEquals(customer);
        }
        return invoices;
    }

    @Override
    public Invoice getOne(Long invoice) {
        return this.invoiceRepository.findById(invoice)
                .orElseThrow(() -> new NotFoundException(String.format("Factura [%s] no pudo ser encontrada!", invoice.toString()), invoice.toString(), LogActivity.Scope.INVOICE.name(), LogActivity.Action.READ.name()));
    }

    @Override
    public Invoice getOneWithCustomerId(Long customerId, Long invoiceId) {
        Customer customer = this.customerService.getOne(customerId);
        return this.invoiceRepository.findByCustomerEqualsAndIdEquals(customer, invoiceId);
    }

    @Override
    @Transactional
    public Invoice save(Invoice invoice) {
        invoice.setModificationDate(new Date());
        invoice.setModifiedBy(this.userService.loggedUser());
        if (Objects.isNull(invoice.getId())) {
            invoice.setCreationDate(new Date());
            invoice.setCreatedBy(this.userService.loggedUser());
            invoice.setBalance(invoice.getPrime());
            switch (invoice.getType()) {
                case RENEWAL:
                    Insurance insurance = this.insuranceService.getOne(invoice.getInsurance().getId());
                    insurance.setStartDate(invoice.getBeginningDate());
                    insurance.setEndDate(invoice.getDueDate());
                    insurance.setPrime(invoice.getPrime());
                    insurance = this.insuranceService.save(insurance);
                    invoice.setInsurance(insurance);
                    break;
                case INCREASE:
                    insurance = this.insuranceService.getOne(invoice.getInsurance().getId());
                    insurance.setPrime(insurance.getPrime() + invoice.getPrime());
                    insurance = this.insuranceService.save(insurance);
                    invoice.setInsurance(insurance);
                    break;
                default:
                    break;
            }

        } else {
            Invoice foundInvoice = getOne(invoice.getId());
            Double diff = invoice.getPrime() - foundInvoice.getPrime();
            invoice.setBalance(foundInvoice.getBalance() + diff);
            switch (invoice.getType()) {
                case RENEWAL:
                    Insurance insurance = this.insuranceService.getOne(invoice.getInsurance().getId());
                    insurance.setEndDate(invoice.getDueDate());
                    insurance.setStartDate(invoice.getBeginningDate());
                    insurance.setPrime(insurance.getPrime() + diff);
                    insurance = this.insuranceService.save(insurance);
                    invoice.setInsurance(insurance);
                    break;
                case INCREASE:
                    insurance = this.insuranceService.getOne(invoice.getInsurance().getId());
                    insurance.setPrime(insurance.getPrime() + diff);
                    insurance = this.insuranceService.save(insurance);
                    invoice.setInsurance(insurance);
                    break;
                default:
                    break;
            }
        }

        String message = "MODIFICANDO INFORMACION DE FACTURA, %s.";
        LogActivity.Action action = LogActivity.Action.MODIFY;

        if (invoice.isNew()) {
            message = "AGREGANDO NUEVA FACTURA, %s.";
            action = LogActivity.Action.CREATE;

        }
        return validateAndSave(invoice, action, message);
    }

    private Invoice validateAndSave(Invoice invoice, LogActivity.Action action, String message) {
        LogActivity.Scope scope = LogActivity.Scope.INVOICE;
        FieldUtil.validateFields(invoice, scope.name(), action.name());
        invoice = invoiceRepository.save(invoice);
        activityService.success(String.format(message, invoice.getInvoiceNo()), scope, action, invoice.getId().toString());
        return invoice;
    }

    @Override
    @Transactional
    public Invoice renew(RenewDto renew) {
        Invoice invoice = Invoice
                .builder()
                .invoiceNo(renew.getInvoiceNo())
                .balance(renew.getPrime())
                .creationDate(new Date())
                .createdBy(this.userService.loggedUser())
                .modificationDate(new Date())
                .modifiedBy(this.userService.loggedUser())
                .customer(renew.getInsurance().getCustomer())
                .insurance(renew.getInsurance())
                .beginningDate(renew.getStartDate())
                .dueDate(renew.getEndDate())
                .startDate(new Date())
                .endDate(new Date())
                .notes("AUTOGENERADA TRAS PROCESO DE RENOVACIÓN AUTOMÁTICA.")
                .prime(renew.getPrime())
                .type(Types.Invoice.RENEWAL)
                .build();

        Insurance insurance = this.insuranceService.getOne(invoice.getInsurance().getId());
        insurance.setEndDate(invoice.getDueDate());
        insurance.setStartDate(invoice.getBeginningDate());
        insurance.setPrime(invoice.getPrime());
        insurance = this.insuranceService.save(insurance);
        invoice.setInsurance(insurance);

        String message = "APLICANDO RENOVACION DE FACTURA, %s.";
        LogActivity.Action action = LogActivity.Action.RENEW;
        return validateAndSave(invoice, action, message);
    }

    @Override
    @Logger(transaction = Log.Transaction.UPDATE_INVOICE_BALANCES_JOB, isHttp = false)
    public void updateBalances() {
        this.invoiceRepository.updateBalances();
    }

}
