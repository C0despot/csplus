package com.codespot.insurance.model.repository;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.model.entity.payment.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReceiptRepository extends JpaRepository<Receipt, Long> {

    List<Receipt> findAllByCustomerEqualsAndInsuranceEqualsAndInvoiceEquals(Customer customer, Insurance insurance, Invoice invoice);

    List<Receipt> findAllByCustomerEqualsAndInsuranceEquals(Customer customer, Insurance insurance);

    List<Receipt> findAllByCustomerEquals(Customer customer);

    Receipt findByReceiptNoEquals(String num);

    @Query(value = "SELECT TRUNCATE(SUM(AMOUNT), 2) FROM RECEIPT WHERE USER_EDITOR = :employee", nativeQuery = true)
    Double getIncomesForEmployee(@Param(value = "employee") Long employee);

    List<Receipt> findAllByInvoiceEquals(Invoice invoice);
}
