package com.codespot.insurance.service.receipt;

import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.model.entity.payment.Receipt;
import com.codespot.insurance.model.repository.ReceiptRepository;
import com.codespot.insurance.model.util.Utility;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.service.customer.CustomerService;
import com.codespot.insurance.service.insurance.InsuranceService;
import com.codespot.insurance.service.invoice.InvoiceService;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.condos.shared.exception.NotFoundException;
import com.condos.shared.exception.UnsupportedOperationException;
import com.condos.shared.util.DataUtil;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service(value = "defaultReceiptService")
public class DefaultReceiptService implements ReceiptService {

    private final
    CustomerService customerService;

    private final
    InsuranceService insuranceService;

    private final
    InvoiceService invoiceService;

    private final
    ReceiptRepository receiptRepository;

    private final
    UserService userService;

    private final
    LoggerActivityService activityService;

    @Autowired
    public DefaultReceiptService(CustomerService customerService, InsuranceService insuranceService, InvoiceService invoiceService, ReceiptRepository receiptRepository, UserService userService, LoggerActivityService activityService) {
        this.customerService = customerService;
        this.insuranceService = insuranceService;
        this.invoiceService = invoiceService;
        this.receiptRepository = receiptRepository;
        this.userService = userService;
        this.activityService = activityService;
    }

    @Override
    public List<Receipt> findAllByCustomerInsuranceInvoice(Long customerId, Long insuranceId, Long invoiceId) {

        Customer customer = this.customerService.getOne(customerId);
        List<Receipt> receipts;

        if (Utility.isValidId(insuranceId) && Utility.isValidId(invoiceId)) {
            Insurance insurance = this.insuranceService.getOne(insuranceId);
            Invoice invoice = this.invoiceService.getOne(invoiceId);
            receipts = this.receiptRepository.findAllByCustomerEqualsAndInsuranceEqualsAndInvoiceEquals(customer, insurance, invoice);
        } else if (Utility.isValidId(insuranceId)) {
            Insurance insurance = this.insuranceService.getOne(insuranceId);
            receipts = this.receiptRepository.findAllByCustomerEqualsAndInsuranceEquals(customer, insurance);
        } else {
            receipts = this.receiptRepository.findAllByCustomerEquals(customer);
        }
        return receipts;
    }

    @Override
    public Receipt getOne(Long receipt) {
        return this.receiptRepository.findById(receipt)
                .orElseThrow(() -> new NotFoundException(String.format("Recibo [%s] no pudo ser encontrada!", receipt.toString()), receipt.toString(), LogActivity.Scope.RECEIPT.name(), LogActivity.Action.READ.name()));

    }

    @Override
    @Transactional
    public Receipt save(@NonNull Receipt receipt) {

        if (Objects.isNull(receipt.getId())) {
            receipt.setCreationDate(new Date());
            receipt.setCreatedBy(this.userService.loggedUser());
            Invoice invoice = this.invoiceService.getOne(receipt.getInvoice().getId());
            if (DataUtil.moneyCompare(invoice.getBalance(), receipt.getAmount()) || receipt.getAmount() <= 0)
                throw new UnsupportedOperationException("El monto no puede ser mayor al balance de la factura.");

            invoice.setBalance(invoice.getBalance() - receipt.getAmount());
            this.insuranceService.getOne(receipt.getInsurance().getId());
            invoice = this.invoiceService.save(invoice);
            receipt.setInvoice(invoice);
        } else {
            Receipt fromDb = getOne(receipt.getId());
            receipt.setUser(fromDb.getUser());
        }

        receipt.setModificationDate(new Date());
        receipt.setModifiedBy(this.userService.loggedUser());
        receipt.setInsurance(receipt.getInvoice().getInsurance());
        receipt.setCurrency(receipt.getInsurance().getCurrency());

        String message = "MODIFICANDO INFORMACION DE RECIBO, %s.";
        LogActivity.Action action = LogActivity.Action.MODIFY;

        if (receipt.isNew()) {
            message = "AGREGANDO NUEVO RECIBO, %s.";
            action = LogActivity.Action.CREATE;
        }
        receipt = this.receiptRepository.save(receipt);
        activityService.success(String.format(message, receipt.getReceiptNo()), LogActivity.Scope.RECEIPT, action, receipt.getId().toString());

        return receipt;
    }
}
