package com.codespot.insurance.model.dto;

import com.codespot.insurance.model.entity.insurance.Insurance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RenewDto {

    private Date startDate;
    private Date endDate;
    private Double prime;
    private Insurance insurance;
    private String invoiceNo;
}
