package com.codespot.insurance.model.repository.log;


import com.codespot.insurance.model.entity.LogDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author Jose An Rodriguez
 */
public interface LogDetailRepository extends JpaRepository<LogDetail, String>, JpaSpecificationExecutor<LogDetail> {
}
