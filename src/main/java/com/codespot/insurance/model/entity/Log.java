package com.codespot.insurance.model.entity;

import com.codespot.insurance.security.model.entity.User;
import lombok.*;

import javax.persistence.*;
import java.util.Date;


@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LOG")
public class Log {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "IP_ADDRESS")
    private String ipAddress;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "USER")
    private User user;

    @Column(name = "TYPE")
    @Enumerated(value = EnumType.STRING)
    private Transaction type;

    @Column(name = "DATE")
    private Date date;

    @Column(name = "PARAMS")
    private String params;

    @Column(name = "METHOD")
    private String method;

    public enum Transaction {
        SAVE_CREDIT_DOCUMENT,
        GET_CREDIT_INFO_BY_DOCUMENT,
        ANOTHER,
        GET_EXTENDED_CUSTOMER_INFO,
        SAVE_CUSTOMER_BASIC_INFO,
        GET_ALL_CUSTOMER_INSURANCES,
        GET_ALL_CUSTOMER_INVOICES,
        GET_ALL_CUSTOMER_BY_FILTER,
        GET_ALL_CUSTOMER_DOCS_BY_FILTER,
        BUILD_REDIRECT_URI,
        GET_ALL_INSURANCE_BY_CUSTOMER,
        GET_INSURANCE_DETAIL,
        GET_BRANCHES,
        GET_ALL_INSURANCE_COVERAGE,
        GET_ALL_INSURANCE_CARRIERS,
        GET_INSURANCE_AVAILABLE_CARRIERS,
        SAVE_INSURANCE_CARRIER,
        GET_ALL_INSURANCE_VEHICLES,
        GET_VEHICLE_INFO,
        SAVE_VEHICLE,
        GET_ALL_INSURANCE_PRIMITIVE,
        GET_ALL_INSURANCE_LOCALS,
        GET_BRANCH_INFO,
        GET_ALL_RENEW_DOCUMENTS,
        CREATE_NEW_BRANCH,
        PRINT_RENEW_LIST,
        DELETE_INSURANCE_DOCUMENT,
        GET_CLAIM_INFO,
        SAVE_CLAIM_INFO,
        GET_ALL_CLAIMS,
        GET_ALL_INVOICE_BY_CUSTOMER,
        GET_INVOICE_INFO,
        SAVE_INVOICE_INFO,
        INVOICE_RENEW_PROCESS,
        GET_ALL_RECEIPT_BY_CUSTOMER,
        GET_RECEIPT_INFO,
        SAVE_RECEIPT_INFO,
        REPORT_PDF_INFO,
        STATS_MONTHLY_REPORT,
        GET_CUSTOMER_PERSONAL_STATS,
        CUSTOMER_CURRENT_STATUS_REPORT,
        CUSTOMER_HISTORICAL_STATUS_REPORT,
        PRINT_CUSTOMER_STATUS_REPORT,
        GET_COMPANY_INCOMES,
        MONEY_EXCHANGE,
        VALIDATE_TRANSACTION_CONSTRAINS,
        GET_NEXT_SEQUENCE_VALUE,
        GET_CURRENT_MONTH_NAME,
        GET_AVAILABLE_YEAR_MONTHS,
        GET_ALL_TRANSACTIONAL_LOGS,
        REVOKE_ACCESS_TOKEN,
        VALIDATE_ACCESS_TOKEN,
        VALIDATE_ACCOUNT_AUTHORITIES,
        GET_ALL_USERS,
        GET_USER_BY_EMAIL,
        CREATE_NEW_USER,
        UPDATE_USER_INFO,
        DELETE_USER_INFO,
        RESTORE_USER_ACCOUNT_MAIL,
        CHANGE_PROFILE_IMAGE,
        CHANGE_USER_PASSWORD,
        SET_USER_PASSWORD,
        RESET_USER_PASSWORD,
        UPDATE_INVOICE_BALANCES_JOB, DELETE_INSURANCE_ELEMENT, GET_ALL_CREDIT_BY_CUSTOMER
    }

    public enum Result {
        SUCCESS,
        FAILURE
    }
}
