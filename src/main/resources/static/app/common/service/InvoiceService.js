(function () {
    'use strict';
    angular
        .module(APP)
        .service('InvoiceService', InvoiceService);
})();

InvoiceService.$inject = ['$http'];

function InvoiceService($http) {

    return {
        allByCustomer: (customer, insurance = 0) => {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}&insurance=${insurance}`;
            return $http.get('invoice/customer/all' + qs);
        },
        getOne: (customer, invoice) => {
            let qs = `?access_token=${getAccessToken()}&customer=${customer}&invoice=${invoice}`;
            return $http.get('invoice/one' + qs);
        },
        save: (invoice) => {
            let qs = `?access_token=${getAccessToken()}`;
            return $http.post('invoice' + qs, invoice);
        }
    }
}