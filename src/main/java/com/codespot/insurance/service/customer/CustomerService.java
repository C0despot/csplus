package com.codespot.insurance.service.customer;

import com.codespot.insurance.model.dto.CustomerDto;
import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.customer.CustomerDoc;

import java.util.List;

public interface CustomerService {

    CustomerDto customerInfo(Long customer);

    Customer save(Customer customer) throws NoSuchFieldException, IllegalAccessException;

    Customer getOne(Long id);

    void delete(long id);

    List<Customer> filter(String criteria);

    List<CustomerDoc> filterDocs(String criteria);

}
