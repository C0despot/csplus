'use strict';
angular.module("ngLocale", [], ["$provide", function($provide) {
    let PLURAL_CATEGORY = {ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other"};
    $provide.value("$locale", {
        "DATETIME_FORMATS": {
            "AMPMS": [
                "AM",
                "PM"
            ],
            "DAY": [
                "Domingo",
                "Lunes",
                "Martes",
                "Miércoles",
                "Jueves",
                "Viernes",
                "Sábado"
            ],
            "ERANAMES": [
                "Antes de Cristo",
                "Aespués de Cristo"
            ],
            "ERAS": [
                "A.C.",
                "D.C."
            ],
            "FIRSTDAYOFWEEK": 6,
            "MONTH": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ],
            "SHORTDAY": [
                "Dom.",
                "Lun.",
                "Mar.",
                "Mié.",
                "Jue.",
                "Vie.",
                "Sáb."
            ],
            "SHORTMONTH": [
                "Ene.",
                "Feb.",
                "Mar.",
                "Abr.",
                "May.",
                "Jun.",
                "Jul.",
                "Ago.",
                "Sep.",
                "Oct.",
                "Nov.",
                "Dic."
            ],
            "WEEKENDRANGE": [
                5,
                6
            ],
            "fullDate": "EEEE, d 'de' MMMM 'de' y",
            "longDate": "d 'de' MMMM 'de' y",
            "medium": "d MMM y h:mm:ss a",
            "mediumDate": "d MMM y",
            "mediumTime": "h:mm:ss a",
            "short": "d/M/yy h:mm a",
            "shortDate": "d/M/yy",
            "shortTime": "h:mm a"
        },
        "NUMBER_FORMATS": {
            "CURRENCY_SYM": "$",
            "DECIMAL_SEP": ".",
            "GROUP_SEP": ",",
            "PATTERNS": [
                {
                    "gSize": 3,
                    "lgSize": 3,
                    "maxFrac": 3,
                    "minFrac": 0,
                    "minInt": 1,
                    "negPre": "-",
                    "negSuf": "",
                    "posPre": "",
                    "posSuf": ""
                },
                {
                    "gSize": 3,
                    "lgSize": 3,
                    "maxFrac": 2,
                    "minFrac": 2,
                    "minInt": 1,
                    "negPre": "-\u00a4",
                    "negSuf": "",
                    "posPre": "\u00a4",
                    "posSuf": ""
                }
            ]
        },
        "id": "es-do",
        "pluralCat": function(n, opt_precision) {  if (n === 1) {    return PLURAL_CATEGORY.ONE;  }  return PLURAL_CATEGORY.OTHER;}
    });
}]);