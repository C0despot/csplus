package com.codespot.insurance.security.model.type;

import lombok.NonNull;

public class Types {
    public enum Invoice {
        INCREASE,
        EMISSION,
        RENEWAL
    }

    public enum Currency {
        PESOS,
        DOLLARS,
        EUROS
    }

    public enum PaymentDocumentType {
        CREDITO,
        RECIBO
    }

    public enum Report {
        CURRENT,
        STORYLINE
    }

    public enum Customer {
        PERSONA,
        EMPRESA
    }

    public enum CustomerDocType {

        INSURANCE("PÓLIZA"),
        CUSTOMER("CLIENTE"),
        INVOICE("FACTURA"),
        RECEIPT("RECIBO"),
        CREDIT("CRÉDITO"),
        CLAIM("RECLAMO"),
        DEPENDENT("DEPENDIENTE");

        private String spn;

        public String spn() {
            return spn;
        }

        CustomerDocType(String spn) {
            this.spn = spn;
        }
    }

    public enum Care {
        LAW,
        DAMAGE
    }

    public enum InsuranceStatus {
        VIGENTE,
        NO_VIGENTE,
        CANCELADA
    }

    public enum Role {
        ADMIN("ADMINISTRADOR"),
        AGENT("AGENTE EXTERNO"),
        EMPLOYEE("EMPLEADO");

        private String value;

        Role(String spn) {
            this.value = spn;
        }

        public String value() {
            return value;
        }
    }

    public enum UserStatus {

        ACTIVE("ACTIVO"),
        FIRED("DESPEDIDO"),
        INACTIVE("INACTIVO");

        private String value;

        UserStatus(String spn) {
            this.value = spn;
        }

        public String value() {
            return value;
        }
    }

    public enum Month {
        JANUARY(0, "ENERO"),
        FEBRUARY(1, "FEBRERO"),
        MARCH(2, "MARZO"),
        APRIL(3, "ABRIL"),
        MAY(4, "MAYO"),
        JUNE(5, "JUNIO"),
        JULY(6, "JULIO"),
        AUGUST(7, "AGOSTO"),
        SEPTEMBER(8, "SEPTIEMBRE"),
        OCTOBER(9, "OCTUBRE"),
        NOVEMBER(10, "NOVIEMBRE"),
        DECEMBER(11, "DICIEMBRE"),
        ALL(-1, "TODOS");

        private int index;
        private String spanish;

        public String spanish() {
            return spanish;
        }

        public int index() {
            return index;
        }

        public boolean isAll(){
            return this.equals(ALL);
        }
        public static Month of(@NonNull Integer index) {
            for (Month month : Month.values()) {
                if (month.index() == index)
                    return month;
            }
            return null;
        }

        public static Month of(@NonNull String spanish) {
            for (Month month : Month.values()) {
                if (month.spanish().equalsIgnoreCase(spanish))
                    return month;
            }
            return null;
        }

        Month(int index, String spanish) {
            this.index = index;
            this.spanish = spanish;
        }
    }
}
