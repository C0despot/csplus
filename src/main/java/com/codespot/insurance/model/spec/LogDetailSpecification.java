package com.codespot.insurance.model.spec;

import com.codespot.insurance.model.entity.LogDetail;
import com.condos.shared.data.page.C2Criteria;
import com.condos.shared.util.DataUtil;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class LogDetailSpecification implements Specification<LogDetail> {

    private C2Criteria criteria;

    public static LogDetailSpecification of(C2Criteria criteria) {
        return new LogDetailSpecification(criteria);
    }

    private LogDetailSpecification(C2Criteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate
            (Root<LogDetail> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        return DataUtil.getPredicate(root, builder, criteria);
    }
}