angular
    .module(APP)
    .controller('EditCompanyModalCtrl', function ($scope, $uibModalInstance, InsuranceService, NotificationService, Carrier) {

        $scope.userTitle = 'Editar Compañía';
        $scope.isEditMode = false;
        $scope.loading = false;
        $scope.edit = true;
        $scope.uibModalInstance = $uibModalInstance;
        $scope.carrier = {
        };

        (() => {
            $scope.carrier = angular.copy(Carrier);
        })();

        $scope.saveCarrier = () => {
            let carrier = angular.copy($scope.carrier);
            $scope.loading = true;
            InsuranceService
                .saveCarrier(carrier)
                .then(ignored => {
                    NotificationService.successMsj();
                    $uibModalInstance.close(true);
                }, () => {
                    NotificationService.errorMsj();
                })
                .finally(() => $scope.loading = false)
        };

    });