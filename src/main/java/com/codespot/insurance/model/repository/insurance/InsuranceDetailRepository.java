package com.codespot.insurance.model.repository.insurance;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.InsuranceDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InsuranceDetailRepository extends JpaRepository<InsuranceDetail, Long> {

    List<InsuranceDetail> findAllByCustomerEquals(Customer customer);
}
