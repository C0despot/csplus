package com.codespot.insurance.service.logger;

import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.LogDetail;
import com.condos.shared.data.page.C2Criteria;
import org.springframework.data.domain.Page;

public interface LoggerActivityService {

    void log(String msg, LogActivity.Type type, LogActivity.Scope scope, LogActivity.Action action, String doc);

    void success(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc);

    void error(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc);

    void info(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc);

    void warning(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc);

    void fail(String msg, LogActivity.Scope scope, LogActivity.Action action, String doc);

    Page<LogActivity> logs(PageDto pageDto, C2Criteria criteria);
}
