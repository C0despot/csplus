angular
    .module(APP)
    .controller('NavigationController',
        ($scope, $window, AuthService, InsuranceService, $uibModal, $state, NotificationService, CustomerService, DataService) => {

            $scope.exchange = {
                to: 'DOP',
                from: 'USD',
                value: '1.0',
                result: '0.0',
            };

            $scope.version = '1.0.0';

            $scope.$watch('exchange', ignored => {
                $scope.exchangeMoney();
            });


            $scope.exchangeMoney = () => {
                let exchange = angular.copy($scope.exchange);
                if (!exchange.value)
                    return;
                exchange.value = numberFormatter(exchange.value);

                DataService
                    .moneyExchange(exchange)
                    .then((resp) => {
                        $scope.exchange.result = resp.data.result.calculated;
                    });
            };

            $scope.user = JSON.parse(localStorage.getItem("user"));

            $scope.customers = [];
            $scope.filter = {
                criteria: null
            };
            $scope.description = null;
            $scope.loading = false;

            $scope.getName = function (name) {
                let nameArr = name.split(" ");
                if (nameArr.length <= 2)
                    return name;
                return nameArr[0] + " " + nameArr [2];
            };

            $scope.applyFilter = () => {
                let criteria = $scope.filter.criteria;
                CustomerService
                    .buildRedirectUri(criteria)
                    .then((resp) => {
                        $window.location.href = resp.data.url;
                    });
                // if (criteria)
                //     $state.go("customer.detail.step_one", {id: criteria.clientId})
                $scope.filter.criteria = null;
            };


            $scope.refreshCustomers = function (value) {
                return CustomerService
                    .docs(value)
                    .then((response) => {
                        let customers = [];
                        response
                            .data
                            .forEach(cust => {
                                if (cust)
                                    customers.push(cust);
                            });
                        $scope.customers = customers;
                    });
            };

            $scope.saveBranch = (desc) => {
                $scope.loading = true;
                InsuranceService
                    .saveBranch(desc)
                    .then(() => {
                        $scope.description = null;
                        NotificationService.successMsj("Branch Agregado Correctamente.");
                        $state.reload();
                    }, () => {
                        NotificationService.errorMsj("Algo Salio Mal, Revise e Intente de Nuevo");
                    })
                    .finally(() => {
                        $scope.loading = false;
                    });
            };

            $scope.openResetPswModal = () => {
                let modalInstance = $uibModal.open({
                    templateUrl: 'app/user/modal/reset/password.html',
                    controller: 'PasswordController',
                    backdrop: 'static',
                    keyboard: false,
                    size: 'md',
                    windowClass: "animated fadeIn middle-modal"
                });

                modalInstance.result.then(function (resp) {
                    if (resp.dependent) {
                        $scope.editDependent(resp.dependent);
                    } else if (resp.isNew) {
                        $scope.addDependent();
                    }
                })
            };

            $scope.logout = function () {
                NotificationService
                    .confirm(
                        "Log Out?",
                        "Seguro que desea cerrar la session actual?",
                        () => {
                            AuthService
                                .logout()
                                .then((ignored) => {
                                    localStorage.clear();
                                    localStorage.setItem("redirectTo", "#/")
                                    $state.go("login");
                                }, (ignored) => {
                                    localStorage.clear();
                                    localStorage.setItem("redirectTo", "#/")
                                    $state.go("login");
                                })
                        })
            };

            (() => {
                InsuranceService.appVersion()
                    .then((resp) => {
                        $scope.version = resp.data.version;
                    })
                setTimeout(() => {
                    if (isFirstLoaded && !$scope.user.validated)
                        $scope.openResetPswModal();
                    isFirstLoaded = false;
                }, 50);
                DataService.currentMonth()
                    .then(function (response) {
                        $scope.currentMonth = response.data;
                    });
            })();
        });