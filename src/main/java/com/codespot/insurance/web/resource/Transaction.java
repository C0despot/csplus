package com.codespot.insurance.web.resource;

import lombok.Builder;
import lombok.Data;

public enum Transaction {
    INSURANCE,
    INVOICE,
    RECEIPT,
    CLAIMS,
    CREDIT;

    public TransactionMessage getTransactionErrorMessage() {
        switch (this) {
            case INSURANCE:
                return TransactionMessage.INSURANCE_ALREADY_EXISTS;
            case RECEIPT:
                return TransactionMessage.RECEIPT_ALREADY_EXISTS;
            case INVOICE:
                return TransactionMessage.INVOICE_ALREADY_EXISTS;
            case CREDIT:
                return TransactionMessage.CREDIT_ALREADY_EXISTS;
            case CLAIMS:
                return TransactionMessage.CLAIM_ALREADY_EXISTS;
            default:
                return TransactionMessage.NON_CATCH_ERROR_CODE;
        }
    }

    public TransactionIndexer getTransactionIndexer() {
        switch (this) {
            case INSURANCE:
                return TransactionIndexer.INSURANCE_TRANSACTION;
            case RECEIPT:
                return TransactionIndexer.RECEIPT_TRANSACTION;
            case INVOICE:
                return TransactionIndexer.INVOICE_TRANSACTION;
            case CREDIT:
                return TransactionIndexer.CREDIT_TRANSACTION;
            case CLAIMS:
                return TransactionIndexer.CLAIM_TRANSACTION;
            default:
                return TransactionIndexer.NONE;
        }
    }

    public TransactionValue getTransactionValue(Long value) {
        switch (this) {
            case INSURANCE:
                return TransactionValue.builder().index(value).prefix(TransactionPrefix.INS).build();
            case RECEIPT:
                return TransactionValue.builder().index(value).prefix(TransactionPrefix.REC).build();
            case INVOICE:
                return TransactionValue.builder().index(value).prefix(TransactionPrefix.INV).build();
            case CREDIT:
                return TransactionValue.builder().index(value).prefix(TransactionPrefix.CRD).build();
            case CLAIMS:
                return TransactionValue.builder().index(value).prefix(TransactionPrefix.CLM).build();
            default:
                return null;
        }
    }

    @Data
    @Builder
    public static class TransactionValue {
        private Long index;
        private TransactionPrefix prefix;

        public String getResult() {
            return prefix.name() + index;
        }
    }

    public enum TransactionMessage {
        INSURANCE_ALREADY_EXISTS,
        INVOICE_ALREADY_EXISTS,
        RECEIPT_ALREADY_EXISTS,
        CREDIT_ALREADY_EXISTS,
        CLAIM_ALREADY_EXISTS,
        NON_CATCH_ERROR_CODE
    }

    public enum TransactionPrefix {
        INS,
        INV,
        REC,
        CLM,
        CRD
    }

    public enum TransactionIndexer {
        INSURANCE_TRANSACTION,
        INVOICE_TRANSACTION,
        RECEIPT_TRANSACTION,
        CREDIT_TRANSACTION,
        CLAIM_TRANSACTION,
        NONE
    }
}
