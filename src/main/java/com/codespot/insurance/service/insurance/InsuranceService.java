package com.codespot.insurance.service.insurance;

import com.codespot.insurance.model.dto.DateFilter;
import com.codespot.insurance.model.dto.InsuranceDto;
import com.codespot.insurance.model.dto.PageDto;
import com.codespot.insurance.model.dto.RemoveDocument;
import com.codespot.insurance.model.entity.insurance.*;
import com.codespot.insurance.web.controller.InsuranceController;
import com.condos.shared.data.page.C2Criteria;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.util.List;

public interface InsuranceService {

    List<InsuranceDetail> getAll(Long customer);

    List<Insurance> getAllPrimitive(Long customer);

    Insurance getOne(Long insurance);

    Insurance save(InsuranceDto insuranceDto);

    Insurance save(Insurance insurance);

    InsuranceDto getByCustomerAndInsurance(Long customer, Long insurance);

    List<Branch> getAvailableBranches();

    List<Coverage> getAvailableCoverage();

    List<InsuranceCarrier> getAvailableCarriers();

    InsuranceCarrier saveCarrier(InsuranceCarrier carrier);

    Page<InsuranceCarrier> getAllCarriers(PageDto pageDto);

    List<Vehicle> getVehicles(Long insuranceId);

    List<Local> getLocals(Long insuranceId);

    Vehicle getOneVehicle(Long id);

    Branch saveBranch(String desc);

    Page<InsuranceRenew> getRenew(PageDto pageDto, C2Criteria criteria, DateFilter dateFilter);

    String printRenew(PageDto pageDto, C2Criteria criteria, DateFilter dateFilter) throws IOException;

    void deleteDocument(RemoveDocument documentInfo);

    void deleteElement(Long element, InsuranceController.ElementType type);
}
