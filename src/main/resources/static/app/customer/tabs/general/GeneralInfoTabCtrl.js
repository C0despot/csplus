angular
    .module('inspinia')
    .controller('GeneralInfoTabCtrl', function ($scope, $uibModal, $stateParams, InsuranceService, $state, CustomerService, DataService, NotificationService) {

        $scope.title = 'General Information';
        $scope.customer = {
            type: 'Persona Física'
        };
        $scope.edit = false;
        $scope.loading = false;
        $scope.isEditMode = true;

        $scope.$on('$viewContentLoaded', function () {
            $scope.loadCustomer();
        });

        $scope.loadCustomer = function () {
            let id = $stateParams.id;
            CustomerService
                .getExtendedCustomerInfo(id)
                .then(function (response) {
                    $scope.edit = false;
                    $scope.customer = response.data;
                }, function (error) {
                })
        };
        $scope.save = function () {
            let customer = DataService.trimFields(['idCard', 'rnc', 'cellPhone', 'homePhone', 'workPhone'], angular.copy($scope.customer));
            $scope.loading = true;
            CustomerService
                .saveBasicInfo(customer)
                .then(function (response) {
                    NotificationService.successMsj();
                    $scope.customer = response.data;
                }, function (errorResp) {
                    NotificationService.errorMsj(errorResp.data.message);
                })
                .finally(function () {
                    $scope.loading = false;
                    $scope.edit = false;
                });
        }

        $scope.showAuditedItem = () => {
            let customer = angular.copy($scope.customer);
            $uibModal.open({
                templateUrl: 'app/report/audit_modal.html',
                controller: 'AuditController',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                windowClass: "animated fadeIn",
                resolve: {
                    document: function () {
                        return {
                            id: customer.name,
                            type: 'CLIENTE',
                            createdBy: customer.createdBy,
                            createdDate: dateFormatter(customer.creationDate),
                            modifiedDate: dateFormatter(customer.modificationDate),
                            modifiedBy: customer.modifiedBy
                        }
                    }
                }
            });
        };

        $scope.deleteItem = () => {
            const customer = angular.copy($scope.customer);
            NotificationService
                .confirmPassword(
                    (password) => {
                        let deleteObj = {
                            documentNo: customer.clientId,
                            type: 'CUSTOMER',
                            password: password
                        };
                        InsuranceService
                            .deleteObj(deleteObj)
                            .then(() => {
                                $state.go("dashboards.dashboard_2");
                            });
                    })
        }
    });