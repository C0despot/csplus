angular
    .module(APP)
    .controller('InvoiceCtrl', function ($scope, $uibModal, $state, $stateParams, InvoiceService, InsuranceService, NotificationService) {

        $scope.edit = false;
        $scope.loading = false;
        $scope.greenLight = false;
        $scope.invoice = {
            invoiceNo: null,
            customer: null,
            insurance: null,
            startDate: new Date(),
            endDate: new Date(),
            beginningDate: new Date(),
            dueDate: nextYear(),
            prime: 0
        };
        // noinspection DuplicatedCode
        $scope.updatePrime = () => {
            if ($scope.invoice.insurance) {
                $scope.invoice.prime = angular.copy($scope.invoice.insurance.prime);
            }
            $scope.invoice.beginningDate = new Date($scope.invoice.insurance.startDate);
            $scope.invoice.dueDate = new Date($scope.invoice.insurance.endDate);
        };
        $scope.insurances = [];
        $scope.$on('$viewContentLoaded', function () {
            let customer = $stateParams.id;
            let invoice = $stateParams.invoice;

            InsuranceService
                .getByCustomer(customer)
                .then(function (response) {
                    $scope.insurances = response.data;
                    InvoiceService
                        .getOne(customer, invoice)
                        .then(function (response) {
                            $scope.invoice = angular.copy(response.data);
                            $scope.greenLight = true;
                        }, function (errorResp) {
                            NotificationService.errorMsj(errorResp.data.message);
                        });
                });

            InsuranceService
                .nextIndex('INVOICE')
                .then(function (response) {
                    setTimeout(() => {
                        if ($scope.invoice.invoiceNo === null) {
                            $scope.invoice.invoiceNo = response.data.result;
                        }
                    }, 1000);
                }, function () {
                })

        });

        $scope.doSave = function () {
            let invoice = angular.copy($scope.invoice);
            invoice.startDate = new Date(invoice.startDate);
            invoice.endDate = new Date(invoice.endDate);
            invoice.beginningDate = new Date(invoice.beginningDate);
            invoice.dueDate = new Date(invoice.dueDate);
            invoice.prime = numberFormatter(invoice.prime);
            invoice.balance = numberFormatter(invoice.balance);

            $scope.loading = true;
            InvoiceService
                .save(invoice)
                .then(() => {
                    NotificationService.successMsj();
                    $state.go("customer.detail.step_three.list", {id: $stateParams.id});
                }, (errorResp) => {
                    NotificationService.errorMsj(errorResp.data.message);
                })
                .finally(() => {
                    $scope.loading = false;
                });
        };

        $scope.validateMe = function () {
            InsuranceService
                .validateNumber($scope.invoice.invoiceNo, 'INVOICE')
                .then(function () {
                    $("#invoiceNo").removeClass("has-error");
                }, function () {
                    $("#invoiceNo").addClass("has-error");
                })
        };

        $scope.showAuditedItem = () => {
            let invoice = angular.copy($scope.invoice);
            $uibModal.open({
                templateUrl: 'app/report/audit_modal.html',
                controller: 'AuditController',
                backdrop: 'static',
                keyboard: false,
                size: 'md',
                windowClass: "animated fadeIn",
                resolve: {
                    document: function () {
                        return {
                            id: invoice.invoiceNo,
                            type: 'FACTURA',
                            createdBy: invoice.createdBy,
                            createdDate: dateFormatter(invoice.creationDate),
                            modifiedDate: dateFormatter(invoice.modificationDate),
                            modifiedBy: invoice.modifiedBy
                        }
                    }
                }
            });
        };

        $scope.deleteItem = () => {
            const invoice = angular.copy($scope.invoice);
            NotificationService
                .confirmPassword(
                    (password) => {
                        let deleteObj = {
                            documentNo: invoice.id,
                            type: 'INVOICE',
                            password: password
                        };
                        InsuranceService
                            .deleteObj(deleteObj)
                            .then(() => {
                                $state.go("customer.detail.step_three.list", {id: invoice.customer.clientId});
                            });
                    })
        }
    });