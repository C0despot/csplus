package com.codespot.insurance.model.repository;

import com.codespot.insurance.model.entity.customer.Customer;
import com.codespot.insurance.model.entity.insurance.Insurance;
import com.codespot.insurance.model.entity.invoice.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    List<Invoice> findAllByCustomerEquals(Customer customer);

    List<Invoice> findAllByCustomerEqualsAndInsuranceEquals(Customer customer, Insurance insurance);

    Invoice findByCustomerEqualsAndIdEquals(Customer customer, Long id);

    Invoice findByInvoiceNoEquals(String num);

    List<Invoice> findAllByInsuranceEquals(Insurance insurance);

    @Query(value = "SELECT ROUND_INVOICE_BALANCE()", nativeQuery = true)
    void updateBalances();
}
