package com.codespot.insurance.service.credit;

import com.codespot.insurance.model.entity.LogActivity;
import com.codespot.insurance.model.entity.invoice.Invoice;
import com.codespot.insurance.model.entity.payment.Credit;
import com.codespot.insurance.model.repository.CreditRepository;
import com.codespot.insurance.security.service.user.UserService;
import com.codespot.insurance.service.invoice.InvoiceService;
import com.codespot.insurance.service.logger.LoggerActivityService;
import com.condos.shared.exception.NotFoundException;
import com.condos.shared.exception.UnsupportedOperationException;
import com.condos.shared.util.DataUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service(value = "defaultCreditService")
public class DefaultCreditService implements CreditService {

    private final
    CreditRepository creditRepository;

    private final
    InvoiceService invoiceService;

    private final
    UserService userService;

    private final
    LoggerActivityService activityService;

    @Autowired
    public DefaultCreditService(CreditRepository creditRepository, InvoiceService invoiceService, UserService userService, LoggerActivityService activityService) {
        this.creditRepository = creditRepository;
        this.invoiceService = invoiceService;
        this.userService = userService;
        this.activityService = activityService;
    }

    @Override
    public List<Credit> findAllByCustomer(Long customer) {
        return this.creditRepository.findAllByCustomerClientId(customer);
    }

    @Override
    public Credit getOne(Long id) {
        return this.creditRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Crédito [%s] no pudo ser encontrada!", id.toString()), id.toString(), LogActivity.Scope.CREDIT.name(), LogActivity.Action.READ.name()));
    }

    @Override
    @Transactional
    public Credit save(Credit credit) {
        if (Objects.isNull(credit.getId())) {
            credit.setCreationDate(new Date());
            credit.setCreatedBy(this.userService.loggedUser());
            Invoice invoice = this.invoiceService.getOne(credit.getInvoice().getId());
            if (credit.getAmount() == 0)
                throw new UnsupportedOperationException("El monto del credito no puede ser cero.");
            else if (DataUtil.moneyCompare(invoice.getBalance(), credit.getAmount()))
                throw new UnsupportedOperationException("El monto no coincide con el balance pendiente");

            invoice.setBalance(invoice.getBalance() - credit.getAmount());

            invoice = this.invoiceService.save(invoice);
            credit.setInvoice(invoice);
        }

        credit.setModificationDate(new Date());
        credit.setModifiedBy(this.userService.loggedUser());
        credit.setInsurance(credit.getInvoice().getInsurance());

        String message = "MODIFICANDO INFORMACION DE CREDITO, %s.";
        LogActivity.Action action = LogActivity.Action.MODIFY;

        if (credit.isNew()) {
            message = "AGREGANDO NUEVO CREDITO, %s.";
            action = LogActivity.Action.CREATE;
        }

        credit = this.creditRepository.save(credit);

        activityService.success(String.format(message, credit.getDocNo()), LogActivity.Scope.CREDIT, action, credit.getId().toString());
        return credit;
    }
}
